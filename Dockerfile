FROM python:3.8-alpine

ENV PATH="/scripts:${PATH}"

COPY ./requirements.txt /requirements.txt

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
RUN apk add --update --no-cache --virtual .tmp gcc libc-dev linux-headers
RUN apk add --no-cache jpeg-dev zlib-dev
RUN apk add --no-cache --virtual .build-deps build-base linux-headers

RUN pip install -r /requirements.txt
RUN apk del .tmp

RUN mkdir /bdtask_contractor
COPY ./bdtask_contractor /bdtask_contractor
WORKDIR /bdtask_contractor
COPY ./scripts /scripts

RUN chmod +x /scripts/*
RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static
# RUN adduser -D user
# RUN chown -R user:user /vol
# RUN chmod -R 755 /vol/web
# USER user

ENV PORT 8000
EXPOSE 80

RUN python manage.py makemigrations && python manage.py migrate
# && python manage.py collectstatic --noinput -v3
# CMD ["entrypoint.sh"]

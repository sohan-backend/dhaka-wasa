from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from .models import CustomUserModel
from django.contrib.auth.models import Permission

# class UserCreationForm(forms.ModelForm):
#     """A form for creating new users. Includes all the required
#     fields, plus a repeated password."""
#     password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
#     password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)
#
#     class Meta:
#         model = CustomUserModel
#         fields = ('email', 'is_admin',)
#
#     def clean_password2(self):
#         # Check that the two password entries match
#         password1 = self.cleaned_data.get("password1")
#         password2 = self.cleaned_data.get("password2")
#         if password1 and password2 and password1 != password2:
#             raise forms.ValidationError("Passwords don't match")
#         return password2
#
#     def save(self, commit=True):
#         # Save the provided password in hashed format
#         user = super(UserCreationForm, self).save(commit=False)
#         user.set_password(self.cleaned_data["password1"])
#         if commit:
#             user.save()
#         return user
#
#
# class UserChangeForm(forms.ModelForm):
#     """A form for updating users. Includes all the fields on
#     the user, but replaces the password field with admin's
#     password hash display field.
#     """
#     password = ReadOnlyPasswordHashField()
#
#     class Meta:
#         model = CustomUserModel
#         fields = ('email', 'password', 'is_admin', 'is_active',)
#
#     def clean_password(self):
#         # Regardless of what the user provides, return the initial value.
#         # This is done here, rather than on the field, because the
#         # field does not have access to the initial value
#         return self.initial["password"]
#
#
# class UserAdmin(BaseUserAdmin):
#     # The forms to add and change user instances
#     form = UserChangeForm
#     add_form = UserCreationForm
#
#     # The fields to be used in displaying the User model.
#     # These override the definitions on the base UserAdmin
#     # that reference specific fields on auth.User.
#     list_display = ('email', 'username', 'is_admin', 'is_active')
#     list_filter = ('is_admin',)
#     fieldsets = (
#         (None, {'fields': ('email', 'password', 'is_active')}),
#         ('Permissions', {'fields': ('is_admin', 'is_staff', 'groups', 'user_permissions')}),
#     )
#     # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
#     # overrides get_fieldsets to use this attribute when creating a user.
#     add_fieldsets = (
#         (None, {
#             'classes': ('wide',),
#             'fields': ('email', 'username', 'password1', 'password2', 'is_admin',)}
#          ),
#     )
#     search_fields = ('email', 'username')
#     ordering = ('email',)
#
#
# # admin.site.register(CustomUserModel, UserAdmin)
# from .models import CustomUserModel
#
# admin.site.register(CustomUserModel)
#
#
# class PermissionAdmin(admin.ModelAdmin):
#     list_display = ('name', 'codename',)
#     search_fields = ('name', 'codename',)
#
#     class Meta:
#         model = Permission
#
#
# admin.site.register(Permission, PermissionAdmin)
#
# # class UserAdmin(BaseUserAdmin):
# #     # The forms to add and change user instances
# #     form = UserAdminChangeForm
# #     add_form = UserAdminCreationForm
# #
# #     # The fields to be used in displaying the User model.
# #     # These override the definitions on the base UserAdmin
# #     # that reference specific fields on auth.User.
# #     list_display = ('email', 'is_admin')
# #     list_filter = ('is_admin',)
# #     fieldsets = (
# #         (None, {'fields': ('email', 'password')}),
# #         ('Personal info', {'fields': ()}),
# #         ('Permissions', {'fields': ('admin',)}),
# #     )
# #     # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
# #     # overrides get_fieldsets to use this attribute when creating a user.
# #     add_fieldsets = (
# #         (None, {
# #             'classes': ('wide',),
# #             'fields': ('email', 'user', 'password1', 'password2')}
# #          ),
# #     )
# #     search_fields = ('email',)
# #     ordering = ('email',)
# #     filter_horizontal = ()
# #
# #     def render_change_form(self, request, context, *args, **kwargs):
# #         context['adminform'].form.fields['user'].queryset = CustomUserModel.objects.filter(user=request.user)
# #         return super(UserAdmin, self).render_change_form(request, context, args, kwargs)
# #
# #     def get_queryset(self, request):
# #         qs = super(UserAdmin, self).get_queryset(request)
# #         if request.user.is_superuser:
# #             return qs
# #         return qs.filter(user=request.user)
#
#
# #
# #
# # admin.site.register(CustomUserModel)
# #
# # #
# # #
# # # # Remove Group Model from admin. We're not using it.
# # admin.site.unregister(Group)
# #
# # # from .models import CustomUserModel
# # # admin.site.register(CustomUserModel)
# # from django import forms
# # from django.contrib import admin
# # from django.contrib.auth import get_user_model
# # from django.contrib.admin.widgets import FilteredSelectMultiple
# # from django.contrib.auth.models import Group
# #
# # User = get_user_model()
#
#
# # class GroupAdminForm(forms.ModelForm):
# #     class Meta:
# #         model = Group
# #         exclude = []
# #
# #     # Add the users field.
# #     users = forms.ModelMultipleChoiceField(
# #         queryset=CustomUserModel.objects.all(),
# #         required=False,
# #         # Use the pretty 'filter_horizontal widget'.
# #         widget=FilteredSelectMultiple('users', False)
# #     )
# #
# #     def __init__(self, *args, **kwargs):
# #         # Do the normal form initialisation.
# #         super(GroupAdminForm, self).__init__(*args, **kwargs)
# #         # If it is an existing group (saved objects have a pk).
# #         if self.instance.pk:
# #             # Populate the users field with the current Group users.
# #             self.fields['users'].initial = self.instance.user_set.all()
# #
# #     def save_m2m(self):
# #         # Add the users to the Group.
# #         self.instance.user_set.set(self.cleaned_data['users'])
# #
# #     def save(self, *args, **kwargs):
# #         # Default save
# #         instance = super(GroupAdminForm, self).save()
# #         # Save many-to-many data
# #         self.save_m2m()
# #         return instance
#
#
# # class GroupAdmin(admin.ModelAdmin):
# #     # Use our custom form.
# #     form = GroupAdminForm
# #     # Filter permissions horizontal as well.
# #     filter_horizontal = ['permissions']
#
#
# # Register the new Group ModelAdmin.
# # admin.site.register(Group)

from .models import *

admin.site.register(CustomUserModel)
admin.site.register(PhoneOtp)
admin.site.register(UserLicense)
admin.site.register(AuthorizedPerson)
admin.site.register(SMSGateway)
admin.site.register(SSLSettings)

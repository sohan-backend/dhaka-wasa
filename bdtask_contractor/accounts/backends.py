from django.contrib.auth import get_user_model
from django.db.models import Q

User = get_user_model()


class CustomAuthenticationBackend:
    """
    Custom authentication backend to handle login by phone/email
    """

    def authenticate(email_or_phone=None, password=None):
        try:
            user = User.objects.filter(Q(email=email_or_phone) | Q(phone=email_or_phone)).first()
            password_valid = user.check_password(password)
            if password_valid:
                return user
            return None
        except User.DoesNotExist:
            print("user does not exists")
            return None


class CustomAuthenticationBackendPhone:
    """
    Custom authentication backend to handle login by phone/email
    """

    def authenticate(phone=None, password=None):
        try:
            user = User.objects.filter(phone=phone).first()
            if user:
                password_valid = user.check_password(password)
                if password_valid:
                    return user
                return None
        except User.DoesNotExist:
            print("user does not exists")
            return None

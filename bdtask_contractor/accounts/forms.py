from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth import get_user_model
from django import forms
from django.contrib.auth.models import Group, Permission
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.password_validation import validate_password
from django.db import models
from django.db.models import Q, fields
from django.utils.translation import ugettext_lazy as _
import re
from django.forms import TextInput

User = get_user_model()

from .models import CustomUserModel, AuthorizedPerson

from django.contrib.auth.forms import PasswordChangeForm


class PasswordUpdateForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super(PasswordUpdateForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'


# class RegisterForm(forms.ModelForm):
#     password = forms.CharField(widget=forms.PasswordInput)
#     password2 = forms.CharField(label='Confirm password', widget=forms.PasswordInput)
#
#     class Meta:
#         model = CustomUserModel
#         fields = ('email', 'user')
#
#     def clean_email(self):
#         email = self.cleaned_data.get('email')
#         qs = CustomUserModel.objects.filter(email=email)
#         if qs.exists():
#             raise forms.ValidationError("email is taken")
#         return email
#
#     def clean_password2(self):
#         # Check that the two password entries match
#         password1 = self.cleaned_data.get("password1")
#         password2 = self.cleaned_data.get("password2")
#         if password1 and password2 and password1 != password2:
#             raise forms.ValidationError("Passwords don't match")
#         return password2
#
#
# class UserAdminCreationForm(forms.ModelForm):
#     """
#     A form for creating new users. Includes all the required
#     fields, plus a repeated password.
#     """
#     password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
#     password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)
#
#     class Meta:
#         model = CustomUserModel
#         fields = ('email', 'user',)
#
#     def clean_password2(self):
#         # Check that the two password entries match
#         password1 = self.cleaned_data.get("password1")
#         password2 = self.cleaned_data.get("password2")
#         if password1 and password2 and password1 != password2:
#             raise forms.ValidationError("Passwords don't match")
#         return password2
#
#     def save(self, commit=True):
#         # Save the provided password in hashed format
#         user = super(UserAdminCreationForm, self).save(commit=False)
#         user.set_password(self.cleaned_data["password1"])
#         if commit:
#             user.save()
#         return user
#
#
# class UserAdminChangeForm(forms.ModelForm):
#     """A form for updating users. Includes all the fields on
#     the user, but replaces the password field with admin's
#     password hash display field.
#     """
#     password = ReadOnlyPasswordHashField()
#
#     class Meta:
#         model = CustomUserModel
#         fields = ('email', 'password', 'active', 'admin')
#
#     def clean_password(self):
#         # Regardless of what the user provides, return the initial value.
#         # This is done here, rather than on the field, because the
#         # field does not have access to the initial value
#         return self.initial["password"]


class RegisterForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            # self.fields['is_admin'].widget.attrs['class'] = ''
            # self.fields['is_secretary'].widget.attrs['class'] = ''
            # self.fields['is_accounts'].widget.attrs['class'] = ''
            self.fields['roles'].widget.attrs['class'] = 'select2 form-control'
            # self.fields['roles'].widgets.attr['class'] = 'form-control select2'

    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                               validators=[validate_password])

    class Meta:
        model = CustomUserModel
        fields = ('username', 'department', 'designation', 'email', 'password', 'roles', 'phone', 'signature')

    def clean_email(self):
        email = self.cleaned_data.get('email')
        qs = CustomUserModel.objects.filter(email=email)
        if qs.exists():
            raise forms.ValidationError("email is taken")
        return email

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2


class UserUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserUpdateForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            # self.fields['is_admin'].widget.attrs['class'] = ''
            # self.fields['is_secretary'].widget.attrs['class'] = ''
            # self.fields['is_accounts'].widget.attrs['class'] = ''
            self.fields['roles'].widget.attrs['class'] = 'select2 form-control'
            # self.fields['roles'].widgets.attr['class'] = 'form-control select2'

    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                               validators=[validate_password])

    class Meta:
        model = CustomUserModel
        # widgets = {
        #     'password': forms.PasswordInput(),
        # }
        # fields = ('username', 'email', 'password', 'roles', 'phone',)
        fields = ('username', 'department', 'designation', 'email', 'password', 'roles', 'phone', 'signature')


class EmployeeUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EmployeeUpdateForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            # self.fields['password'].widget.attrs['class'] = 'form-control'

    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                               validators=[validate_password])

    class Meta:
        model = CustomUserModel
        # widgets = {
        #     'password': forms.PasswordInput(),
        # }
        fields = ('username', 'email', 'password',)


class FrontLoginForm(forms.Form):
    phone = forms.CharField(max_length=255, label='Phone', help_text='phone', widget=forms.TextInput(
        attrs={
            'class': 'form-control h-auto placeholder-dark-75 rounded-pill border-0 py-4 px-8 mb-5',
            'placeholder': 'Phone Number'}))
    password = forms.CharField(max_length=255, label='Password', help_text='Password', widget=forms.PasswordInput(
        attrs={
            'class': 'form-control h-auto placeholder-dark-75 rounded-pill border-0 py-4 px-8 mb-5',
            'placeholder': 'Password'}))

    def clean(self, *args, **kwargs):
        phone = self.cleaned_data.get('phone')
        password = self.cleaned_data.get('password')

        # make_password(self.cleaned_data.get('password'))
        user_qs = User.objects.filter(Q(phone=phone))

        if not user_qs.exists():
            raise forms.ValidationError("Invalid credentials - user does not exists")

        user_obj = user_qs.first()

        if not user_obj.is_active:
            raise forms.ValidationError("Invalid credentials - user is not active")

        if not user_obj.check_password(password):
            raise forms.ValidationError("Password not correct")

        return super(FrontLoginForm, self).clean(*args, **kwargs)

class ForgetPasswordForm(forms.Form):
    phone = forms.CharField(max_length=255, label='Phone', help_text='phone', widget=forms.TextInput(
        attrs={
            'class': 'form-control h-auto placeholder-dark-75 rounded-pill border-0 py-4 px-8 mb-5',
            'placeholder': 'Phone Number'}))

    def clean(self, *args, **kwargs):
        phone = self.cleaned_data.get('phone')

        user_qs = User.objects.filter(Q(phone=phone))

        if not user_qs.exists():
            raise forms.ValidationError("Invalid credentials - Phone does not exists")

        return super(ForgetPasswordForm, self).clean(*args, **kwargs)


class PasswordRestForm(forms.Form):
    password = forms.CharField(max_length=255, label='Password', help_text='Password', widget=forms.PasswordInput(
        attrs={
            'class': 'form-control h-auto placeholder-dark-75 rounded-pill border-0 py-4 px-8 mb-5',
            'placeholder': 'Type New Password'}))

    c_password = forms.CharField(max_length=255, label='Confirm Password', help_text='Password',
                                 widget=forms.PasswordInput(
                                     attrs={
                                         'class': 'form-control h-auto placeholder-dark-75 rounded-pill border-0 py-4 px-8 mb-5',
                                         'placeholder': 'Confirm Password'}))

    def clean(self, *args, **kwargs):
        password1 = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('c_password')

        if password1 != password2:
            raise forms.ValidationError("Password did not Match!")


class FrontRegisterForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FrontRegisterForm, self).__init__(*args, **kwargs)
        
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                'class'] = 'form-control h-auto placeholder-dark-75 rounded-pill border-0 py-4 px-8 mb-5'
            # self.fields['password'].widget.attrs['class'] = 'form-control'
            self.fields['phone'].widget.attrs['placeholder'] = 'Phone Number'
            
            self.fields['username'].widget.attrs['placeholder'] = 'FullName'
            self.fields['email'].widget.attrs['placeholder'] = 'Email'
            self.fields['password1'].widget.attrs['placeholder'] = 'Type Your Password'
            self.fields['password2'].widget.attrs['placeholder'] = 'Confirm Password'

    password1 = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control h-auto text-white placeholder-white opacity-70 bg-dark-o-70 rounded-pill border-0 py-4 px-8'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control h-auto text-white placeholder-white opacity-70 bg-dark-o-70 rounded-pill border-0 py-4 px-8'}))

    # phone = forms.CharField( widget=TextInput(attrs={'type':'number'}))
    def clean_password(self):
        password = self.cleaned_data.get('password1')
        if len(password) < 8:
            raise forms.ValidationError('Password should not less than 8 character')
        return super(FrontRegisterForm, self).clean_password1()


    def clean(self, *args, **kwargs):
        phone = self.cleaned_data.get('phone')
        email = self.cleaned_data.get('email')
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        num_format = re.compile("^(?:\+?88)?01[0-9]\d{8}$")
        isnumber = re.match(num_format, phone)
        if not isnumber:
            raise forms.ValidationError("Please enter a valid phone number!")

        if password1 != password2:
            raise forms.ValidationError("Password did not Match!")
        else:
            if User.objects.filter(phone=phone).exists():
                raise forms.ValidationError("User with this phone number already exists!")
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError("User with this email already exists!")

        return super(FrontRegisterForm, self).clean(*args, **kwargs)

    class Meta:
        model = CustomUserModel
        widgets = {
            'password1': forms.PasswordInput(),
            'password2': forms.PasswordInput(),
        }
        fields = ('phone', 'username', 'email', 'password1', 'password2')


class LoginForm(forms.Form):
    username = forms.CharField(max_length=255, label='Username', help_text='username', widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Email or Phone Number'}))
    password = forms.CharField(max_length=255, label='Password', help_text='Password', widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Password'}))

    def clean(self, *args, **kwargs):
        email_or_phone = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        # make_password(self.cleaned_data.get('password'))
        user_qs = User.objects.filter(Q(email=email_or_phone) | Q(phone=email_or_phone))

        if not user_qs.exists():
            raise forms.ValidationError("Invalid credentials - user does not exists")

        user_obj = user_qs.first()

        if not user_obj.is_active:
            raise forms.ValidationError("Invalid credentials - user is not active ")

        if not user_obj.check_password(password):
            raise forms.ValidationError("Password not correct")

        return super(LoginForm, self).clean(*args, **kwargs)

    # def clean(self, *args, **kwargs):
    #     phone = self.cleaned_data.get('phone')
    #     password = self.cleaned_data.get('password')
    #
    #     user_qs = User.objects.filter(phone=phone)
    #
    #     if not user_qs.exists():
    #         raise forms.ValidationError("Invalid credentials - user does not exists")
    #
    #     user = user_qs.first()
    #
    #     if not user.is_active:
    #         raise forms.ValidationError("Invalid credentials - user does not exists")
    #
    #     if not user.check_password(password):
    #         raise forms.ValidationError("Credentials not correct")
    #
    #     return super(LoginForm, self).clean(*args, **kwargs)


class UserGroupForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserGroupForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

        self.fields['name'].widget.attrs.update({
            'placeholder': 'Enter group name...',
            'class': 'form-control',
            'maxlength': 20,
            'pattern': "^[A-Za-z- ']{1,}$"
        })

        self.fields['name'].help_text = "Only [_A-z .,'-] these characters are allowed"

        self.fields['permissions'] = forms.ModelMultipleChoiceField(
            label="Permissions", required=False,
            help_text="Hold down “Control”, or “Command” on a Mac, to select more than one.",
            queryset=Permission.objects.all()

        )

    class Meta:
        model = Group
        fields = ('name', 'permissions',)
        widgets = {
            'permissions': FilteredSelectMultiple("Permission", False, attrs={'rows': '2'}),
        }


class AuthoprisedPersonForm(forms.ModelForm):
    signature = forms.ImageField(label=_('Signature'), required=False,
                                 error_messages={'invalid': _("Image files only")}, widget=forms.FileInput)

    def __init__(self, *args, **kwargs):
        super(AuthoprisedPersonForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = AuthorizedPerson
        fields = '__all__'

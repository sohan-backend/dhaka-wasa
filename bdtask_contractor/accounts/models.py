from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, AbstractUser
)
from django.contrib.auth.models import Group


class UserManger(BaseUserManager):
    def create_user(self, email, phone, password=None):
        if not email:
            raise ValueError('User must have an email')
        user = self.model(phone=phone, email=self.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_staffuser(self, email, phone, password=None):
        user = self.create_user(email=email, phone=phone, password=password)
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, phone, password=None):
        user = self.create_user(email=email, phone=phone, password=password)
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class CustomUserModel(AbstractUser):
    ROLES = (
        ('CONTRACTOR', 'CONTRACTOR'),
        ('SUPERADMIN', 'SUPERADMIN'),
        ('DS_ADMIN', 'DS_ADMIN'),
        ('AS_ADMIN', 'AS_ADMIN'),
        ('0S_ADMIN', '0S_ADMIN'),
        ('CO_ADMIN', 'CO_ADMIN'),
        ('COMMITTEE_SECRETARY', 'COMMITTEE_SECRETARY'),
        ('COMMITTEE_MEMBER', 'COMMITTEE_MEMBER'),
        ('SECRETARY', 'SECRETARY'),
        ('DMD', 'DMD'),
        ('MD', 'MD')
    )

    username = models.CharField(max_length=55)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    phone = models.CharField(max_length=255, unique=True, null=True, blank=True)
   
    full_name = models.CharField(max_length=255, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(verbose_name='admin', default=False)
    roles = models.CharField(max_length=55, choices=ROLES, default='SUPERADMIN')
    # is_contractor = models.BooleanField(default=False)
    # is_secretary = models.BooleanField(verbose_name='Secretary', default=False)
    # is_accounts = models.BooleanField(verbose_name='Accounts', default=False)
    is_superuser = models.BooleanField(default=False)
    designation = models.CharField(max_length=255, blank=True, null=True)
    department = models.CharField(max_length=255, blank=True, null=True)
    signature = models.FileField(blank=True, null=True)

    objects = UserManger()

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = ['phone']  # Email & Password are required by default.

    def __str__(self):  # __unicode__ on Python 2
        return self.email

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    class Meta:
        verbose_name = 'User'


class UserLicense(models.Model):
    user = models.ForeignKey(CustomUserModel, on_delete=models.CASCADE)
    license = models.CharField(max_length=255)

    def __str__(self):
        return self.license


class PhoneOtp(models.Model):
    """
    Object will be invalid if count>10
    """
    phone = models.CharField(max_length=20)
    otp = models.CharField(max_length=10, blank=True, null=True)
    count = models.IntegerField(default=0, help_text='Number of OTP sent')
    created_at = models.DateTimeField(auto_now_add=True)
    valid = models.BooleanField(default=True)


class AuthorizedPerson(models.Model):
    name = models.CharField(max_length=32, null=True, blank=True,default="name will be here")
    signature = models.FileField(upload_to='settings/',default='settingts/image.png')

    def __str__(self):
        return self.name


class SMSGateway(models.Model):
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    message = models.CharField(max_length=255)

    def __str__(self):
        return self.username


class SSLSettings(models.Model):
    store_id = models.CharField(max_length=255)
    store_pass = models.CharField(max_length=255)

    def __str__(self):
        return self.store_id

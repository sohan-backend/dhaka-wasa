from django.urls import path
from accounts.views.views import *
from accounts.views.front_view import *

urlpatterns = [
    path('dashboard/', DashboardView.as_view(), name='dashboard'),

    # frontend urls

    path('login/', LoginView, name='phone-login'),
    path('signup/', PhoneRegisterView, name='phone-register-new'),
    path('signup-upgrade/', PhoneRegisterUpgradeView.as_view(), name='phone-register-old'),
    path('validateotp/', ValidateOtp.as_view(), name='validateotp'),

    path('loginotp/', ValidateOtpLogin.as_view(), name='login-otp'),
    path('logout/', front_logout_view, name='phone-logout'),

    path('forget-password/', forgotPassword, name='forgot-password'),
    path('validateotp-password/', ValidateOtpPassword.as_view(), name='validateotp-password'),
    path('password-reset/<str:otp>/', password_reset, name='password-reset'),
    # path('<str:str>/',loginTestView,name='test-login'),

    # backend urls
    path('backend/login/', login_view, name='login'),
    path('backend/logout/', back_logout_view, name='logout'),
    path('backend/add-user/', register_view, name='add-user'),
    path('backend/user-list/', userList, name='user-list'),
    path('backend/edit-user/<int:id>/', editUser, name='edit-user'),
    path('backend/delete-user/<int:id>/', deleteUser, name='delete-user'),

    path('backend/change-password/', change_password, name='change_password'),

    path('backend/role-permission/', rolePermission, name='role-permission'),
    path('backend/role_create/', create_role, name='role_create'),
    path('backend/role-list/', roleList, name='role-list'),
    path('backend/edit-role/<int:id>/', edit_role_page, name='edit-role'),
    path('backend/delete-group/<int:id>/', delete_particular_group, name='delete-group'),
    path('backend/assign-role/', user_assign_role, name='assign-role'),
    path('backend/user-role-list/', userRoleList, name='user-role-list'),
    path('backend/user-role-edit/<int:id>/', editUserRole, name='user-role-edit'),
    path('backend/remove-group-user/<int:uid>/<int:role_id>/', removeGroupUser, name='remove-group-user'),

    path('backend/send-application-list/', send_application_list, name='send-application'),
    # path('api/application_list/', json_application_list, name='json-application-lists'),

    path('add-auth-sign/<int:pk>', AuthorizedPerson.as_view(), name="add-auth-sign"),
    path('user-lists/<str:role>/', load_users, name="user-lists-role"),

]

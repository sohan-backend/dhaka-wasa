from django.http import request
from django.shortcuts import render, redirect, HttpResponse
from ..models import PhoneOtp, UserLicense, CustomUserModel
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.views import View
from django.contrib import messages
import datetime
from django.contrib.auth.hashers import make_password
import re, random
from django.db.models import Q
from ..backends import CustomAuthenticationBackend, CustomAuthenticationBackendPhone
from accounts.models import SMSGateway
import requests
from ..forms import FrontLoginForm, FrontRegisterForm, ForgetPasswordForm, PasswordRestForm

User = get_user_model()


def generate_otp(phone):
    """
    View to Generate OTP
    """
    if phone:
        key = random.randint(999, 9999)
        return key
    else:
        return False


def send_sms(otp, mobile):
    settings = SMSGateway.objects.all()
    if settings.exists():
        user = settings[0].username
        password = settings[0].password
        message = settings[0].message
    else:
        return redirect('No SMS Gateway added yet')
    otp = '{}{}'.format(message, otp)
    url = 'https://bulksms.teletalk.com.bd/link_sms_send.php?op=SMS&user={}&pass={}&mobile={}&charset=UTF-8&sms={}'.format(
        user, password, mobile, otp)
    x = requests.post(url)
    print(x)


def del_sessions(request):
    """
    To Delete Temporary sessions
    """
    try:
        del request.session['password']
    except KeyError as e:
        print("The requested Session variable is already deleted")

    try:
        del request.session['phone']
    except KeyError as e:
        print("The requested Session variable is already deleted")

    try:
        del request.session['phone_number']
    except KeyError as e:
        print("The requested Session variable is already deleted")

    try:
        del request.session['fullname']
    except KeyError as e:
        print("The requested Session variable is already deleted")

    try:
        del request.session['license']
    except KeyError as e:
        print("The requested Session variable is already deleted")


def LoginView(request):
    if request.method == 'POST':
        nextUrl = request.GET.get('next', None)
        form = FrontLoginForm(request.POST)
        if form.is_valid():
            phone = form.cleaned_data['phone']
            password = form.cleaned_data['password']
            user = CustomAuthenticationBackendPhone.authenticate(phone=phone, password=password)

            if user:
                request.session['password'] = password
                request.session['phone'] = phone
                key = generate_otp(phone)
                if key:
                    # if key is generated, create new otp object
                    old = PhoneOtp.objects.filter(phone=phone).last()
                    if old:
                        old.valid = False
                        old.save()

                    phoneotp = PhoneOtp.objects.create(phone=phone, otp=key)
                    phoneotp.save()

                    # send sms
                    send_sms(key, phone)
                    if nextUrl is not None:
                        return redirect("/loginotp/?next={}".format(nextUrl))

                    return redirect("/loginotp/")
    else:
        form = FrontLoginForm()

    context = {
        'form': form,
    }
    return render(request, 'frontend/front_login.html', context)


# class LoginView(View):
#     template_name = 'frontend/front_login.html'
#
#     def get(self, request, *args, **kwargs):
#         form = FrontLoginForm()
#         context = {
#             'form': form
#         }
#         if request.user.is_authenticated:
#             return redirect('/')
#         return render(request, self.template_name, context)
#
#     def post(self, request, *args, **kwargs):
#         form = FrontLoginForm(request.POST or None)
#
#         if form.is_valid():
#             print(form.cleaned_data)
#         else:
#             form = FrontLoginForm()
#
#         context = {
#             'form': form
#         }
#
#         return render(request, self.template_name, context)
#
#         phone = request.POST.get('phone')
#         password = request.POST.get('password')
#
#         user = CustomAuthenticationBackendPhone.authenticate(phone=phone, password=password)
#
#         if user:
#             request.session['password'] = password
#             request.session['phone'] = phone
#             key = generate_otp(phone)
#             if key:
#                 # if key is generated, create new otp object
#                 old = PhoneOtp.objects.filter(phone=phone).last()
#                 if old:
#                     old.valid = False
#                     old.save()
#
#                 phoneotp = PhoneOtp.objects.create(phone=phone, otp=key)
#                 phoneotp.save()
#
#                 # send sms
#                 send_sms(key, phone)
#
#                 return redirect("login-otp")
#         else:
#             # messages.warning(request, 'Credentials Error!')
#             return render(request, self.template_name)


class ValidateOtp(View):
    """
    View to Validate OTP
    """
    template_name = 'frontend/validate_otp.html'

    def get(self, request, *args, **kwargs):

        phone = request.session.get('phone', None)
        otp_obj = PhoneOtp.objects.filter(phone=phone).last()
        context = {
            'otp': otp_obj.otp
        }
        if phone is None:
            return redirect('dashboard')
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        """
        parameter: otp, fname, lname
        """
        phone = request.session.get('phone', None)
        license = request.session.get('license', None)
        nextUrl = request.GET.get('next', None)

        otp_obj = PhoneOtp.objects.filter(phone=phone).last()

        if "enter" in request.POST:
            """
            Validates Otp. 
            If otp is matched, user is registered
            """
            otp = request.POST.get('otp')

            # checks if otp is entered
            if otp:
                # checks if PhoneOtp object exists
                if otp_obj:
                    # checks expiration time (expiration time=5 min)
                    time = datetime.datetime.now(datetime.timezone.utc) - otp_obj.created_at

                    # checks if otp matches and it's expiration time
                    if otp == otp_obj.otp and time < datetime.timedelta(minutes=5):
                        # Register

                        if not license:

                            print(request.session['phone'])

                            user = User.objects.create(roles='CONTRACTOR', email=request.session['email'],
                                                       phone=request.session['phone'],
                                                       username=request.session['fullname'],
                                                       password=make_password(request.session['password']))
                            user.save()
                            otp_obj.delete()
                            # delete temporary session variables
                            del_sessions(request)
                            login(self.request, user, backend='django.contrib.auth.backends.ModelBackend')
                            if nextUrl is not None:
                                return redirect(nextUrl)
                            return redirect("/")
                        else:
                            user = User.objects.create(roles='CONTRACTOR', email=request.session['email'],
                                                       phone=request.session['phone'],
                                                       password=make_password(request.session['password']))
                            user.save()
                            UserLicense.objects.create(user=user, license=license)
                            # messages.info(request, "Registration successful!!")
                            # delete otp
                            otp_obj.delete()
                            # delete temporary session variables
                            del_sessions(request)
                            login(self.request, user, backend='django.contrib.auth.backends.ModelBackend')
                            if nextUrl is not None:
                                return redirect(nextUrl)
                            return redirect("/")


                    else:
                        messages.warning(request, "Otp does not match!")
                        if nextUrl is not None:
                            return redirect("/loginotp/?next={}".format(nextUrl))

                        return redirect('validateotp')
                else:
                    messages.warning(request, "Otp does not match!")
                    return redirect('validateotp')
            else:
                messages.warning(request, "Please Enter the OTP")
                return redirect('validateotp')

        if "resend" in request.POST:
            """
            otp expiry time = 5 min
            If time>=expiry time, sends a new otp
            """
            # Resend SMS
            if otp_obj:
                # checks otp expiration time
                time = datetime.datetime.now(datetime.timezone.utc) - otp_obj.created_at
                if time < datetime.timedelta(minutes=1):
                    messages.info(request,
                                  "Sorry! You cannot request before the otp expires. Try again few minutes later!!")
                    return redirect("validateotp")
                else:
                    # Generates new otp
                    key = generate_otp(phone)
                    if key:
                        # if key is generated, create new otp object
                        old = PhoneOtp.objects.filter(phone=phone).last()
                        if old:
                            old.valid = False
                            old.save()

                        phoneotp = PhoneOtp.objects.create(phone=phone, otp=key)
                        phoneotp.save()

                        # send sms
                        send_sms(key, phone)

                        messages.info(request, "We have sent a new 4-digit PIN to you number!!")

                        return redirect("validateotp")
            else:
                messages.warning(request, "Sorry! Phone no. is not registered!!")
                return redirect('phone-register-new')


class ValidateOtpLogin(View):
    """
    View to Validate OTP
    """
    template_name = 'frontend/validate_otp.html'

    def get(self, request, *args, **kwargs):
        phone = request.session.get('phone', None)
        otp_obj = PhoneOtp.objects.filter(phone=phone).last()
        print(otp_obj.otp)
        context = {
            'otp': otp_obj.otp
        }
        if phone is None:
            return redirect('dashboard')
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        """
        parameter: otp, fname, lname
        """
        nextUrl = request.GET.get('next', None)
        phone = request.session.get('phone', None)
        password = request.session.get('password')
        otp_obj = PhoneOtp.objects.filter(phone=phone).last()

        if "enter" in request.POST:
            """
            Validates Otp. 
            If otp is matched, user is registered
            """
            otp = request.POST.get('otp')

            # checks if otp is entered
            if otp:
                # checks if PhoneOtp object exists
                if otp_obj:
                    # checks expiration time (expiration time=5 min)
                    time = datetime.datetime.now(datetime.timezone.utc) - otp_obj.created_at

                    # checks if otp matches and it's expiration time
                    if otp == otp_obj.otp and time < datetime.timedelta(minutes=5):
                        # Register
                        user = CustomAuthenticationBackendPhone.authenticate(phone=phone, password=password)
                        # user = authenticate(phone=phone, password=password)

                        # messages.info(request, "Login successful!!")
                        # delete otp
                        otp_obj.delete()
                        # delete temporary session variables
                        del_sessions(request)
                        login(self.request, user, backend='django.contrib.auth.backends.ModelBackend')
                        if nextUrl is not None:
                            return redirect(nextUrl)
                        else:
                            return redirect('/')

                    else:
                        messages.info(request, "Otp does not match!")
                        if nextUrl is not None:
                            return redirect("/loginotp/?next={}".format(nextUrl))
                        return redirect('validateotp')
                else:
                    messages.info(request, "Otp does not match!")
                    if nextUrl is not None:
                        return redirect("/loginotp/?next={}".format(nextUrl))
                    return redirect('validateotp')
            else:
                messages.info(request, "Please Enter the OTP")
                if nextUrl is not None:
                    return redirect("/loginotp/?next={}".format(nextUrl))
                return redirect('validateotp')

        if "resend" in request.POST:
            """
            otp expiry time = 5 min
            If time>=expiry time, sends a new otp
            """
            # Resend SMS
            if otp_obj:
                # checks otp expiration time
                time = datetime.datetime.now(datetime.timezone.utc) - otp_obj.created_at
                if time < datetime.timedelta(minutes=1):
                    messages.info(request,
                                  "Sorry! You cannot request before the otp expires. Try again few minutes later!!")
                    return redirect("login-otp")
                else:
                    # Generates new otp
                    key = generate_otp(phone)
                    if key:
                        # if key is generated, create new otp object
                        old = PhoneOtp.objects.filter(phone=phone).last()
                        if old:
                            old.valid = False
                            old.save()

                        phoneotp = PhoneOtp.objects.create(phone=phone, otp=key)
                        phoneotp.save()

                        # send sms
                        send_sms(key, phone)

                        messages.info(request, "We have sent a new 4-digit PIN to you number!!")
                        return redirect("login-otp")
            else:
                messages.info(request, "Sorry! Phone no. is not registered!!")
                return redirect('phone-register')


def forgotPassword(request):
    print('******* session *** ', request.session.get('phone_number'))
    if request.method == 'POST':
        form = ForgetPasswordForm(request.POST)

        if form.is_valid():
            phone = form.cleaned_data['phone']
            request.session['phone_number'] = phone
            key = generate_otp(phone)
            if key:
                # if key is generated, create new otp object
                old = PhoneOtp.objects.filter(phone=phone).last()
                if old:
                    old.valid = False
                    old.save()

                phoneotp = PhoneOtp.objects.create(phone=phone, otp=key)
                phoneotp.save()

                # send sms
                send_sms(key, phone)

                return redirect('validateotp-password')

    else:
        form = ForgetPasswordForm()

    context = {
        'form': form,
    }
    return render(request, 'frontend/password_forgot.html', context)


class ValidateOtpPassword(View):
    """
    View to Validate OTP
    """
    template_name = 'frontend/validate-otp-password.html'

    def get(self, request, *args, **kwargs):
        phone = request.session.get('phone_number')
        otp_obj = PhoneOtp.objects.filter(phone=phone).last()
        print(otp_obj.otp)
        context = {
            'otp': otp_obj.otp
        }
        if phone is None:
            return redirect('dashboard')
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        """
        parameter: otp, fname, lname
        """

        phone = request.session.get('phone_number', None)

        otp_obj = PhoneOtp.objects.filter(phone=phone).last()

        if "enter" in request.POST:
            """
            Validates Otp. 
            If otp is matched, user is registered
            """
            otp = request.POST.get('otp')

            # checks if otp is entered
            if otp:
                # checks if PhoneOtp object exists
                if otp_obj:

                    # checks expiration time (expiration time=5 min)
                    time = datetime.datetime.now(datetime.timezone.utc) - otp_obj.created_at

                    # checks if otp matches and it's expiration time
                    if otp == otp_obj.otp and time < datetime.timedelta(minutes=10):
                        # otp_obj.delete()
                        # delete temporary session variables
                        # del_sessions(request)
                        # login(self.request, user, backend='django.contrib.auth.backends.ModelBackend')
                        return redirect('password-reset', otp=otp_obj.otp)

                    else:
                        messages.info(request, "Otp does not match!")
                        return redirect('validateotp-password')
                else:
                    messages.info(request, "Otp does not match!")
                    return redirect('validateotp-password')
            else:
                messages.info(request, "Please Enter the OTP")
                return redirect('validateotp-password')


def password_reset(request, otp):
    phone = request.session.get('phone_number')
    # return HttpResponse(phone)

    otp = PhoneOtp.objects.filter(otp=otp)
    if otp.exists():
        form = PasswordRestForm(request.POST or None)
        if form.is_valid():
            password = form.cleaned_data['password']
            user_obj = CustomUserModel.objects.filter(phone=phone).first()
            if user_obj:
                user_obj.set_password(password)
                user_obj.save()
                # user = CustomAuthenticationBackendPhone.authenticate(phone=phone, password=password)
                # if user:
                #     login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                otp_obj = otp.first()
                otp_obj.delete()
                del_sessions(request)
                messages.success(request, 'Your password has been reset')
                return redirect('phone-login')

        context = {
            'form': form
        }
        return render(request, 'frontend/password-reset-form.html', context)
    else:
        messages.info(request, 'Not Allowed')
        return redirect('/')


def PhoneRegisterView(request):
    if request.method == 'POST':
        form = FrontRegisterForm(request.POST)
        if form.is_valid():
            phone = form.cleaned_data['phone']
            fullname = form.cleaned_data['username']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password1']

            request.session['password'] = password
            request.session['phone'] = phone
            request.session['fullname'] = fullname
            request.session['email'] = email
            # otp generated
            key = generate_otp(phone)
            if key:
                # if key is generated, create new otp object
                old = PhoneOtp.objects.filter(phone=phone).last()
                if old:
                    old.valid = False
                    old.save()

                phoneotp = PhoneOtp.objects.create(phone=phone, otp=key)
                phoneotp.save()

                # send sms
                send_sms(key, phone)
                nexturl = request.GET.get('next', None)
                if nexturl is not None:
                    nexturl = '/validateotp/?next={}'.format(nexturl)
                    return redirect(nexturl)
                return redirect('validateotp')
        else:
            print(form.errors)

    else:
        form = FrontRegisterForm()
    nexturl = request.GET.get('next', None)

    if nexturl is not None:
        nexturl = '/login/?next={}'.format(nexturl)

    context = {
        'form': form,
        'nexturl': nexturl
    }
    return render(request, 'frontend/front_signup.html', context)


#
# class PhoneRegisterView(View):
#     template_name = 'frontend/front_signup.html'
#
#     def get(self, request, *args, **kwargs):
#         form = FrontRegisterForm()
#         context = {
#             'form': form
#         }
#         return render(request, self.template_name, context)
#
#     def post(self, request, *args, **kwargs):
#         """
#         Parameter: phone, password
#         """
#         form = FrontRegisterForm(request.POST)
#         if form.is_valid():
#             phone = form.cleaned_data['phone']
#             fullname = form.cleaned_data['username']
#             email = form.cleaned_data['email']
#             password = form.cleaned_data['password1']
#
#             request.session['password'] = password
#             request.session['phone'] = phone
#             request.session['fullname'] = fullname
#             request.session['email'] = email
#             # otp generated
#             key = generate_otp(phone)
#             if key:
#                 # if key is generated, create new otp object
#                 old = PhoneOtp.objects.filter(phone=phone).last()
#                 if old:
#                     old.valid = False
#                     old.save()
#
#                 phoneotp = PhoneOtp.objects.create(phone=phone, otp=key)
#                 phoneotp.save()
#
#                 # send sms
#                 send_sms(key, phone)
#
#                 return redirect('validateotp')
#
#         return redirect('phone-register-new')


class PhoneRegisterUpgradeView(View):
    template_name = 'frontend/front_signup_upgrade.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        """
        Parameter: phone, password
        """

        phone = request.POST.get('phone')
        fullname = request.POST.get('fullname')
        email = request.POST.get('email')
        license = request.POST.get('license')

        if not license:
            messages.warning(request, 'Please enter a valid license number')
            return redirect('phone-register-old')

        # phone validation
        num_format = re.compile("^(?:\+?88)?01[13-9]\d{8}$")
        isnumber = re.match(num_format, phone)
        if not isnumber:
            messages.info(request, "Please enter a valid phone number!")
            return redirect('phone-register-old')

        password = request.POST.get('password')
        # password validation
        pass_format = re.compile("^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$")
        isvalid = re.match(pass_format, password)
        if not isvalid:
            messages.warning(request, "Password must contain minimum 8 digit Alphanumeric character!")
            return redirect('phone-register-old')
        c_password = request.POST.get('cpassword')

        # password matching
        if password == c_password:
            # checks if phone number already exists
            if User.objects.filter(phone=phone).exists():
                # redirects with error message
                messages.warning(request, "User with this phone number already exists!")
                return redirect("phone-register-new")
            else:
                # Temporary Session variables created
                request.session['password'] = password
                request.session['phone'] = phone
                request.session['fullname'] = fullname
                request.session['email'] = email
                request.session['license'] = license

                # otp generated
                key = generate_otp(phone)
                if key:
                    # if key is generated, create new otp object
                    old = PhoneOtp.objects.filter(phone=phone).last()
                    if old:
                        old.valid = False
                        old.save()

                    phoneotp = PhoneOtp.objects.create(phone=phone, otp=key)
                    phoneotp.save()

                    # send sms
                    send_sms(key, phone)

                    print('OTP Here..', phoneotp.otp)
                    return redirect("validateotp")
        else:
            messages.warning(request, "Password is not Matching!")
            return redirect("phone-register-new")

        return redirect("validateotp")


def front_logout_view(request):
    """ User Log out """
    logout(request)
    return redirect('phone-login')


def back_logout_view(request):
    """ User Log out """
    logout(request)
    return redirect('login')

# class PhoneRegisterView(View):
#     """
#     Registration through phone
#     """
#
#     template_name = 'signup.html'
#
#     def get(self, request, *args, **kwargs):
#         return render(request, self.template_name)
#
#     def post(self, request, *args, **kwargs):
#         """
#         Parameter: phone, password
#         """
#
#         phone = request.POST.get('phone')
#         # phone validation
#         num_format = re.compile("^(?:\+?88)?01[13-9]\d{8}$")
#         isnumber = re.match(num_format, phone)
#         if not isnumber:
#             messages.info(request, "Please enter a valid phone number!")
#             return redirect('phone-register')
#
#         password = request.POST.get('password')
#         # password validation
#         pass_format = re.compile("^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$")
#         isvalid = re.match(pass_format, password)
#         if not isvalid:
#             messages.info(request, "Password must contain minimum 8 digit Alphanumeric character!")
#             return redirect('phone-register')
#         c_password = request.POST.get('confirm-password')
#
#         # password matching
#         if password == c_password:
#             # checks if phone number already exists
#             if User.objects.filter(phone=phone).exists():
#                 # redirects with error message
#                 messages.info(request, "User with this phone number already exists!")
#                 return redirect("phone-register")
#             else:
#                 # Temporary Session variables created
#                 request.session['password'] = password
#                 request.session['phone'] = phone
#
#                 # otp generated
#                 key = generate_otp(phone)
#                 if key:
#                     # if key is generated, create new otp object
#                     old = PhoneOtp.objects.filter(phone=phone).last()
#                     if old:
#                         old.valid = False
#                         old.save()
#
#                     phoneotp = PhoneOtp.objects.create(phone=phone, otp=key)
#                     phoneotp.save()
#                     return redirect("validateotp")
#         else:
#             messages.info(request, "Password is not Matching!")
#             return redirect("phone-register")
#
#         return redirect("validateotp")

# def login_view(request):
#     """
#     If logged in, redirects to homepage, else redirects to log in page
#     """
#     form = LoginForm(request.POST or None)
#     if form.is_valid():
#         email_or_phone = form.cleaned_data.get('username')
#         password = form.cleaned_data.get('password')
#         # Authenticates using CustomAuthenticationBackend
#         user = CustomAuthenticationBackend.authenticate(email_or_phone=email_or_phone, password=password)
#         login(request, user, backend='django.contrib.auth.backends.ModelBackend')
#         return redirect('/')
#     context = {"form": form}

from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.views.generic import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from accounts.forms import LoginForm, RegisterForm, AuthoprisedPersonForm
from django.contrib import messages
from django.contrib.auth.hashers import make_password
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from accounts.forms import UserGroupForm, UserUpdateForm, EmployeeUpdateForm
from django.db.models import Q
from ..backends import CustomAuthenticationBackend

from django.views.generic.edit import UpdateView, CreateView

from accounts.forms import PasswordUpdateForm
from django.contrib.auth import update_session_auth_hash

from contractor.models import *

from ..models import AuthorizedPerson

User = get_user_model()
from django.apps import apps
from contractor.models import *


# def check_user(user):
#     if user.groups.filter(name='accounts').exists() or user.is_admin:
#         return True
#     return False


def get_all_modals(request):
    all_models_new = []
    # apps_list = [app for app in settings.INSTALLED_APPS if not 'django' in app]
    if request.user.is_admin:
        apps_list = ['accounts', 'contractor', 'set_module']
    else:
        apps_list = []

    for app_name in apps_list:
        all_app_models = apps.get_app_config(app_name).get_models()
        app_models = []
        for model in all_app_models:
            app_models.append({
                'name': model.__name__,
                'meta_name': model
            })
        if len(app_models) > 0:
            all_models_new.append({
                'name': app_name,
                'tables': app_models
            })
    return all_models_new


@login_required()
@user_passes_test(lambda user: user.is_admin or user.is_restaurant_owner)
def create_role(request):
    if request.method == 'POST':
        role_name = request.POST['role']
        if request.user.is_admin:
            role_name = role_name
        if 'description' in request.POST:
            role_desc = request.POST['description']
        else:
            role_desc = ''

        group = Group.objects.filter(name=role_name)

        if group.exists():
            messages.error(request, 'Group Already Exists With Same Name , Try another name')
            return redirect('role-permission')

        create_group, created = Group.objects.get_or_create(name=role_name, description=role_desc,
                                                            created_by=request.user)

        if created:
            create_group.save()
            all_models = get_all_modals(request)

            user_access = []
            for app_name in all_models:
                for models in app_name['tables']:
                    can_add = app_name['name'] + '_' + models['name'] + '_1'
                    can_edit = app_name['name'] + '_' + models['name'] + '_2'
                    can_view = app_name['name'] + '_' + models['name'] + '_3'
                    can_delete = app_name['name'] + '_' + models['name'] + '_4'
                    con_type = ContentType.objects.get_for_model(models['meta_name'])
                    if can_add in request.POST:
                        add = {
                            'codename': 'add_' + str((models['name'])).lower(),
                            'name': 'Can add ' + models['name'],
                            'content_type': con_type
                        }
                        user_access.append(add)

                    if can_edit in request.POST:
                        edit = {
                            'codename': 'change_' + str((models['name'])).lower(),
                            'name': 'Can change ' + models['name'],
                            'content_type': con_type
                        }
                        user_access.append(edit)

                    if can_view in request.POST:
                        view = {
                            'codename': 'view_' + str((models['name'])).lower(),
                            'name': 'Can view ' + models['name'],
                            'content_type': con_type
                        }
                        user_access.append(view)

                    if can_delete in request.POST:
                        delete = {
                            'codename': 'delete_' + str((models['name'])).lower(),
                            'name': 'Can delete ' + models['name'],
                            'content_type': con_type
                        }
                        user_access.append(delete)
            for permission in user_access:
                permission, created = Permission.objects.get_or_create(codename=permission['codename'],
                                                                       content_type=permission['content_type'])

                if created:
                    return HttpResponse('created')
                create_group.permissions.add(permission)
            messages.add_message(request, messages.INFO, "Role has been created successfully !!")
            return redirect('role-list')

        else:
            messages.error(request, "Role Could not be created !!")
            return redirect('role-permission')


@login_required()
@user_passes_test(lambda user: user.is_admin)
def rolePermission(request):
    all_models = get_all_modals(request)
    all_perms = request.user.get_all_permissions()
    form = UserGroupForm()

    context = {
        "form": form,
        'all_models': all_models,
        "all_perms": all_perms,
    }
    return render(request, 'accounts/role-permission.html', context)


def get_all_role(request):
    roles_info = []
    if request.user.is_admin:
        all_roles = Group.objects.filter(created_by=request.user)
    else:
        all_roles = Group.objects.filter(Q(user=request.user) | Q(created_by=request.user))

    for role in all_roles:
        roles_info.append({
            'id': role.id,
            'name': role.name,
            'description': role.description,
            'created_by': role.created_by
        })
    return roles_info


@login_required()
@user_passes_test(lambda user: user.is_admin)
def roleList(request):
    """
    role of list for users
    """
    all_roles = get_all_role(request)
    context = {
        'all_roles': all_roles
    }
    return render(request, 'accounts/role-list.html', context)


@login_required()
@user_passes_test(lambda user: user.is_admin and user.roles == 'SUPERADMIN', login_url='/dashboard/')
def userList(request):
    """
    role of list for users
    """

    users = User.objects.filter(is_admin=True).order_by('-id')
    context = {
        'users': users
    }
    return render(request, 'accounts/user-list.html', context)


@login_required()
@user_passes_test(lambda user: user.is_admin)
def editUser(request, id):
    user = get_object_or_404(User, id=id)
    if request.method == 'POST':
        if request.user.is_admin:
            form = UserUpdateForm(request.POST, instance=user)
        else:
            form = EmployeeUpdateForm(request.POST, instance=user)
        if form.is_valid():
            user = form.save(commit=False)
            clearPassNoHash = form.cleaned_data['password']
            user.set_password(clearPassNoHash)
            user.save()
            messages.success(request, 'User Updated Successfully')
            return redirect('user-list')
        else:
            messages.error(request, "Sorry")
            print(form.errors)
            # return redirect('#')
    else:
        if request.user.is_admin:
            form = UserUpdateForm(instance=user)
        else:
            form = EmployeeUpdateForm(instance=user)
    context = {
        'form': form
    }
    return render(request, 'accounts/edit-user.html', context)


def deleteUser(request, id):
    if request.user.is_admin:
        user = get_object_or_404(User, id=id)
        user.delete()
        messages.success(request, 'User Deleted Successfully')
    return redirect('user-list')


def all_models_including_permission(group_name, request):
    group_all_permissions = group_name.permissions.all()
    all_permission_name = []
    group_all_info = []
    all_models = get_all_modals(request)
    for permission in group_all_permissions:
        all_permission_name.append(permission.codename)
    for module in all_models:
        module_name = module['name']
        model_permission_info = []
        for model in module['tables']:
            specific_permission = {'name': model['name']}
            if 'add_' + str(model['name']).lower() in all_permission_name:
                specific_permission.update({'can_add': True})
            else:
                specific_permission.update({'can_add': False})

            if 'change_' + str(model['name']).lower() in all_permission_name:
                specific_permission.update({'can_edit': True})
            else:
                specific_permission.update({'can_edit': False})

            if 'delete_' + str(model['name']).lower() in all_permission_name:
                specific_permission.update({'can_delete': True})
            else:
                specific_permission.update({'can_delete': False})

            if 'view_' + str(model['name']).lower() in all_permission_name:
                specific_permission.update({'can_view': True})
            else:
                specific_permission.update({'can_view': False})
            model_permission_info.append(specific_permission)
        module_permission = {'module_name': module_name, 'permission_info': model_permission_info}
        group_all_info.append(module_permission)
    return group_all_info


@login_required()
@user_passes_test(lambda user: user.is_admin)
def edit_role_page(request, id):
    if request.method == 'GET':
        specific_group = get_object_or_404(Group, id=id)
        print(specific_group)
        all_models = get_all_modals(request)
        all_perms = request.user.get_all_permissions()
        form = UserGroupForm(instance=specific_group)
        if specific_group:
            group_details = {
                'role_id': specific_group.id,
                'group_name': specific_group.name,
                'group_desc': specific_group.description,
            }

            model_permission_info = all_models_including_permission(specific_group, request)
        else:
            group_details = {}
            messages.error(request, "Requested group does not exist !!")
        context = {
            'model_info': model_permission_info,
            'group_details': group_details,
            "form": form,
            'all_models': all_models,
            "all_perms": all_perms,

        }
        return render(request, "accounts/edit-role.html", context)

    if request.method == 'POST':
        role_name = request.POST['role']
        try:
            role_desc = request.POST['description']
        except:
            role_desc = ''
        particular_group = Group.objects.get(pk=id)

        if not request.user.is_admin:
            if not particular_group.created_by == request.user:
                messages.error(request, 'You have not enough Permission to edit this Role')
                return redirect('edit-role', id=particular_group.id)

        if role_name == particular_group.name:
            pass
        else:
            group_qs = Group.objects.filter(name=role_name)
            if group_qs.exists():
                messages.error(request, f'Already Role {role_name} Exists, Try With Different roles ')
                return redirect('edit-role', id=particular_group.id)

        role_permission = request.POST.getlist('permission_type')
        all_models = get_all_modals(request)
        user_access_edit = []
        for app_name in all_models:
            for models in app_name['tables']:
                con_type = ContentType.objects.get_for_model(models['meta_name'])
                if app_name['name'] + '_' + models['name'] + '_can_add' in role_permission:
                    add = {
                        'codename': 'add_' + str((models['name'])).lower(),
                        'name': 'Can add ' + models['name'],
                        'content_type': con_type
                    }
                    user_access_edit.append(add)

                if app_name['name'] + '_' + models['name'] + '_can_edit' in role_permission:
                    edit = {
                        'codename': 'change_' + str((models['name'])).lower(),
                        'name': 'Can change ' + models['name'],
                        'content_type': con_type
                    }
                    user_access_edit.append(edit)

                if app_name['name'] + '_' + models['name'] + '_can_view' in role_permission:
                    view = {
                        'codename': 'view_' + str((models['name'])).lower(),
                        'name': 'Can view ' + models['name'],
                        'content_type': con_type
                    }
                    user_access_edit.append(view)

                if app_name['name'] + '_' + models['name'] + '_can_delete' in role_permission:
                    delete = {
                        'codename': 'delete_' + str((models['name'])).lower(),
                        'name': 'Can delete ' + models['name'],
                        'content_type': con_type
                    }
                    user_access_edit.append(delete)

        if particular_group:
            particular_group.permissions.clear()
            for new_role in user_access_edit:
                permission, created = Permission.objects.get_or_create(codename=new_role['codename'],
                                                                       content_type=new_role['content_type'])

                if created:
                    return HttpResponse('created')
                particular_group.permissions.add(permission)

            particular_group.description = role_desc
            particular_group.name = role_name
            particular_group.save()
            messages.add_message(request, messages.INFO, "Role has been updated successfully !!")
        else:
            messages.error(request, "Role Information could not be updated !!")
        return redirect('edit-role', id=particular_group.id)


@login_required()
@user_passes_test(lambda user: user.is_admin or user.is_restaurant_owner)
def delete_particular_group(request, id):
    particular_group = get_object_or_404(Group, pk=id)
    if not particular_group.created_by == request.user:
        messages.error(request, 'You have not enough Permission to delete this Role')
        return redirect('role-list')
    # particular_group = Group.objects.get(pk=id)
    particular_group.permissions.clear()
    particular_group.delete()
    messages.success(request, "Role has been deleted successfully !!")
    return redirect('role-list')


@login_required()
@user_passes_test(lambda user: user.is_admin)
def user_assign_role(request):
    if request.user.is_admin:
        # users = User.objects.filter(is_restaurant_owner=True)
        users = User.objects.filter()
    elif request.user.is_restaurant_owner:
        users = User.objects.filter()

    all_roles = get_all_role(request)
    if request.method == 'POST':
        user_id = request.POST['user_id']
        user = get_object_or_404(User, id=user_id)
        selected_roles = request.POST.getlist('selected_roles')
        for roles in selected_roles:
            group = Group.objects.get(id=int(roles))
            user.groups.add(group)
        messages.success(request, "User has assigned to the role successfully !!")
        return redirect('user-role-list')
    context = {
        'users': users,
        'all_roles': all_roles,
    }
    return render(request, 'accounts/assign-role.html', context)


def user_details_role(request):
    user_specific_role = []
    all_roles = get_all_role(request)

    print(all_roles)
    for roles in all_roles:
        # all_user = User.objects.filter(Q(groups__name=roles['name']) & Q(is_restaurant_owner=True))
        all_user = User.objects.filter(Q(groups__name=roles['name']))
        #     if request.user.is_admin:
        #         all_user = User.objects.filter(Q(groups__name=roles['name']) & Q(is_restaurant_owner=True))
        # if request.user.is_restaurant_owner:
        #     all_user = User.objects.filter(restaurant_user__restaurant=request.user.restaurant)

        print(all_user)
        for user in all_user:
            user_role_info = {
                'role_name': roles['name'],
                'user_id': user.id,
                'username': user.email,
                'role_id': roles['id'],
                'created_by': roles['created_by']
            }
            user_specific_role.append(user_role_info)
    return user_specific_role


def user_speicfic_role(request):
    if request.user.is_admin:
        all_available_user = User.objects.all()
    else:
        # restaurant = Restaurants.objects.get(user=request.user)
        # all_available_user = RestaurantsUser.objects.filter(restaurant_id=restaurant)
        # all_available_user = User.objects.filter(email=request.user.email)

        all_available_user = User.objects.all()
    all_user_role_list = []
    for user in all_available_user:
        user_group_name = []
        user = User.objects.get(id=user.id)
        user_all_group = user.groups.all()
        if user_all_group:
            for group in user_all_group:
                user_group_name.append(group.name)
        all_user_role_list.append({
            'user_id': user.id,
            'user_role': user_group_name
        })

    return all_user_role_list


@login_required()
@user_passes_test(lambda user: user.is_admin)
def userRoleList(request):
    user_specific_role = user_details_role(request)
    print(user_specific_role)
    # role_list = user_speicfic_role(request)
    # print(role_list)
    if request.user.is_admin:
        # users = User.objects.filter(is_restaurant_owner=True)
        users = User.objects.filter()
    # elif request.user.is_restaurant_owner:
    #     users = User.objects.filter(restaurant_user__restaurant=request.user.restaurant)
    context = {
        "user_role": user_specific_role,
        'users': users,
    }
    return render(request, 'accounts/user-role-list.html', context)


@login_required()
@user_passes_test(lambda user: user.is_admin)
def editUserRole(request, id):
    particular_user = get_object_or_404(User, id=id)
    specific_user_groups = particular_user.groups.all()

    if request.user.is_admin:
        all_groups = Group.objects.all()

    specific_user_info = []
    for group in all_groups:
        if group in specific_user_groups:
            specific_user_info.append({
                'role_name': group.name,
                'role_id': group.id,
                'is_group': True
            })
        else:
            specific_user_info.append({
                'role_name': group.name,
                'role_id': group.id,
                'is_group': False
            })

    context = {
        'name': particular_user.email,
        'role_info': specific_user_info,
        'user_id': id,
        "all_groups": all_groups,
    }
    if request.method == 'POST':

        selected_roles = request.POST.getlist('selected_roles')
        particular_user.groups.clear()
        for roles in selected_roles:
            group = Group.objects.get(id=int(roles))
            particular_user.groups.add(group)
        messages.add_message(request, messages.INFO, " Role has been updated successfully !!")
        return redirect('user-role-list')
    return render(request, "accounts/userAssignRoleEdit.html", context)


@login_required()
@user_passes_test(lambda user: user.is_admin)
def removeGroupUser(request, uid, role_id):
    user = User.objects.get(id=uid)
    if user:
        group = Group.objects.get(id=role_id)
        group.user_set.remove(user)
        messages.add_message(request, messages.INFO, "User is removed from the group successfully !!")
        return redirect('user-role-list')
    messages.error(request, 'You can not remove default assigned permission')
    return redirect('user-role-list')


def login_view(request):
    """
    If logged in, redirects to dashboard, else redirects to log in page
    """
    form = LoginForm(request.POST or None)
    if request.user.is_authenticated:
        return redirect('/')
    if request.method == 'POST':
        if form.is_valid():
            email_or_phone = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            print(password)
            # Authenticates using CustomAuthenticationBackend
            user = CustomAuthenticationBackend.authenticate(email_or_phone=email_or_phone, password=password)
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            messages.success(request, 'User Logged in Successfully !')
            return redirect('dashboard')

    context = {"form": form}
    return render(request, 'accounts/login.html', context)


@login_required()
@user_passes_test(lambda user: user.is_admin and user.roles == 'SUPERADMIN', login_url='/dashboard/')
def register_view(request):
    """
    If logged in, redirects to dashboard, else redirects to log in page
    """

    # if not request.user.is_admin:
    #     messages.error(request, 'Not Enough Permissions')
    #     return redirect('/')
    if request.method == 'POST':
        if request.user.is_admin:
            form = RegisterForm(request.POST, request.FILES or None)
            if form.is_valid():
                user = form.save(commit=False)
                password = form.cleaned_data['password']
                print(password)
                user.set_password(password)
                print(user.roles)
                if user.roles != 'CONTRACTOR':
                    user.is_admin = True
                user.save()
                messages.success(request, 'User Created Successfully !')
                return redirect('user-list')
            else:
                print(form.errors)
                messages.error(request, 'Not ok')
                return redirect('user-list')
    else:
        form = RegisterForm(request.POST or None)
        context = {
            'form': form,
            'menu': 'Users',
            'chain': 'add user',
        }
        return render(request, 'accounts/register.html', context)


def logout_view(request):
    """ User Log out """
    logout(request)
    return redirect('login')


def change_password(request):
    if request.method == 'POST':
        form = PasswordUpdateForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('change_password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordUpdateForm(request.user)
    return render(request, 'accounts/change_password.html', {
        'form': form
    })


class DashboardView(LoginRequiredMixin, View):
    template_name = 'dashboard/dashboard.html'

    def get(self, request, *args, **kwargs):
        new_applicants = len(LayerFormStatus.objects.filter(layer=request.user.roles))
        confirmed_applicants = len(AssessmentEvaluation.objects.filter(user=request.user, is_approved='CONFIRMED'))
        rejected_applicants = len(AssessmentEvaluation.objects.filter(user=request.user, is_approved='REJECTED'))
        print(new_applicants)

        context = {
            'customers': "Hello",
            'new_applicants': new_applicants,
            'confirmed_applicants': confirmed_applicants,
            'rejected_applicants': rejected_applicants

        }
        return render(request, self.template_name, context)


from django.core import serializers


def json_application_list(request):
    applications = LayerFormStatus.objects.filter(is_submitted=True, form_payment=True)
    qs_json = serializers.serialize('json', applications)
    return HttpResponse(qs_json, content_type='application/json')


def send_application_list(request):
    if request.user.is_admin:
        application = LayerFormStatus.objects.filter(is_submitted=True, form_payment=True)

    users = User.objects.filter(Q(roles='AS_ADMIN') | Q(roles='0S_ADMIN') | Q(roles='CO_ADMIN'))
    if request.method == 'POST':
        selected_rows = request.POST.get('selected_rows')
        send_users = request.POST.getlist('send_user')

        for user_id in send_users:
            for layer_id in selected_rows:
                LayerViewUser.objects.get_or_create(user=User.objects.get(id=user_id),
                                                    layer=LayerFormStatus.objects.get(id=layer_id))

        messages.success(request, 'Application Forwarded')
        return redirect('approved_contractor_list_view')

        applications = LayerFormStatus.objects.filter(is_submitted=True, form_payment=True, layer='DS_ADMIN')
        if applications.exists():
            for application in applications:
                for user in users:
                    user = User.objects.get(id=user)
                    LayerViewUser.objects.get_or_create(user=user, layer=application)
        # for role in roles:
        #     if role == '0S_ADMIN':
        #         for application in applications:
        #             application.has_perm_os_admin = True
        #             application.save()
        #     if role == 'AS_ADMIN':
        #         for application in applications:
        #             application.has_perm_as_admin = True
        #             application.save()
        #     if role == 'CO_ADMIN':
        #         for application in applications:
        #             application.has_perm_co_admin = True
        #             application.save()
        return redirect('approved_contractor_list_view')

    context = {
        'application': application,
        'users': users,
    }
    return render(request, 'accounts/assign-role.html', context)


class AuthorizedPerson(LoginRequiredMixin, UpdateView):
    model = AuthorizedPerson
    form_class = AuthoprisedPersonForm
    template_name = 'accounts/settings/authorized_person.html'
    success_url = '/add-auth-sign/1'

    # def get_queryset(self):
    #     return self.object.book_set.all()


def load_users(request, role):
    users = User.objects.filter(roles=role)
    return render(request, 'snippets/ajax/user_list.html', {'users': users})

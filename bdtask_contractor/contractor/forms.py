from django import forms
from django.forms import ModelForm
from .models import *
from django.db.models import Q

CHOICES = (('Yes', 'Yes'), ('No', 'No'))


class ContractorFormStep1(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContractorFormStep1, self).__init__(*args, **kwargs)
        for field_name in self.fields:
            field = self.fields.get(field_name)
            if field and isinstance(field, forms.TypedChoiceField):
                field.choices = field.choices[1:]

        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

        self.fields['contractor_name'].widget.attrs['placeholder'] = 'Ex. XYZ.traders'
        self.fields['trade_license_no'].widget.attrs['placeholder'] = 'Trade License No'
        self.fields['trade_license'].widget.attrs['placeholder'] = 'Trade License Attachment'
        self.fields['mailing_address'].widget.attrs['placeholder'] = 'Mailing address'
        self.fields['fax_no'].widget.attrs['placeholder'] = 'fax no'
        self.fields['office_address'].widget.attrs['placeholder'] = 'B-25 Mannan Plaza, 4th Floor, Khilkhet'
        self.fields['NID_NUMBER'].widget.attrs['placeholder'] = 'Ex. ১৯৮২৫৬২৪৬০৩১১২৯৪৮'

        self.fields['application_division'].widget.attrs[
            'class'] = 'radio'

        self.fields['contractor_name'].widget.attrs[
            'class'] = 'form-control form-control-solid form-control-lg kt_autosize_1'

        self.fields['NID_attachment'].widget.attrs[
            'class'] = 'custom-file-input custom-file-input-lg'

        self.fields['old_license_copy'].widget.attrs[
            'class'] = 'custom-file-input custom-file-input-lg'

        self.fields['doc_with_signature'].widget.attrs[
            'class'] = 'custom-file-input custom-file-input-lg'

        self.fields['trade_license'].widget.attrs[
            'class'] = 'custom-file-input custom-file-input-lg'

        self.fields['non_judicial_stamp'].widget.attrs[
            'class'] = 'custom-file-input custom-file-input-lg'

        self.fields['photograph'].widget.attrs[
            'class'] = 'custom-file-input custom-file-input-lg'
        self.fields['trade_license_area'].empty_label = None

    def clean(self, *args, **kwargs):
        nid = self.cleaned_data.get('NID_NUMBER')
        #
        application_division = self.cleaned_data.get('application_division')
        if not application_division:
            raise forms.ValidationError("Application division needed")

        trade_license_area = self.cleaned_data.get('trade_license_area')

        if not trade_license_area:
            raise forms.ValidationError("Trade License area needed")


        nid_length = len(str(nid))

        if nid_length == 9 or nid_length == 13 or nid_length == 17:
            pass
        else:
            raise forms.ValidationError("Please Enter a valid NID number.")

        # contractor_qs = Contractor.objects.filter(NID_NUMBER=nid, application_division=application_division)
        #
        # if contractor_qs.exists():
        #     raise forms.ValidationError("User has already register with this division")

        return super(ContractorFormStep1, self).clean(*args, **kwargs)

    class Meta:
        model = Contractor
        fields = ('__all__')
        exclude = ('user',)
        widgets = {
            'declaration_agreement': forms.CheckboxInput(attrs={'class': 'checkbox form-control'}),
            'application_division': forms.RadioSelect(attrs={'class': 'form-check-inline'}),
            'trade_license_area': forms.RadioSelect(attrs={'class': 'form-check-inline'})
        }


class ContractorPartnerForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContractorPartnerForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

    class Meta:
        model = ContractorPartners
        fields = ('__all__')
        exclude = ('contractor',)


from django.forms import formsets
from django.forms import DateInput


class DateInput(forms.DateInput):
    input_type = 'date'


class DetailsRegistrationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DetailsRegistrationForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

        self.fields['last_renewal_date'].widget.attrs['class'] = 'form-control form-control-solid kt_datepicker_1'
        # self.fields['last_renewal_date'].widget.attrs['type'] = 'date'

    class Meta:
        model = DetailsRegistration
        fields = ('__all__')
        # widgets = {
        #     'last_renewal_date': DateInput(),
        # }
        exclude = ('contractor',)


class ProfessionalTechnicalForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProfessionalTechnicalForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'
        self.fields['certificate_of_incorporation'].widget.attrs['class'] = 'custom-file-input custom-file-input-lg'
        self.fields['judicial_stamp'].widget.attrs['class'] = 'custom-file-input custom-file-input-lg'
        self.fields['general_certificate_1'].widget.attrs['class'] = 'custom-file-input custom-file-input-lg'
        self.fields['general_certificate_2'].widget.attrs['class'] = 'custom-file-input custom-file-input-lg'
        # self.fields['last_renewal_date'].widget.attrs['class'] = 'form-control form-control-solid kt_datepicker_1'
        # self.fields['last_renewal_date'].widget.attrs['type'] = 'date'

    class Meta:
        model = ProfessionalTechnical
        fields = ('__all__')
        exclude = ('contractor',)


class ListOfWorkForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ListOfWorkForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

        self.fields['start_date'].widget.attrs['class'] = 'form-control form-control-solid start_date kt_datepicker_1'
        self.fields['delivery_date'].widget.attrs[
            'class'] = 'form-control form-control-solid  deliver_date kt_datepicker_1'

    class Meta:
        model = ListOfWork
        fields = ('__all__')
        exclude = ('contractor',)


class ListOfWorkC2Form(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ListOfWorkC2Form, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

        self.fields['start_date'].widget.attrs['class'] = 'form-control form-control-solid kt_datepicker_1'
        self.fields['delivery_date'].widget.attrs['class'] = 'form-control form-control-solid kt_datepicker_1'

    class Meta:
        model = ListOfWorkC2
        fields = ('__all__')
        # widgets = {
        #     'start_date': DateInput(),
        #     'delivery_date': DateInput(),
        #
        # }
        exclude = ('contractor',)


class ListOfWorkC3Form(forms.ModelForm):
    # role_applicant = (('prime_contractor', 'Prime Contractor/Supplier'),
    #                   ('subcontractor', 'Subcontractor/Partner'),
    #                   ('partner_jvc', 'partner in JVC'))

    def __init__(self, *args, **kwargs):
        super(ListOfWorkC3Form, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

        self.fields['reason'].widget.attrs['class'] = 'form-control form-control-solid form-control-lg kt_autosize_1'
        self.fields['explanation'].widget.attrs[
            'class'] = 'form-control form-control-solid form-control-lg kt_autosize_1'
        self.fields['description_of_work'].widget.attrs[
            'class'] = 'form-control form-control-solid form-control-lg kt_autosize_1'
        self.fields['responsibility'].widget.attrs[
            'class'] = 'form-control form-control-solid form-control-lg kt_autosize_1'
        # self.fields['date'].widget.attrs['class'] = 'form-control form-control-solid kt_datepicker_1'
        # self.fields['completation_date'].widget.attrs['class'] = 'form-control form-control-solid kt_datepicker_1'
        self.fields['reason'].widget.attrs[
            'rows'] = '3'
        self.fields['explanation'].widget.attrs['rows'] = '3'

        self.fields['role'].widget.attrs['class'] = ''
        self.fields['date'].widget.attrs['class'] = 'form-control form-control-solid kt_datepicker_1'
        self.fields['completation_date'].widget.attrs['class'] = 'form-control form-control-solid kt_datepicker_1'


        # self.fields['role'] = forms.ChoiceField(label='Type', choices=self.role_applicant, required=True,
        #                                         widget=forms.RadioSelect(attrs={'class': 'form-control'}))

    # role = forms.ChoiceField(choices=role_applicant, widget=forms.RadioSelect(attrs={'class': 'form-check-inline'}))

    class Meta:
        model = ListOfWorkC3
        fields = ('__all__')
        widgets = {
            'role': forms.RadioSelect(attrs={'class': 'form-check-inline'})
        }
        exclude = ('contractor',)


class ListOfWorkC3FormView(forms.ModelForm):
    role_applicant = (('prime_contractor', 'prime_contractor'),
                      ('subcontractor', 'subcontractor'),
                      ('partner_jvc', 'partner_jvc'))

    def __init__(self, *args, **kwargs):
        super(ListOfWorkC3FormView, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'
            visible.field.widget.attrs['readonly'] = True

        self.fields['reason'].widget.attrs['class'] = 'form-control form-control-solid form-control-lg kt_autosize_1'
        self.fields['explanation'].widget.attrs[
            'class'] = 'form-control form-control-solid form-control-lg kt_autosize_1'
        self.fields['description_of_work'].widget.attrs[
            'class'] = 'form-control form-control-solid form-control-lg kt_autosize_1'
        self.fields['responsibility'].widget.attrs[
            'class'] = 'form-control form-control-solid form-control-lg kt_autosize_1'
        # self.fields['date'].widget.attrs['class'] = 'form-control form-control-solid kt_datepicker_1'
        # self.fields['completation_date'].widget.attrs['class'] = 'form-control form-control-solid kt_datepicker_1'

        self.fields['role'].widget.attrs['class'] = ''
        # self.fields['role'] = forms.ChoiceField(label='Type', choices=self.role_applicant, required=True,
        #                                         widget=forms.RadioSelect(attrs={'class': 'form-control'}))

    # role = forms.ChoiceField(choices=role_applicant, widget=forms.RadioSelect(attrs={'class': 'form-check-inline'}))

    class Meta:
        model = ListOfWorkC3
        fields = ('__all__')
        widgets = {
            'date': DateInput(),
            'completation_date': DateInput(),
            'role': forms.RadioSelect(attrs={'class': 'form-check-inline'})
        }
        exclude = ('contractor',)


class ListOfMachineryForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ListOfMachineryForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

    class Meta:
        model = ListOfMachinery
        fields = ('__all__')
        exclude = ('contractor',)


class ListOfMachineryFormD2(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ListOfMachineryFormD2, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

    class Meta:
        model = ListOfMachinery
        fields = ('__all__')
        exclude = ('contractor',)


class ListOfTechnicalForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ListOfTechnicalForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

    class Meta:
        model = ListofTechnical
        fields = ('__all__')
        exclude = ('contractor',)


class FinancialCapacityForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FinancialCapacityForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'
        self.fields['attached_file'].widget.attrs['class'] = 'custom-file-input custom-file-input-lg'
        self.fields['balance_sheet'].widget.attrs['class'] = 'custom-file-input custom-file-input-lg'
        # self.fields['year_1_total_assets'].widget.attrs['class'] = 'form-control form-control-solid form-control-sm'

    class Meta:
        model = FinancialCapacity
        fields = ('__all__')
        exclude = ('contractor',)


class BalanceSheetForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(BalanceSheetForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

    class Meta:
        model = BalanceSheet
        fields = ('__all__')
        exclude = ('contractor',)


class LegalCapacityForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(LegalCapacityForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

        self.fields['declaration_of_procurement'].widget.attrs['class'] = 'custom-file-input custom-file-input-lg'
        self.fields['declaration_form_court'].widget.attrs['class'] = 'custom-file-input custom-file-input-lg'

    class Meta:
        model = LegalCapacity
        fields = ('__all__')
        exclude = ('contractor',)


class AnnexFForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AnnexFForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

    class Meta:
        model = AnnexF
        fields = ('__all__')
        exclude = ('contractor',)


class VatForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(VatForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'
        self.fields['attached_file'].widget.attrs['class'] = 'custom-file-input custom-file-input-lg'


    class Meta:
        model = Vat
        fields = ('__all__')
        exclude = ('contractor',)


class TinForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TinForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'
        self.fields['attached_file'].widget.attrs['class'] = 'custom-file-input custom-file-input-lg'

    class Meta:
        model = Tin
        fields = ('__all__')
        exclude = ('contractor',)


class TexationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TexationForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

        # self.fields['applicant_date'].widget.attrs['class'] = 'form-control form-control-solid kt_datepicker_1'
        self.fields['evidence_of_fulfillment'].widget.attrs['class'] = 'custom-file-input custom-file-input-lg'
        self.fields['signature_of_applicant'].widget.attrs['class'] = 'custom-file-input custom-file-input-lg'

    class Meta:
        model = Texation
        fields = ('__all__')
        exclude = ('contractor', 'declaration', 'applicant_date')


class AssessmentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AssessmentForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'

    class Meta:
        model = Assessment
        fields = ('__all__')
        exclude = ('contractor',)


class AssessmentMemberForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AssessmentMemberForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'
            visible.field.widget.attrs['readonly'] = True

    class Meta:
        model = Assessment
        fields = ('__all__')
        exclude = ('contractor',)


class AssessmentEvaluationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AssessmentEvaluationForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'
        self.fields['is_approved'].widget.attrs['class'] = 'form-check-input'

    class Meta:
        model = AssessmentEvaluation
        fields = ('__all__')
        exclude = ('user',)


class ContractorListEvaluationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContractorListEvaluationForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'
        self.fields['comments'].widget.attrs['placeholder'] = 'Write Your Comments'

    class Meta:
        model = ContractorListEvaluation
        fields = ('__all__')
        exclude = ('user',)


class MettingForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MettingForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'
            self.fields['metting_date'].widget.attrs[
                'class'] = 'form-control form-control-solid date_picker kt_datepicker_1'
        # self.fields['attached_file'].widget.attrs['class'] = 'custom-file-label custom-file-label-solid custom-file-label-lg'

    class Meta:
        model = Metting
        fields = ('__all__')
        # widgets = {
        #     'metting_date': DateInput(),
        # }


class ChalanForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ChalanForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'
        self.fields['contractor'].widget.attrs['class'] = 'select2 form-control'
        # self.fields['chalan_type'].widget.attrs['class'] = 'select2 form-control'
        self.fields['class_type'].widget.attrs['class'] = 'select2 form-control'
        # self.fields['contractor_group_type'].widget.attrs['class'] = 'select2 form-control'
        self.fields['contractor'].empty_label = '---বাছাই করুন---'
        self.fields['class_type'].empty_label = '---বাছাই করুন---'
        # self.fields['chalan_type'].empty_label = '---বাছাই করুন---'
        # self.fields['contractor_group_type'].empty_label = '---বাছাই করুন---'
        self.fields['status'].widget.attrs['class'] = 'form-check-input'

    class Meta:
        model = Chalan
        fields = ('__all__')
        exclude = ('chalan_type', 'contractor_group_type', 'address', 'money_in_words')


class LicenseForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(LicenseForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-solid'
        self.fields['contractor'].widget.attrs['class'] = 'select2 form-control'

    class Meta:
        model = License
        fields = '__all__'

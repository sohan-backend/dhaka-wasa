from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from django.urls import reverse

User = get_user_model()
from django.utils import timezone


# Create your models here.


class Contractor(models.Model):
    application_choices = (('enrollment', 'enrollment'),
                           ('renewal', 'renewal'),
                           ('upgrade', 'upgrade'))

    application_division = (
        ('3', 'Civil'),
        ('1', 'Civil and Supplier'),
        ('4', 'Electrical & Mechanical'),
        ('2', 'Electrical & Mechanical and Supplier'),
        ('5', 'Supplier')
    )

    trade_license_area = (
        ('dhaka_north', 'Dhaka North City Corporation'),
        ('dhaka_south', 'Dhaka South City Corporation '),
        ('narayanganj', 'Narayanganj City Corporation'))

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    proprietor_name = models.CharField(max_length=50, blank=True, null=True)
    contractor_name = models.CharField(max_length=50, blank=True)
    fathers_name = models.CharField(max_length=255, blank=True, null=True)
    mothers_name = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    position_in_firm = models.CharField(max_length=55, blank=True, null=True)
    mailing_address = models.CharField(max_length=255, blank=True)
    office_address = models.CharField(max_length=255, blank=True)
    # fax_no = models.CharField(max_length=255, blank=True)
    fax_no = models.IntegerField(blank=True, null=True)
    doc_with_signature = models.FileField(upload_to='Documents/', blank=True)
    NID_attachment = models.FileField(upload_to='NID/', blank=True)
    NID_NUMBER = models.IntegerField(blank=True, null=True)
    trade_license_no = models.CharField(max_length=255, blank=True)
    trade_license = models.FileField(upload_to='trade_license/', blank=True)
    non_judicial_stamp = models.FileField(upload_to='stamp/', blank=True)
    has_partners = models.BooleanField(default=False)
    declaration_agreement = models.BooleanField(default=False)
    photograph = models.ImageField(upload_to='photograph/', blank=True)
    applied_for = models.CharField(max_length=55, choices=application_choices, blank=True, null=True)
    application_division = models.CharField(max_length=55, choices=application_division, blank=True, null=True)
    trade_license_area = models.CharField(max_length=55, choices=trade_license_area, blank=True, null=True)
    old_license_number = models.CharField(max_length=255, blank=True, null=True)
    old_license_copy = models.FileField(blank=True, upload_to='license/')
    class_name = models.CharField(max_length=10, blank=True, null=True)
    upgraded_class = models.CharField(max_length=10, blank=True, null=True)

    def __str__(self):
        return str(self.contractor_name) + ' - ' + str(self.applied_for) + ' - ' + str(
            self.get_application_division_display())


@receiver(post_save, sender=User)
def contractor_from_user_registration(sender, created, instance, *args, **kwargs):
    if created and instance and instance.roles == 'CONTRACTOR':
        Contractor.objects.create(user=instance)


post_save.connect(contractor_from_user_registration, sender=User)


class ContractorPartners(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    partner_name = models.CharField(max_length=255)
    fathers_name = models.CharField(max_length=255, blank=True)
    address = models.CharField(max_length=255, blank=True)
    position_in_firm = models.CharField(max_length=55, blank=True)
    mothers_name = models.CharField(max_length=55, blank=True)

    def __str__(self):
        return self.partner_name


class ProfessionalTechnical(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    certificate_of_incorporation = models.FileField(upload_to='certificate_of_incorporation/', blank=True)
    judicial_stamp = models.FileField(upload_to='judicial_stamp/', blank=True, null=True)
    has_registered = models.BooleanField(default=False)
    has_general_certificate = models.BooleanField(default=False)
    general_certificate_1 = models.FileField(upload_to='general_certificate/', blank=True)
    general_certificate_2 = models.FileField(upload_to='general_certificate/', blank=True)
    list_of_work_annex_c1 = models.BooleanField(default=False)
    list_of_work_annex_c2 = models.BooleanField(default=False)
    list_of_work_annex_c3 = models.BooleanField(default=False)
    list_of_work_annex_d1 = models.BooleanField(default=False)
    desc_of_work_annex_d2 = models.BooleanField(default=False)
    list_of_work_e1 = models.BooleanField(default=False)

    def __str__(self):
        return str(self.contractor)


class DetailsRegistration(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    name_of_dept = models.CharField(max_length=55, blank=True)
    enlisted_class = models.CharField(max_length=55, blank=True)
    last_renewal_date = models.DateTimeField(blank=True, null=True)
    enlistment_date_RegNo = models.CharField(max_length=255, blank=True)
    file_attachment = models.FileField(upload_to='details_registration/', blank=True)

    def __str__(self):
        return self.name_of_dept


class ListOfWork(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    name_of_work = models.CharField(max_length=255)
    client = models.CharField(max_length=255)
    contractor_amount = models.IntegerField()
    start_date = models.DateTimeField(blank=True, null=True)
    delivery_date = models.DateTimeField(blank=True, null=True)
    remarks = models.CharField(max_length=255)
    contact_no = models.CharField(max_length=255)
    completion_certificate = models.FileField(upload_to='details_registration/', blank=True)

    def __str__(self):
        return self.name_of_work


class ListOfWorkC2(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    name_of_work = models.CharField(max_length=255)
    contact_no = models.CharField(max_length=55)
    client = models.CharField(max_length=255)
    contact_amount = models.IntegerField()
    start_date = models.DateTimeField(blank=True, null=True)
    delivery_date = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=55)
    progress_certificate = models.FileField(upload_to='details_registration/', blank=True)

    def __str__(self):
        return self.name_of_work


class ListOfWorkC3(models.Model):
    role_applicant = (('Prime Contractor/Supplier', 'Prime Contractor/Supplier'),
                      ('Subcontractor/Partner', 'Subcontractor/Partner'),
                      ('partner in JVC', 'partner in JVC'))
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    contact_name = models.CharField(max_length=255)
    contact_number = models.CharField(max_length=255)
    name_of_client = models.CharField(max_length=255)
    date = models.DateTimeField(blank=True, null=True)
    address = models.CharField(max_length=255)
    role = models.CharField(max_length=55, choices=role_applicant, default='prime_contractor')
    amount_of_contact = models.IntegerField()
    applicant_share = models.IntegerField()
    reason = models.TextField()
    date_commencement_contract = models.CharField(max_length=255)
    time_for_completion = models.CharField(max_length=255)
    completation_date = models.DateTimeField()
    explanation = models.TextField()
    description_of_work = models.TextField()
    responsibility = models.TextField()

    def __str__(self):
        return str(self.contact_name)


class ListOfMachinery(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    name_of_eqipment = models.CharField(max_length=55, blank=True)
    manufacture = models.CharField(max_length=55, blank=True)
    capacity = models.CharField(max_length=55, blank=True)
    year_of_model = models.CharField(max_length=55, blank=True)
    status = models.CharField(max_length=55, blank=True)
    ownership = models.CharField(max_length=55, blank=True)
    remarks = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.name_of_eqipment


class ListOfMachineryD2(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    name_of_eqipment = models.CharField(max_length=55, blank=True)
    manufacture = models.CharField(max_length=55, blank=True)
    capacity = models.CharField(max_length=55, blank=True)
    year_of_model = models.CharField(max_length=55, blank=True)
    status = models.CharField(max_length=55, blank=True)
    ownership = models.CharField(max_length=55, blank=True)
    remarks = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.name_of_eqipment


class ListofTechnical(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE, blank=True, null=True)
    name_of_employee = models.CharField(max_length=255, blank=True)
    designation = models.CharField(max_length=255, blank=True)
    file = models.FileField(upload_to='technical_personal')

    def __str__(self):
        return self.name_of_employee


class FinancialCapacity(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    name_of_the_banker = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    telephone = models.CharField(max_length=255)
    fax = models.CharField(max_length=255)
    email = models.EmailField()
    contact_name = models.CharField(max_length=255)
    attached_file = models.FileField(upload_to='attached-file/', blank=True)
    year_1_total_assets = models.FloatField()
    year_2_total_assets = models.FloatField()
    year_3_total_assets = models.FloatField()
    total_assets_remarks = models.CharField(max_length=255, blank=True)
    year_1_current_assets = models.FloatField()
    year_2_current_assets = models.FloatField()
    year_3_current_assets = models.FloatField()
    total_current_remarks = models.CharField(max_length=255, blank=True)
    year_1_total_liabilities = models.FloatField()
    year_2_total_liabilities = models.FloatField()
    year_3_total_liabilities = models.FloatField()
    remarks_total_liabilities = models.CharField(max_length=255, blank=True)
    year_1_current_liabilities = models.FloatField()
    year_2_current_liabilities = models.FloatField()
    year_3_current_liabilities = models.FloatField()
    remarks_current_liabilities = models.CharField(max_length=255, blank=True)
    year_1_profit_before_tax = models.FloatField()
    year_2_profit_before_tax = models.FloatField()
    year_3_profit_before_tax = models.FloatField()
    remarks_profit_before_tax = models.CharField(max_length=255, blank=True)
    year_1_profit_after_tax = models.FloatField()
    year_2_profit_after_tax = models.FloatField()
    year_3_profit_after_tax = models.FloatField()
    remarks_profit_after_tax = models.CharField(max_length=255, blank=True)
    balance_sheet = models.FileField(upload_to='balance_sheet/', blank=True)

    def __str__(self):
        return self.name_of_the_banker


class BalanceSheet(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    fixed_assets_less_accumulated = models.DecimalField(max_digits=20, decimal_places=2)
    depreciation = models.DecimalField(max_digits=20, decimal_places=2)
    capital_work = models.DecimalField(max_digits=20, decimal_places=2)
    deferred_expenditure_1 = models.DecimalField(max_digits=20, decimal_places=2)
    deferred_expenditure_2 = models.DecimalField(max_digits=20, decimal_places=2)
    investment_1 = models.DecimalField(max_digits=20, decimal_places=2)
    investment_2 = models.DecimalField(max_digits=20, decimal_places=2)
    drainage_maintenance_1 = models.DecimalField(max_digits=20, decimal_places=2)
    drainage_maintenance_2 = models.DecimalField(max_digits=20, decimal_places=2)
    balance_subtotal_1 = models.DecimalField(max_digits=20, decimal_places=2)
    balance_subtotal_2 = models.DecimalField(max_digits=20, decimal_places=2)
    investors_1 = models.DecimalField(max_digits=20, decimal_places=2)
    investors_2 = models.DecimalField(max_digits=20, decimal_places=2)
    rates_receivable_1 = models.DecimalField(max_digits=20, decimal_places=2)
    rates_receivable_2 = models.DecimalField(max_digits=20, decimal_places=2)
    advances_deposits_1 = models.DecimalField(max_digits=20, decimal_places=2)
    advances_deposits_2 = models.DecimalField(max_digits=20, decimal_places=2)
    prepayments_1 = models.DecimalField(max_digits=20, decimal_places=2)
    prepayments_2 = models.DecimalField(max_digits=20, decimal_places=2)
    cash_bank_balance_1 = models.DecimalField(max_digits=20, decimal_places=2)
    cash_bank_balance_2 = models.DecimalField(max_digits=20, decimal_places=2)
    current_subtotal_1 = models.DecimalField(max_digits=20, decimal_places=2)
    current_subtotal_2 = models.DecimalField(max_digits=20, decimal_places=2)
    current_total_assests_1 = models.DecimalField(max_digits=20, decimal_places=2)
    current_total_assests_2 = models.DecimalField(max_digits=20, decimal_places=2)
    capital_fund_1 = models.DecimalField(max_digits=20, decimal_places=2)
    capital_fund_2 = models.DecimalField(max_digits=20, decimal_places=2)
    other_fund_1 = models.DecimalField(max_digits=20, decimal_places=2)
    other_fund_2 = models.DecimalField(max_digits=20, decimal_places=2)
    revaluation_of_assets_1 = models.DecimalField(max_digits=20, decimal_places=2)
    revaluation_of_assets_2 = models.DecimalField(max_digits=20, decimal_places=2)
    retained_earnings_1 = models.DecimalField(max_digits=20, decimal_places=2)
    retained_earnings_2 = models.DecimalField(max_digits=20, decimal_places=2)

    equity_subtotal_1 = models.DecimalField(max_digits=20, decimal_places=2)
    equity_subtotal_2 = models.DecimalField(max_digits=20, decimal_places=2)

    liabilities_for_other_finance_1 = models.DecimalField(max_digits=20, decimal_places=2)
    liabilities_for_other_finance_2 = models.DecimalField(max_digits=20, decimal_places=2)
    vat_tax_payable_1 = models.DecimalField(max_digits=20, decimal_places=2)
    vat_tax_payable_2 = models.DecimalField(max_digits=20, decimal_places=2)
    non_current_subtotal_1 = models.DecimalField(max_digits=20, decimal_places=2)
    non_current_subtotal_2 = models.DecimalField(max_digits=20, decimal_places=2)
    total_equity_1 = models.DecimalField(max_digits=20, decimal_places=2)
    total_equity_2 = models.DecimalField(max_digits=20, decimal_places=2)

    def __str__(self):
        return str(self.contractor)


class LegalCapacity(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    declaration_of_procurement = models.FileField(upload_to='declaration_of_procurement/', blank=True)
    declaration_form_court = models.FileField(upload_to='declaration_from_court/', blank=True)
    enclosed_Annex_F = models.BooleanField(default=False)

    def __str__(self):
        return str(self.contractor)


class AnnexF(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    year = models.CharField(max_length=55, blank=True)
    award = models.CharField(max_length=55, blank=True)
    course_of_litigation = models.CharField(max_length=55)
    amount = models.IntegerField()

    def __str__(self):
        return str(self.year)


class Vat(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    vat_no = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    attached_file = models.FileField()


class Tin(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    tin_no = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    attached_file = models.FileField(max_length=255)


class Texation(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    evidence_of_fulfillment = models.FileField(upload_to='evidence_of_fulfillment/')
    declaration = models.BooleanField(default=False)
    # applicant_date = models.DateTimeField()
    signature_of_applicant = models.FileField('signature_of_applicant/')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class ContractorForm(models.Model):
    PENDING = 1
    COMPLETED = 2
    payment_status = (
        (PENDING, 'pending'),
        (COMPLETED, 'completed'),
    )

    contractor_form = models.OneToOneField(Contractor, on_delete=models.CASCADE, related_name='contractor')
    payment_status = models.IntegerField(choices=payment_status, default=1)
    approved_by_committee_layer = models.BooleanField(default=False)
    approved_by_secretary_layer = models.BooleanField(default=False)
    approved_by_dmd_layer = models.BooleanField(default=False)
    approved_by_md_layer = models.BooleanField(default=False)

    def __str__(self):
        return str(self.contractor_form)


class Assessment(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    authentic_information_documents = models.CharField(max_length=55, blank=True)
    authentic_information_documents_notes = models.CharField(max_length=55, blank=True)
    application_to_submit = models.CharField(max_length=55, blank=True)
    application_to_submit_notes = models.CharField(max_length=55, blank=True)
    experience_successful_execution_1 = models.CharField(max_length=55, blank=True)
    experience_successful_execution_1_notes = models.CharField(max_length=55, blank=True)
    experience_successful_execution_2 = models.CharField(max_length=55, blank=True)
    experience_successful_execution_2_notes = models.CharField(max_length=55, blank=True)
    workshop_facilities_1 = models.CharField(max_length=55, blank=True)
    workshop_facilities_1_notes = models.CharField(max_length=55, blank=True)
    workshop_facilities_2 = models.CharField(max_length=55, blank=True)
    workshop_facilities_2_notes = models.CharField(max_length=55, blank=True)
    ability_to_provide_required_personnel_1 = models.CharField(max_length=55, blank=True)
    ability_to_provide_required_personnel_1_notes = models.CharField(max_length=55, blank=True)
    ability_to_provide_required_personnel_2 = models.CharField(max_length=55, blank=True)
    ability_to_provide_required_personnel_2_notes = models.CharField(max_length=55, blank=True)
    adequacy_of_the_financial_strength = models.CharField(max_length=55, blank=True)
    adequacy_of_the_financial_strength_notes = models.CharField(max_length=55, blank=True)
    ability_in_financing_of_works = models.CharField(max_length=55, blank=True)
    ability_in_financing_of_works_notes = models.CharField(max_length=55, blank=True)
    information_and_docs = models.CharField(max_length=55, blank=True)
    information_and_docs_notes = models.CharField(max_length=55, blank=True)
    eligibility_of_the_applicant = models.CharField(max_length=55, blank=True)
    eligibility_of_the_applicant_notes = models.CharField(max_length=55, blank=True)
    acceptability_of_applicant = models.CharField(max_length=55, blank=True)
    acceptability_of_applicant_notes = models.CharField(max_length=55, blank=True)
    proof_of_fulfilment = models.CharField(max_length=55, blank=True)
    proof_of_fulfilment_notes = models.CharField(max_length=55, blank=True)
    is_submitted = models.BooleanField(default=False)

    def __str__(self):
        return str(self.contractor)


class AssessmentEvaluation(models.Model):
    form_status = (
        ('PENDING', 'PENDING'),
        ('CONFIRMED', 'CONFIRMED'),
        ('REJECTED', 'REJECTED'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    assessment = models.ForeignKey(Assessment, on_delete=models.CASCADE)
    is_approved = models.CharField(max_length=20, choices=form_status, default='PENDING')

    def __str__(self):
        return str(self.user) + ' - ' + str(self.assessment)


class ContractorListEvaluation(models.Model):
    form_status = (
        ('PENDING', 'PENDING'),
        ('CONFIRMED', 'CONFIRMED'),
        ('REJECTED', 'REJECTED'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    committee_secretary_comments = models.CharField(max_length=255, blank=True, null=True)
    is_approved = models.CharField(max_length=20, choices=form_status, default='PENDING')

    def __str__(self):
        return str(self.user) + ' - ' + str(self.user)


class MemberList(models.Model):
    contractor_list_id = models.ForeignKey(ContractorListEvaluation, on_delete=models.CASCADE)
    contractor_id = models.ForeignKey(Contractor, on_delete=models.CASCADE)


# class CommitteeLayer(models.Model):
#     PENDING = 0
#     REVIEW = 1
#     REJECTED = 2
#     CONFIRMED = 1
#
#
#     COMMITTEE_SECRETARY = 1
#     COMMITTEE_MEMBER = 2
#
#     LAYER = (
#         (COMMITTEE_SECRETARY, 'COMMITTEE_SECRETARY'),
#         (COMMITTEE_MEMBER, 'COMMITTEE_MEMBER'),
#     )
#     layer = models.IntegerField(choices=LAYER, default=1)
#     contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
#     form_status = models.IntegerField(choices=form_status, default=0)
#
#     def __str__(self):
#         return str(self.contractor) + ' - ' + str(
#             self.get_layer_display() + ' - ' + str(self.get_form_status_display()))


class Metting(models.Model):
    metting_title = models.CharField(max_length=255)
    metting_date = models.DateTimeField(verbose_name='Metting date & Time')
    fiscal_year = models.CharField(max_length=255)
    metting_room = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return str(self.metting_title)


class MettingMembers(models.Model):
    metting = models.ForeignKey(Metting, on_delete=models.CASCADE, related_name='metting_members')
    member = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.member) + ' - ' + str(self.metting)


class LayerFormStatus(models.Model):
    form_status = (
        ('PENDING', 'PENDING'),
        ('REVIEW', 'REVIEW'),
        ('CONFIRMED', 'CONFIRMED'),
        ('REJECTED', 'REJECTED'),
    )

    LAYER = (
        ('DS_ADMIN', 'DS_ADMIN'),
        ('AS_ADMIN', 'AS_ADMIN'),
        ('0S_ADMIN', '0S_ADMIN'),
        ('CO_ADMIN', 'CO_ADMIN'),
        ('COMMITTEE_SECRETARY', 'COMMITTEE_SECRETARY'),
        ('COMMITTEE_MEMBER', 'COMMITTEE_MEMBER'),
        ('SECRETARY', 'SECRETARY'),
        ('DMD', 'DMD'),
        ('MD', 'MD')
    )
    layer = models.CharField(choices=LAYER, max_length=20, default='COMMITTEE_SECRETARY')
    dict = models.CharField(max_length=255, blank=True, null=True)
    contractor = models.OneToOneField(Contractor, on_delete=models.CASCADE, related_name='layer')
    form_status = models.CharField(choices=form_status, max_length=20, default='PENDING')
    COMMITTEE_SECRETARY_comments = models.CharField(max_length=255, blank=True, null=True)
    SECRETARY_comments = models.CharField(max_length=255, blank=True, null=True)
    DMD_comments = models.CharField(max_length=255, blank=True, null=True)
    MD_comments = models.CharField(max_length=255, blank=True, null=True)
    has_perm_as_admin = models.BooleanField(default=False)
    has_perm_os_admin = models.BooleanField(default=False)
    has_perm_co_admin = models.BooleanField(default=False)
    is_submitted = models.BooleanField(default=False)
    renewal_submitted = models.BooleanField(default=False)
    upgrade_submitted = models.BooleanField(default=False)
    form_payment = models.BooleanField(default=False)
    renewal_form_payment = models.BooleanField(default=False)
    upgrade_form_payment = models.BooleanField(default=False)
    chalan_payment = models.BooleanField(default=False)
    meetting = models.ForeignKey(Metting, on_delete=models.DO_NOTHING, blank=True, null=True)
    class_name = models.CharField(max_length=10, blank=True, null=True)
    tracking_id = models.CharField(max_length=255, blank=True, null=True)
    application_approved = models.BooleanField(default=False)
    license_created = models.BooleanField(default=False)

    def __str__(self):
        return str(self.contractor) + ' - ' + str(self.layer) + ' - ' + str(self.form_status)


@receiver(post_save, sender=LayerFormStatus)
def contractor_layer_form_save(sender, instance, *args, **kwargs):
    # if committee secretary send for review to members
    if instance.form_status == 'REVIEW' and instance.layer == 'COMMITTEE_SECRETARY':
        instance.layer = 'COMMITTEE_MEMBER'
        instance.form_status = 'PENDING'
        instance.save()

    # if committee secretary confirmed the application
    # if instance.form_status == 'CONFIRMED' and instance.layer == 'COMMITTEE_SECRETARY':
    #     instance.layer = 'SECRETARY'
    #     instance.form_status = 'PENDING'
    #     instance.save()
    #
    # # if  secretary confirmed application
    # if instance.form_status == 'CONFIRMED' and instance.layer == 'SECRETARY':
    #     instance.layer = 'DMD'
    #     instance.form_status = 'PENDING'
    #     instance.save()
    #
    # # if  secretary rejected application
    # if instance.form_status == 'REJECTED' and instance.layer == 'SECRETARY':
    #     instance.layer = 'COMMITTEE_SECRETARY'
    #     instance.form_status = 'PENDING'
    #     instance.save()
    #
    # # if committee dmd confirmed the application
    # if instance.form_status == 'CONFIRMED' and instance.layer == 'DMD':
    #     instance.layer = 'MD'
    #     instance.form_status = 'PENDING'
    #     instance.save()
    #
    # # if committee dmd rejected the application
    # if instance.form_status == 'REJECTED' and instance.layer == 'DMD':
    #     instance.layer = 'SECRETARY'
    #     instance.form_status = 'PENDING'
    #     instance.save()

    # IF MD REJECTED
    if instance.form_status == 'REJECTED' and instance.layer == 'MD':
        instance.layer = 'DMD'
        instance.form_status = 'PENDING'
        instance.save()


post_save.connect(contractor_layer_form_save, sender=LayerFormStatus)


@receiver(post_save, sender=Contractor)
def contractor_form_save(sender, created, instance, *args, **kwargs):
    if created:
        # ContractorForm.objects.create(contractor_form=instance)
        LayerFormStatus.objects.create(layer='DS_ADMIN', contractor=instance)


post_save.connect(contractor_form_save, sender=Contractor)


class LayerViewUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    layer = models.ForeignKey(LayerFormStatus, on_delete=models.CASCADE, related_name='layer_view')

    def __str__(self):
        return str(self.user)


# class ContractorListForward(models.Model):
#     comments = models.CharField(max_length=255, blank=True, null=True)
#     members = models.ForeignKey(User, on_delete=models.CASCADE)
#
#     def __str__(self):
#         return str(self.members) + str(self.comments)
#
#
# class ContractorListLayerFormStatus(models.Model):
#     form_status = (
#         ('PENDING', 'PENDING'),
#         ('REVIEW', 'REVIEW'),
#         ('CONFIRMED', 'CONFIRMED'),
#         ('REJECTED', 'REJECTED'),
#     )
#
#     LAYER = (
#         ('COMMITTEE_SECRETARY', 'COMMITTEE_SECRETARY'),
#         ('COMMITTEE_MEMBER', 'COMMITTEE_MEMBER'),
#         ('SECRETARY', 'SECRETARY'),
#         ('DMD', 'DMD'),
#         ('MD', 'MD')
#     )
#     layer = models.CharField(choices=LAYER, max_length=20, default='COMMITTEE_SECRETARY')
#     form_status = models.CharField(choices=form_status, max_length=20, default='PENDING')
#     contractor_list = models.ForeignKey(ContractorListForward,on_delete=models.CASCADE)
#
#     def __str__(self):
#         return str(self.contractor) + ' - ' + str(self.layer) + ' - ' + str(self.form_status)


class Transaction(models.Model):
    transaction_type = (
        (1, 'Application Fee'),
        (2, 'Chalan Bill'),
        (3, 'application_fee_renewal'),
        (4, 'application_fee_upgrade'),
        (5, 'chalan_fee_renewal'),
        (6, 'chalan_fee_upgrade'),)

    tran_id = models.CharField(max_length=255, blank=True, null=True)
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    paid_amount = models.FloatField(blank=True, null=True)
    transaction_type = models.IntegerField(choices=transaction_type, default=1)
    status = models.BooleanField(default=False)

    def __str__(self):
        return str(self.contractor)


class Chalan(models.Model):
    chalan_type = (
        ('', '---বাছাই করুন---'),
        ('enrollment', 'ঠিকাদারী লাইসেন্স তালিকাভুক্তি ফি জমাদান প্রসঙ্গে। '),
        ('renewal', 'তালিকাভুক্ত ঠিকাদারী লাইসেন্স শ্রেণী উন্নতিকরণ প্রসঙ্গে ।'),
        ('upgrade', 'ঠিকাদারী লাইসেন্স নবায়ন ফি পরিশোধ করণ প্রসঙ্গে |'),
    )
    class_type = (
        ('', '---বাছাই করুন---'),
        ('first_class', '১ম'),
        ('second_class', '২য়'),
        ('third_class', '৩য়')
    )

    group_type = (
        ('', '---বাছাই করুন---'),
        ('civil', 'সিভিল'),
        ('electrical_mechanical', 'ইলেকট্রিকাল এন্ড মেকানিক্যাল'),

    )

    chalan_type = models.CharField(max_length=55, choices=chalan_type, blank=True, null=True)
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE, related_name='chalan_contractor')
    memorandum_no = models.CharField(null=True, blank=True, max_length=255)
    issue_date = models.DateTimeField(blank=True, null=True)
    address = models.TextField()
    class_type = models.CharField(max_length=55, choices=class_type, blank=True, null=True, verbose_name='Class')
    fiscal_year = models.CharField(null=True, blank=True, max_length=255)

    fee = models.FloatField()
    money_in_words = models.CharField(null=True, blank=True, max_length=255)
    last_submission_date = models.DateTimeField(blank=True, null=True)
    contractor_group_type = models.CharField(max_length=55, choices=group_type, blank=True, null=True)
    not_renewal_fiscal_year = models.CharField(null=True, blank=True, max_length=255)
    license_no = models.CharField(null=True, blank=True, max_length=255)
    fine_rate = models.CharField(null=True, blank=True, max_length=255)
    total_fine = models.CharField(null=True, blank=True, max_length=255)
    total_taka = models.CharField(null=True, blank=True, max_length=255)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}-{}'.format(self.contractor, self.chalan_type)

    def get_absolute_url(self):
        return f"/print_preview/{self.id}"


class License(models.Model):
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE, related_name='license_contractor')
    enlishment_no = models.CharField(null=True, blank=True, max_length=255)
    enlishment_year = models.CharField(null=True, blank=True, max_length=255)
    upgradataion_or_renewal_year = models.CharField(null=True, blank=True, max_length=255)

    def __str__(self):
        return self.enlishment_no

    def get_absolute_url(self):
        return f"/print_license/{self.id}"

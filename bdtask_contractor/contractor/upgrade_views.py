import json

from django.shortcuts import render, get_object_or_404

from .models import *

# Create your views here.
from django.views.generic.base import View
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.shortcuts import HttpResponse
from django.contrib.auth.decorators import login_required
from .models import Contractor
from .forms import *
from django.shortcuts import redirect, get_object_or_404
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.forms import formset_factory
from django.forms import modelformset_factory
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.db.models import Q
import requests
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import ListView
from accounts.models import AuthorizedPerson, SSLSettings
from django.views.generic.detail import DetailView
from django.http import HttpResponseRedirect

from django.http import JsonResponse
import uuid


class UpgradeView(View):
    template_name = 'step-1-upgrade.html'
    queryset = ContractorPartners.objects.all()
    PartnerFormSet = modelformset_factory(ContractorPartners, form=ContractorPartnerForm, extra=1)

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        tracking_number = request.session.get('tracking_number')
        if tracking_number:
            tracking_no = LayerFormStatus.objects.filter(tracking_id=tracking_number)
            if tracking_no.exists():
                contractor = Contractor.objects.get(layer=tracking_no[0])

                form = ContractorFormStep1(instance=contractor)
                formset = self.PartnerFormSet(queryset=ContractorPartners.objects.filter(contractor=contractor))

        else:
            contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=False)

            if contractor.exists():

                contractor_obj = contractor.first()

                form = ContractorFormStep1(instance=contractor_obj)
                formset = self.PartnerFormSet(queryset=ContractorPartners.objects.filter(contractor=contractor_obj,
                                                                                         contractor__layer__is_submitted=False))

            else:
                form = ContractorFormStep1()
                formset = self.PartnerFormSet(queryset=ContractorPartners.objects.none())

        context = {
            'form': form,
            'formset': formset,
            'contractor': contractor,
            'type': 'upgrade'
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        template_name = 'step-1-upgrade.html'

        if 'contractor_form' in request.POST:
            tracking_number = request.session.get('tracking_number')
            if tracking_number:
                tracking_no = LayerFormStatus.objects.filter(tracking_id=tracking_number)
                if tracking_no.exists():
                    contractor = Contractor.objects.get(layer=tracking_no[0])
                    form = ContractorFormStep1(request.POST, request.FILES, instance=contractor)
                    formset = self.PartnerFormSet(request.POST, request.FILES,
                                                  queryset=ContractorPartners.objects.filter(contractor=contractor))

                    if form.is_valid() and formset.is_valid():
                        contractor = form.save(commit=False)
                        contractor.applied_for = 'upgrade'
                        contractor.user = request.user
                        radios = request.POST.get('radios2')
                        if radios == 'on':
                            contractor.has_partners = True
                        else:
                            contractor.has_partners = False

                        contractor.save()

                        if contractor.has_partners:
                            instances = formset.save(commit=False)
                            for instance in instances:
                                instance.contractor = contractor
                                instance.save()

                        return redirect('license-upgrade-step-2')

            else:
                formset = self.PartnerFormSet(request.POST, request.FILES)
                radios = request.POST.get('radios2')

                contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=False)

                if contractor.exists():
                    contractor_obj = contractor.last()
                    form = ContractorFormStep1(request.POST, request.FILES, instance=contractor_obj)

                else:
                    form = ContractorFormStep1(request.POST, request.FILES)

                if form.is_valid() and formset.is_valid():
                    contractor = form.save(commit=False)
                    contractor.applied_for = 'renewal'
                    contractor.user = request.user

                    if radios == 'on':
                        contractor.has_partners = True
                    else:
                        contractor.has_partners = False

                    contractor.save()

                    if contractor.has_partners:
                        instances = formset.save(commit=False)
                        for instance in instances:
                            instance.contractor = contractor
                            instance.save()

                    return redirect('license-upgrade-step-2')

                context = {
                    'form': form,
                    'formset': formset
                }
                return render(request, self.template_name, context)

        if 'search' in request.POST:
            tracking_number = request.POST.get('tracking_no')
            tracking_no = LayerFormStatus.objects.filter(tracking_id=tracking_number)
            if tracking_no.exists():
                contractor = Contractor.objects.get(layer=tracking_no[0])

                form = ContractorFormStep1()
                formset = self.PartnerFormSet()
                context = {
                    'form': form,
                    'formset': formset,
                    'contractor': contractor,
                    'type': 'renewal',
                    'tracking_number': tracking_number,
                }
                messages.success(request,
                                 '{} Contractor Found with that Tracking Number:'.format(contractor.contractor_name))
                return render(request, template_name, context)
            else:
                messages.error(request, 'No Tracking No Found')
                return redirect('license-upgrade-step-1')

        if 'load' in request.POST:
            tracking_number = request.POST.get('tracking_no')
            tracking_no = LayerFormStatus.objects.filter(tracking_id=tracking_number)
            if tracking_no.exists():
                contractor = Contractor.objects.get(layer=tracking_no[0])

                form = ContractorFormStep1(instance=contractor)
                formset = self.PartnerFormSet(queryset=ContractorPartners.objects.filter(contractor=contractor))

                request.session['tracking_number'] = tracking_number
                messages.success(request,
                                 '{} Contractor Data Loaded throughout the systems'.format(contractor.contractor_name))
                return redirect('license-upgrade-step-1')

                # context = {
                #     'form': form,
                #     'formset': formset,
                #     'contractor': contractor,
                #     'type': 'renewal',
                #     'tracking_number': tracking_number,
                # }
                #
                # return render(request, template_name, context)

        if 'clear' in request.POST:
            try:
                del request.session['tracking_number']
                messages.success(request, 'Data Reset throughout the systems')
            except KeyError as e:
                messages.success(request, 'Not Found')
                print("The requested Session variable is already deleted")
            return redirect('license-upgrade-step-1')


class UpgradeViewStep2(View):
    template_name = 'step-2-upgrade.html'
    detailsRegFormset = modelformset_factory(DetailsRegistration, form=DetailsRegistrationForm, extra=1)
    listofworkFormset = modelformset_factory(ListOfWork, form=ListOfWorkForm, extra=1)
    listofworkC2Formset = modelformset_factory(ListOfWorkC2, form=ListOfWorkC2Form, extra=1)
    listofworkC3Formset = modelformset_factory(ListOfWorkC3, form=ListOfWorkC3Form, extra=1)
    ListOfMachineryFormset = modelformset_factory(ListOfMachinery, form=ListOfMachineryForm, extra=1)
    ListOfMachineryFormsetd2 = modelformset_factory(ListOfMachineryD2, form=ListOfMachineryFormD2, extra=1)
    ListOfTechnicalFormset = modelformset_factory(ListofTechnical, form=ListOfTechnicalForm, extra=1)

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        tracking_number = request.session.get('tracking_number')
        if tracking_number:
            tracking_no = LayerFormStatus.objects.filter(tracking_id=tracking_number)
            if tracking_no.exists():
                contractor = Contractor.objects.get(layer=tracking_no[0])

                formset = self.detailsRegFormset(prefix='regform',
                                                 queryset=DetailsRegistration.objects.filter(
                                                     contractor=contractor))

                worklist_formset = self.listofworkFormset(prefix='worklist',
                                                          queryset=ListOfWork.objects.filter(
                                                              contractor=contractor))

                worklist_formsetc2 = self.listofworkC2Formset(prefix='worklistc2', queryset=ListOfWorkC2.objects.filter(
                    contractor=contractor))
                worklist_formsetc3 = self.listofworkC3Formset(prefix='worklistc3', queryset=ListOfWorkC3.objects.filter(
                    contractor=contractor))
                ListOfMachineryForm = self.ListOfMachineryFormset(prefix='machinary',
                                                                  queryset=ListOfMachinery.objects.filter(
                                                                      contractor=contractor))
                ListOfMachineryFormd2 = self.ListOfMachineryFormsetd2(prefix='machinaryd2',
                                                                      queryset=ListOfMachineryD2.objects.filter(
                                                                          contractor=contractor))
                ListOfTechnicalForm = self.ListOfTechnicalFormset(prefix='technical',
                                                                  queryset=ListofTechnical.objects.filter(
                                                                      contractor=contractor))

                instant_val = ProfessionalTechnical.objects.filter(contractor=contractor)

                if instant_val.exists():
                    protech_form = ProfessionalTechnicalForm(instance=instant_val[0])
                else:
                    protech_form = ProfessionalTechnicalForm()

                # details_reg = DetailsRegistration.objects.filter(contractor__user=request.user)
                protech = ProfessionalTechnical.objects.filter(contractor=contractor)

                if protech.exists():
                    protech = protech[0]

        else:
            formset = self.detailsRegFormset(prefix='regform',
                                             queryset=DetailsRegistration.objects.filter(contractor__user=request.user,
                                                                                         contractor__layer__is_submitted=False))
            worklist_formset = self.listofworkFormset(prefix='worklist',
                                                      queryset=ListOfWork.objects.filter(contractor__user=request.user,
                                                                                         contractor__layer__is_submitted=False))

            worklist_formsetc2 = self.listofworkC2Formset(prefix='worklistc2', queryset=ListOfWorkC2.objects.filter(
                contractor__user=request.user, contractor__layer__is_submitted=False))
            worklist_formsetc3 = self.listofworkC3Formset(prefix='worklistc3', queryset=ListOfWorkC3.objects.filter(
                contractor__user=request.user, contractor__layer__is_submitted=False))
            ListOfMachineryForm = self.ListOfMachineryFormset(prefix='machinary',
                                                              queryset=ListOfMachinery.objects.filter(
                                                                  contractor__user=request.user,
                                                                  contractor__layer__is_submitted=False))
            ListOfMachineryFormd2 = self.ListOfMachineryFormsetd2(prefix='machinaryd2',
                                                                  queryset=ListOfMachineryD2.objects.filter(
                                                                      contractor__user=request.user,
                                                                      contractor__layer__is_submitted=False))
            ListOfTechnicalForm = self.ListOfTechnicalFormset(prefix='technical',
                                                              queryset=ListofTechnical.objects.filter(
                                                                  contractor__user=request.user,
                                                                  contractor__layer__is_submitted=False))

            instant_val = ProfessionalTechnical.objects.filter(contractor__user=request.user,
                                                               contractor__layer__is_submitted=False)

            if instant_val.exists():
                protech_form = ProfessionalTechnicalForm(instance=instant_val[0])
            else:
                protech_form = ProfessionalTechnicalForm()

            # details_reg = DetailsRegistration.objects.filter(contractor__user=request.user)
            protech = ProfessionalTechnical.objects.filter(contractor__user=request.user,
                                                           contractor__layer__is_submitted=False)

            if protech.exists():
                protech = protech[0]

        context = {
            'formset': formset,
            'protech': protech,
            'protech_form': protech_form,
            'worklist_formset': worklist_formset,
            'worklist_formset_c2': worklist_formsetc2,
            'worklist_formset_c3': worklist_formsetc3,
            'ListOfMachineryForm': ListOfMachineryForm,
            'ListOfMachineryFormd2': ListOfMachineryFormd2,
            'ListOfTechnicalForm': ListOfTechnicalForm,
            'type': 'renewal',

        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        tracking_number = request.session.get('tracking_number')
        if tracking_number:
            tracking_no = LayerFormStatus.objects.filter(tracking_id=tracking_number)
            if tracking_no.exists():
                contractor = Contractor.objects.get(layer=tracking_no[0])
                detail_reg_formset = self.detailsRegFormset(request.POST, request.FILES, prefix='regform')
                worklist_formset = self.listofworkFormset(request.POST, request.FILES, prefix='worklist',
                                                          queryset=ListOfWork.objects.filter(
                                                              contractor=contractor))
                instant_val = ProfessionalTechnical.objects.filter(contractor=contractor)

                if instant_val.exists():
                    protech_form = ProfessionalTechnicalForm(request.POST, request.FILES, instance=instant_val[0])
                else:
                    protech_form = ProfessionalTechnicalForm(request.POST, request.FILES)

                if protech_form.is_valid():
                    protech = protech_form.save(commit=False)
                    protech.contractor = contractor
                    protech.save()

                    if protech.has_registered:
                        if detail_reg_formset.is_valid():
                            instances = detail_reg_formset.save(commit=False)
                            for instance in instances:
                                instance.contractor = contractor
                                instance.save()
                listofworkFormset = modelformset_factory(ListOfWork, form=ListOfWorkForm)
                worklist_formset = listofworkFormset(request.POST, request.FILES, prefix='worklist',
                                                     queryset=ListOfWork.objects.filter(contractor=contractor))

                if worklist_formset.is_valid():
                    worklist = worklist_formset.save(commit=False)
                    for work_list in worklist:
                        work_list.contractor = contractor
                        work_list.save()

                listofworkFormset_2 = modelformset_factory(ListOfWorkC2, form=ListOfWorkC2Form)
                worklist_formset_2 = listofworkFormset_2(request.POST, request.FILES, prefix='worklistc2',
                                                         queryset=ListOfWorkC2.objects.filter(contractor=contractor))
                if worklist_formset_2.is_valid():
                    worklist = worklist_formset_2.save(commit=False)
                    for work_list in worklist:
                        work_list.contractor = contractor
                        work_list.save()

                listofworkFormset_3 = modelformset_factory(ListOfWorkC3, form=ListOfWorkC3Form)
                worklist_formset_3 = listofworkFormset_3(request.POST, request.FILES, prefix='worklistc3',
                                                         queryset=ListOfWorkC3.objects.filter(contractor=contractor))

                if worklist_formset_3.is_valid():
                    worklist = worklist_formset_3.save(commit=False)
                    for work_list in worklist:
                        work_list.contractor = contractor
                        work_list.save()

                ListOfMachineryFormset = modelformset_factory(ListOfMachinery, form=ListOfMachineryForm)

                form_sets_machinery = ListOfMachineryFormset(request.POST, request.FILES, prefix='machinary',
                                                             queryset=ListOfMachinery.objects.filter(
                                                                 contractor=contractor))

                if form_sets_machinery.is_valid():
                    worklist = form_sets_machinery.save(commit=False)
                    for work_list in worklist:
                        work_list.contractor = contractor
                        work_list.save()

                ListOfMachineryFormsetd2 = modelformset_factory(ListOfMachineryD2, form=ListOfMachineryFormD2)

                form_sets_machinery_d2 = ListOfMachineryFormsetd2(request.POST, request.FILES, prefix='machinaryd2',
                                                                  queryset=ListOfMachineryD2.objects.filter(
                                                                      contractor=contractor))

                if form_sets_machinery_d2.is_valid():
                    worklist = form_sets_machinery_d2.save(commit=False)
                    for work_list in worklist:
                        work_list.contractor = contractor
                        work_list.save()

                ListOfTechnicalFormset = modelformset_factory(ListofTechnical, form=ListOfTechnicalForm)

                form_sets_technical = ListOfTechnicalFormset(request.POST, request.FILES, prefix='technical',
                                                             queryset=ListofTechnical.objects.filter(
                                                                 contractor=contractor))

                if form_sets_technical.is_valid():

                    worklist = form_sets_technical.save(commit=False)
                    for work_list in worklist:
                        work_list.contractor = contractor
                        work_list.save()

                print('from session submit')

                return redirect('renewal-step-3')

        else:
            contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=False)
            if not contractor.exists():
                messages.info(request, 'No Contractor Found')
                return redirect('renewal-step-1')
            detail_reg_formset = self.detailsRegFormset(request.POST, request.FILES, prefix='regform')
            worklist_formset = self.listofworkFormset(request.POST, request.FILES, prefix='worklist',
                                                      queryset=ListOfWork.objects.filter(contractor__user=request.user,
                                                                                         contractor__layer__is_submitted=False))
            instant_val = ProfessionalTechnical.objects.filter(contractor__user=request.user,
                                                               contractor__layer__is_submitted=False)

            if instant_val.exists():
                protech_form = ProfessionalTechnicalForm(request.POST, request.FILES, instance=instant_val[0])
            else:
                protech_form = ProfessionalTechnicalForm(request.POST, request.FILES)

            if protech_form.is_valid():
                protech = protech_form.save(commit=False)
                protech.contractor = contractor.last()
                protech.save()

                if protech.has_registered:
                    if detail_reg_formset.is_valid():
                        instances = detail_reg_formset.save(commit=False)
                        for instance in instances:
                            instance.contractor = contractor.last()
                            instance.save()
            listofworkFormset = modelformset_factory(ListOfWork, form=ListOfWorkForm)
            worklist_formset = listofworkFormset(request.POST, request.FILES, prefix='worklist',
                                                 queryset=ListOfWork.objects.all())

            if worklist_formset.is_valid():
                worklist = worklist_formset.save(commit=False)
                for work_list in worklist:
                    work_list.contractor = contractor.last()
                    work_list.save()

            listofworkFormset_2 = modelformset_factory(ListOfWorkC2, form=ListOfWorkC2Form)
            worklist_formset_2 = listofworkFormset_2(request.POST, request.FILES, prefix='worklistc2',
                                                     queryset=ListOfWorkC2.objects.filter(contractor__user=request.user,
                                                                                          contractor__layer__is_submitted=False))
            if worklist_formset_2.is_valid():
                worklist = worklist_formset_2.save(commit=False)
                for work_list in worklist:
                    work_list.contractor = contractor.last()
                    work_list.save()

            listofworkFormset_3 = modelformset_factory(ListOfWorkC3, form=ListOfWorkC3Form)
            worklist_formset_3 = listofworkFormset_3(request.POST, request.FILES, prefix='worklistc3',
                                                     queryset=ListOfWorkC3.objects.filter(contractor__user=request.user,
                                                                                          contractor__layer__is_submitted=False))

            if worklist_formset_3.is_valid():
                worklist = worklist_formset_3.save(commit=False)
                for work_list in worklist:
                    work_list.contractor = contractor.last()
                    work_list.save()

            ListOfMachineryFormset = modelformset_factory(ListOfMachinery, form=ListOfMachineryForm)

            form_sets_machinery = ListOfMachineryFormset(request.POST, request.FILES, prefix='machinary',
                                                         queryset=ListOfMachinery.objects.filter(
                                                             contractor__user=request.user,
                                                             contractor__layer__is_submitted=False))

            if form_sets_machinery.is_valid():
                worklist = form_sets_machinery.save(commit=False)
                for work_list in worklist:
                    work_list.contractor = contractor.last()
                    work_list.save()

            ListOfMachineryFormsetd2 = modelformset_factory(ListOfMachineryD2, form=ListOfMachineryFormD2)

            form_sets_machinery_d2 = ListOfMachineryFormsetd2(request.POST, request.FILES, prefix='machinaryd2',
                                                              queryset=ListOfMachineryD2.objects.filter(
                                                                  contractor__user=request.user,
                                                                  contractor__layer__is_submitted=False))

            if form_sets_machinery_d2.is_valid():
                worklist = form_sets_machinery_d2.save(commit=False)
                for work_list in worklist:
                    work_list.contractor = contractor.first()
                    work_list.save()

            ListOfTechnicalFormset = modelformset_factory(ListofTechnical, form=ListOfTechnicalForm)

            form_sets_technical = ListOfTechnicalFormset(request.POST, request.FILES, prefix='technical',
                                                         queryset=ListofTechnical.objects.filter(
                                                             contractor__user=request.user,
                                                             contractor__layer__is_submitted=False))

            if form_sets_technical.is_valid():
                print('list technical ok')
                worklist = form_sets_technical.save(commit=False)
                for work_list in worklist:
                    work_list.contractor = contractor.last()
                    work_list.save()

            return redirect('license-upgrade-step-3')


class UpgradeViewStep3(TemplateView):
    template_name = 'step-3-upgrade.html'

    def get(self, request, *args, **kwargs):
        tracking_number = request.session.get('tracking_number')
        # With session
        if tracking_number:
            tracking_no = LayerFormStatus.objects.filter(tracking_id=tracking_number)
            if tracking_no.exists():
                contractor = Contractor.objects.get(layer=tracking_no[0])

                financial_capacity = FinancialCapacity.objects.filter(contractor=contractor)
                if financial_capacity.exists():
                    form = FinancialCapacityForm(instance=financial_capacity[0])
                else:
                    form = FinancialCapacityForm()

                balance_sheet = BalanceSheet.objects.filter(contractor=contractor)
                if balance_sheet.exists():
                    balance_form = BalanceSheetForm(instance=balance_sheet[0])
                else:
                    balance_form = BalanceSheetForm()

        # Without session

        else:
            financial_capacity = FinancialCapacity.objects.filter(contractor__user=request.user,
                                                                  contractor__layer__is_submitted=False)
            if financial_capacity.exists():
                form = FinancialCapacityForm(instance=financial_capacity[0])
            else:
                form = FinancialCapacityForm()

            balance_sheet = BalanceSheet.objects.filter(contractor__user=request.user,
                                                        contractor__layer__is_submitted=False)
            if balance_sheet.exists():
                balance_form = BalanceSheetForm(instance=balance_sheet[0])
            else:
                balance_form = BalanceSheetForm()

        context = {
            'form': form,
            'balance_form': balance_form,
            'type': 'renewal',
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        tracking_number = request.session.get('tracking_number')
        # With session
        if tracking_number:
            tracking_no = LayerFormStatus.objects.filter(tracking_id=tracking_number)
            if tracking_no.exists():
                contractor = Contractor.objects.get(layer=tracking_no[0])

                financial_capacity = FinancialCapacity.objects.filter(contractor=contractor)

                if financial_capacity.exists():

                    form = FinancialCapacityForm(request.POST, request.FILES, instance=financial_capacity[0])
                else:
                    form = FinancialCapacityForm(request.POST, request.FILES)

                if form.is_valid():
                    financial_capacity_obj = form.save(commit=False)
                    financial_capacity_obj.contractor = contractor
                    financial_capacity_obj.save()

                balance_sheet = BalanceSheet.objects.filter(contractor=contractor)

                if balance_sheet.exists():
                    balance_form = BalanceSheetForm(request.POST, request.FILES, instance=balance_sheet[0])
                else:
                    balance_form = BalanceSheetForm(request.POST, request.FILES)

                if balance_form.is_valid():
                    balance_form = balance_form.save(commit=False)
                    balance_form.contractor = contractor
                    balance_form.save()
                else:
                    print('*******', balance_form.errors)
                print('session submit')
                return redirect('license-upgrade-step-4')
        # Without session
        else:
            contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=False)

            financial_capacity = FinancialCapacity.objects.filter(contractor__user=request.user,
                                                                  contractor__layer__is_submitted=False)

            if financial_capacity.exists():

                form = FinancialCapacityForm(request.POST, request.FILES, instance=financial_capacity[0])
            else:
                form = FinancialCapacityForm(request.POST, request.FILES)

            if form.is_valid():
                financial_capacity_obj = form.save(commit=False)
                financial_capacity_obj.contractor = contractor.last()
                financial_capacity_obj.save()

            balance_sheet = BalanceSheet.objects.filter(contractor__user=request.user,
                                                        contractor__layer__is_submitted=False)

            if balance_sheet.exists():
                balance_form = BalanceSheetForm(request.POST, request.FILES, instance=balance_sheet[0])
            else:
                balance_form = BalanceSheetForm(request.POST, request.FILES)

            if balance_form.is_valid():
                balance_form = balance_form.save(commit=False)
                balance_form.contractor = contractor.last()
                balance_form.save()
            else:
                print('*******', balance_form.errors)

            return redirect('license-upgrade-step-4')


class UpgradeViewStep4(TemplateView):
    template_name = 'step-4-upgrade.html'

    def get(self, request, *args, **kwargs):
        tracking_number = request.session.get('tracking_number')
        # With session
        if tracking_number:
            tracking_no = LayerFormStatus.objects.filter(tracking_id=tracking_number)
            if tracking_no.exists():
                contractor = Contractor.objects.get(layer=tracking_no[0])
                annexf_formset = modelformset_factory(AnnexF, form=AnnexFForm, extra=1)
                annexf_form = annexf_formset(prefix='annexf',
                                             queryset=AnnexF.objects.filter(contractor=contractor))

                ListOfMachineryFormset = modelformset_factory(ListOfMachinery, form=ListOfMachineryForm)

                form_sets = ListOfMachineryFormset(prefix='machinary',
                                                   queryset=ListOfMachinery.objects.filter(contractor=contractor))

                legal_capacity = LegalCapacity.objects.filter(contractor=contractor)
                legal_capacity_obj = ''
                if legal_capacity.exists():
                    legal_capacity_obj = legal_capacity[0]
                    legal_form = LegalCapacityForm(instance=legal_capacity[0])
                else:
                    legal_form = LegalCapacityForm()

        else:
            annexf_formset = modelformset_factory(AnnexF, form=AnnexFForm, extra=1)
            annexf_form = annexf_formset(prefix='annexf',
                                         queryset=AnnexF.objects.filter(contractor__user=request.user,
                                                                        contractor__layer__is_submitted=False))

            ListOfMachineryFormset = modelformset_factory(ListOfMachinery, form=ListOfMachineryForm)

            form_sets = ListOfMachineryFormset(prefix='machinary', queryset=ListOfMachinery.objects.all())

            legal_capacity = LegalCapacity.objects.filter(contractor__user=request.user,
                                                          contractor__layer__is_submitted=False)
            legal_capacity_obj = ''
            if legal_capacity.exists():
                legal_capacity_obj = legal_capacity[0]
                legal_form = LegalCapacityForm(instance=legal_capacity[0])
            else:
                legal_form = LegalCapacityForm()

        context = {
            'legal_form': legal_form,
            'legal_capacity_obj': legal_capacity_obj,
            'annexf_form': annexf_form,
            'type': 'renewal',
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):

        tracking_number = request.session.get('tracking_number')
        # With session
        if tracking_number:
            tracking_no = LayerFormStatus.objects.filter(tracking_id=tracking_number)
            if tracking_no.exists():
                contractor = Contractor.objects.get(layer=tracking_no[0])
                enclosed_Annex_F = request.POST.get('annex_f')
                legal_capacity = LegalCapacity.objects.filter(contractor=contractor)
                if legal_capacity.exists():
                    legal_form = LegalCapacityForm(request.POST, request.FILES, instance=legal_capacity[0])
                else:
                    legal_form = LegalCapacityForm(request.POST, request.FILES)

                if legal_form.is_valid():
                    legalcapacity = legal_form.save(commit=False)
                    legalcapacity.contractor = contractor
                    if enclosed_Annex_F == 'on':
                        legalcapacity.enclosed_Annex_F = True
                    else:
                        legalcapacity.enclosed_Annex_F = False

                    legalcapacity.save()

                annexf_formset = modelformset_factory(AnnexF, form=AnnexFForm)
                annexf_form = annexf_formset(request.POST, request.FILES, prefix='annexf',
                                             queryset=AnnexF.objects.filter(contractor=contractor))

                if annexf_form.is_valid():
                    annexf = annexf_form.save(commit=False)
                    for anxf in annexf:
                        anxf.contractor = contractor
                        anxf.save()

                return redirect('license-upgrade-step-5')


        # Without session
        else:
            contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=False)
            enclosed_Annex_F = request.POST.get('annex_f')
            legal_capacity = LegalCapacity.objects.filter(contractor__user=request.user,
                                                          contractor__layer__is_submitted=False)
            if legal_capacity.exists():
                legal_form = LegalCapacityForm(request.POST, request.FILES, instance=legal_capacity[0])
            else:
                legal_form = LegalCapacityForm(request.POST, request.FILES)

            if legal_form.is_valid():
                legalcapacity = legal_form.save(commit=False)
                legalcapacity.contractor = contractor.last()
                if enclosed_Annex_F == 'on':
                    legalcapacity.enclosed_Annex_F = True
                else:
                    legalcapacity.enclosed_Annex_F = False

                legalcapacity.save()

            else:
                print(legal_form.errors)
                return HttpResponse('error')

            annexf_formset = modelformset_factory(AnnexF, form=AnnexFForm)
            annexf_form = annexf_formset(request.POST, request.FILES, prefix='annexf',
                                         queryset=AnnexF.objects.filter(contractor=contractor.last()))

            if annexf_form.is_valid():
                annexf = annexf_form.save(commit=False)
                for anxf in annexf:
                    anxf.contractor = contractor.last()
                    anxf.save()
            else:
                print(annexf_form.errors)
                return HttpResponse('errror.')

            return redirect('license-upgrade-step-5')


class UpgradeViewStep5(TemplateView):
    template_name = 'step-5-upgrade.html'

    def get(self, request, *args, **kwargs):

        tracking_number = request.session.get('tracking_number')
        # With session
        if tracking_number:
            tracking_no = LayerFormStatus.objects.filter(tracking_id=tracking_number)
            if tracking_no.exists():
                contractor = Contractor.objects.get(layer=tracking_no[0])
                texatation = Texation.objects.filter(contractor=contractor)
                vats = Vat.objects.filter(contractor=contractor)
                tins = Tin.objects.filter(contractor=contractor)
                texation_obj = ''
                if texatation.exists():
                    texation_form = TexationForm(instance=texatation[0])
                    texation_obj = texatation[0]
                else:
                    texation_form = TexationForm()

                if vats.exists():
                    vat_formset = modelformset_factory(Vat, form=VatForm, extra=0)
                else:
                    vat_formset = modelformset_factory(Vat, form=VatForm, extra=1)

                vat_form = vat_formset(prefix='vat', queryset=Vat.objects.filter(contractor=contractor))

                if tins.exists():
                    tin_formset = modelformset_factory(Tin, form=TinForm, extra=0)
                else:
                    tin_formset = modelformset_factory(Tin, form=TinForm, extra=1)
                tin_form = tin_formset(prefix='tin', queryset=Tin.objects.filter(contractor=contractor))

                context = {
                    'vat_form': vat_form,
                    'tin_form': tin_form,
                    'texation_form': texation_form,
                    'texation_obj': texation_obj,
                    'contractor': contractor,
                    'type': 'renewal',
                }
                return render(request, self.template_name, context)


        else:
            contractor = Contractor.objects.filter(user=request.user)
            contractor_id = contractor[0].id

            texatation = Texation.objects.filter(contractor__user=request.user, contractor__layer__is_submitted=False)
            vats = Vat.objects.filter(contractor__user=request.user, contractor__layer__is_submitted=False)
            tins = Tin.objects.filter(contractor__user=request.user, contractor__layer__is_submitted=False)
            texation_obj = ''
            if texatation.exists():
                texation_form = TexationForm(instance=texatation[0])
                texation_obj = texatation[0]
            else:
                texation_form = TexationForm()

            if vats.exists():
                vat_formset = modelformset_factory(Vat, form=VatForm, extra=0)
            else:
                vat_formset = modelformset_factory(Vat, form=VatForm, extra=1)

            vat_form = vat_formset(prefix='vat', queryset=Vat.objects.filter(contractor__user=request.user,
                                                                             contractor__layer__is_submitted=False))

            print(Vat.objects.filter(contractor__user=request.user, contractor__layer__is_submitted=False))

            if tins.exists():
                tin_formset = modelformset_factory(Tin, form=TinForm, extra=0)
            else:
                tin_formset = modelformset_factory(Tin, form=TinForm, extra=1)
            tin_form = tin_formset(prefix='tin', queryset=Tin.objects.filter(contractor__user=request.user,
                                                                             contractor__layer__is_submitted=False))

            context = {
                'vat_form': vat_form,
                'tin_form': tin_form,
                'texation_form': texation_form,
                'texation_obj': texation_obj,
                'contractor': contractor[0],
                'type': 'renewal',
            }
            return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        tracking_number = request.session.get('tracking_number')
        # With session
        if tracking_number:
            tracking_no = LayerFormStatus.objects.filter(tracking_id=tracking_number)
            if tracking_no.exists():
                contractor = Contractor.objects.get(layer=tracking_no[0])

                vat_formset = modelformset_factory(Vat, form=VatForm, extra=0)
                vat_form = vat_formset(request.POST, request.FILES, prefix='vat',
                                       queryset=Vat.objects.filter(contractor=contractor))

                if vat_form.is_valid():
                    vat_form_set = vat_form.save(commit=False)
                    for vat_form in vat_form_set:
                        vat_form.contractor = contractor
                        vat_form.save()

                tin_formset = modelformset_factory(Tin, form=TinForm, extra=0)
                tin_form = tin_formset(request.POST, request.FILES, prefix='tin',
                                       queryset=Tin.objects.filter(contractor=contractor))

                if tin_form.is_valid():
                    tin_form_set = tin_form.save(commit=False)
                    for tin_form in tin_form_set:
                        tin_form.contractor = contractor
                        tin_form.save()

                declaration_checkbox = request.POST.get('declaration_checkbox')
                texatation = Texation.objects.filter(contractor=contractor)
                if texatation.exists():
                    texation_form = TexationForm(request.POST, request.FILES, instance=texatation[0])
                else:
                    texation_form = TexationForm(request.POST, request.FILES)

                if texation_form.is_valid():
                    texation_form_obj = texation_form.save(commit=False)
                    texation_form_obj.contractor = contractor
                    if declaration_checkbox == 'on':
                        texation_form_obj.declaration = True
                    else:
                        texation_form_obj.declaration = False

                    texation_form_obj.save()

                print('session submit')

                return redirect('contractor_details_view', contractor.id)

        else:
            contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=False)

            contractor_id = contractor.first().id

            vat_formset = modelformset_factory(Vat, form=VatForm, extra=0)
            vat_form = vat_formset(request.POST, request.FILES, prefix='vat', queryset=Vat.objects.all())

            if vat_form.is_valid():
                vat_form_set = vat_form.save(commit=False)
                for vat_form in vat_form_set:
                    vat_form.contractor = contractor.first()
                    vat_form.save()

            tin_formset = modelformset_factory(Tin, form=TinForm, extra=0)
            tin_form = tin_formset(request.POST, request.FILES, prefix='tin', queryset=Tin.objects.all())

            if tin_form.is_valid():
                tin_form_set = tin_form.save(commit=False)
                for tin_form in tin_form_set:
                    tin_form.contractor = contractor.first()
                    tin_form.save()

            declaration_checkbox = request.POST.get('declaration_checkbox')
            texatation = Texation.objects.filter(contractor=contractor.first())
            if texatation.exists():
                texation_form = TexationForm(request.POST, request.FILES, instance=texatation[0])
            else:
                texation_form = TexationForm(request.POST, request.FILES)

            if texation_form.is_valid():
                texation_form_obj = texation_form.save(commit=False)
                texation_form_obj.contractor = contractor.first()
                if declaration_checkbox == 'on':
                    texation_form_obj.declaration = True
                else:
                    texation_form_obj.declaration = False

                texation_form_obj.save()

            return redirect('contractor_details_view', contractor_id)

from django.urls import path
from .views import *
from .renewal_views import *
from .upgrade_views import *


urlpatterns = [
    # frontend urls
    path('', HomeView.as_view(), name='home'),

    # enrollment urls
    path('enrollment/step-1/', EnrollmentStep1View.as_view(), name='enrollment-step-1'),
    path('enrollment/step-2/', EnrollmentStep2View.as_view(), name='enrollment-step-2'),
    path('enrollment/step-3/', EnrollmentStep3View.as_view(), name='enrollment-step-3'),
    path('enrollment/step-4/', EnrollmentStep4View.as_view(), name='enrollment-step-4'),
    path('enrollment/step-5/', EnrollmentStep5View.as_view(), name='enrollment-step-5'),

    # renewal urls

    path('renewal/step-1/', renewalView.as_view(), name='renewal-step-1'),
    path('renewal/step-2/', RenewalStep2View.as_view(), name='renewal-step-2'),
    path('renewal/step-3/', RenewalStep3View.as_view(), name='renewal-step-3'),
    path('renewal/step-4/', RenewalStep4View.as_view(), name='renewal-step-4'),
    path('renewal/step-5/', RenewalStep5View.as_view(), name='renewal-step-5'),

    # license upgrade

    path('license-upgrade/step-1/', UpgradeView.as_view(), name='license-upgrade-step-1'),
    path('license-upgrade/step-2/', UpgradeViewStep2.as_view(), name='license-upgrade-step-2'),
    path('license-upgrade/step-3/', UpgradeViewStep3.as_view(), name='license-upgrade-step-3'),
    path('license-upgrade/step-4/', UpgradeViewStep4.as_view(), name='license-upgrade-step-4'),
    path('license-upgrade/step-5/', UpgradeViewStep5.as_view(), name='license-upgrade-step-5'),

    path('profile-page/', ProfileView, name='profile-page'),
    path('contractor-preview-view/<int:pk>/', ContractorPreviewView, name='contractor_preview_view'),

    path('partners/delete/<int:id>/', deletePartnerView, name='partner-delete'),
    path('reg/delete/<int:id>/', regdeleteView, name='reg-delete'),
    path('worklist/delete/<int:id>/', worklistdeleteView, name='worklist-delete'),
    path('worklistc2/delete/<int:id>/', worklistc2deleteView, name='worklistc2-delete'),
    path('worklistc3/delete/<int:id>/', worklistc3deleteView, name='worklistc3-delete'),
    path('machinery/delete/<int:id>/', machinerydeleteView, name='machinery-delete'),
    path('technical/delete/<int:id>/', technicaldeleteView, name='technical-delete'),

    path('machineryd2/delete/<int:id>/', machineryd2deleteView, name='machineryd2-delete'),
    path('annexf/delete/<int:id>/', annexFDelete, name='annexf-delete'),
    path('vat/delete/<int:id>/', vatDelete, name='vat-delete'),
    path('tin/delete/<int:id>/', tinDelete, name='tin-delete'),

    # path('worklistForm/', worklistForm, name='worklistForm'),
    # path('worklistFormc2/', worklistFormc2, name='worklistFormc2'),
    # path('worklistFormc3/', worklistFormc3, name='worklistFormc3'),
    # path('listofeqipment/', listofeqipment, name='listofeqipment'),
    path('listofeqipmentd2/', listofeqipmentd2, name='listofeqipmentd2'),
    path('listtechnical/', listtechnical, name='listtechnical'),

    path('contact/', ContactView.as_view(), name='contact'),
    path('guidelines/', GuideLineView.as_view(), name='guideline'),

    path('approved-contractor-list-view/', approved_contractor_list_view, name='approved_contractor_list_view'),
    path('contractor-list-view/<str:role>/<str:division>/', contractor_list_view, name='contractor_list_view'),
    path('metting/', mettingCall, name='call-for-metting'),
    path('meetting-list', meettingList, name='meetting_list'),
    path('metting/<int:id>/', mettingEditCall, name='edit-call-for-metting'),

    path('contractor-details-view/<int:pk>/', ContractorDetailsView, name='contractor_details_view'),
    path('contractor-details-view-backend/<int:pk>/', ContractorDetailsViewBackend,
         name='contractor_details_view_backend'),

    path('assesment-form/<int:id>/', assesmentView, name='assesment_form'),
    path('pdf-contractor-list/', GeneratePdf.as_view(), name='contractor_pdf_view'),
    path('success-url/', successView, name='success-url'),
    path('failure-url/', failureView, name='failure-url'),

    path('contractor_form_submit/', contractor_form_submit, name='contractor_form_submit'),
    path('renewal_form_submit/', renewal_form_submit, name='renewal_form_submit'),
    path('upgrade_form_submit/', upgrade_form_submit, name='upgrade_form_submit'),


    path('ssl-payment/<int:tran_type>/', integrate_ssl_commrz, name='ssl-payment'),
    path('ssl-payment-renewal/<int:tran_type>/', integrate_ssl_commrz_renewal, name='ssl-payment-renewal'),
    path('ssl-payment-upgrade/<int:tran_type>/', integrate_ssl_commrz_upgrade, name='ssl-payment-upgrade'),
    path('chalan-ssl-payment/', integrate_chalan_ssl_commrz, name='chalan-ssl-payment'),

    # Chalan start
    # path('add_chalan/', ChalanView.as_view(), name='add_chalan'),
    path('add_chalan/', ChalanView, name='add_chalan'),
    path('chalan-list/', ChalanListBack.as_view(), name="chalan-list"),
    path('getContractorInfo/<int:pk>/', getContractor, name="getContractor"),
    path('addLicense/', addLicenseView, name='add-license'),
    path('print_preview/<int:pk>', PrintPreview, name='print_preview'),
    path('print_license/<int:pk>', print_license_preview, name='print_license'),
    path('contractor_license/<int:pk>', contractorLicensePreview, name="contractor_license"),

    # Contractor Chalan

    path('download/', ChalanList.as_view(), name="download"),
    path('license_list/', LicenseList.as_view(), name="license_list"),
    path('frontend_contractor_license/<int:pk>/', contractorLicensePreview, name="frontend_contractor_license"),
    path('frontend_print_license/<int:pk>/', contractor_print_license_preview, name='frontend_print_license'),
    path('chalan-payment/', chalanPayment, name='chalan-payment')

]

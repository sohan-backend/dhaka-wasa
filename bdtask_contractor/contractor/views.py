import json

from django.shortcuts import render, get_object_or_404

from .models import *

# Create your views here.
from django.views.generic.base import View
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.shortcuts import HttpResponse
from django.contrib.auth.decorators import login_required
from .models import Contractor
from .forms import *
from django.shortcuts import redirect, get_object_or_404
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.forms import formset_factory
from django.forms import modelformset_factory
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.db.models import Q
import requests
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import ListView
from accounts.models import AuthorizedPerson, SSLSettings
from django.views.generic.detail import DetailView
from django.http import HttpResponseRedirect

from django.http import JsonResponse
import uuid


class GeneratePdf(View):
    def get(self, request, *args, **kwargs):
        confirmed_list = LayerFormStatus.objects.filter(layer=request.user.roles, form_status='CONFIRMED')
        assessment_eval_sign = ''
        user_lists = User.objects.filter(roles='CONTRACTOR')

        print(confirmed_list, request.user.roles)
        messages_to = request.user.roles + '_comments'

        if request.user.roles == 'COMMITTEE_SECRETARY':
            confirmed_list = LayerFormStatus.objects.filter(layer=request.user.roles, form_status='CONFIRMED')
            user_lists = User.objects.filter(Q(roles='COMMITTEE_SECRETARY'))
            assessment_eval_sign = ContractorListEvaluation.objects.filter(is_approved='CONFIRMED',
                                                                           user__roles='COMMITTEE_SECRETARY')

        elif request.user.roles == 'SECRETARY':
            confirmed_list = LayerFormStatus.objects.filter(
                Q(layer=request.user.roles, form_status='PENDING') | Q(layer=request.user.roles,
                                                                       form_status='CONFIRMED'))
            user_lists = User.objects.filter(Q(roles='COMMITTEE_SECRETARY') | Q(roles='SECRETARY'))
            assessment_eval_sign = AssessmentEvaluation.objects.filter(
                Q(is_approved='CONFIRMED', user__roles='SECRETARY') | Q(is_approved='CONFIRMED',
                                                                        user__roles='COMMITTEE_SECRETARY'))

        elif request.user.roles == 'DMD':
            confirmed_list = LayerFormStatus.objects.filter(
                Q(layer=request.user.roles, form_status='PENDING') | Q(layer=request.user.roles,
                                                                       form_status='CONFIRMED'))

            user_lists = User.objects.filter(Q(roles='COMMITTEE_SECRETARY') | Q(roles='SECRETARY') | Q(roles='DMD'))

            assessment_eval_sign = AssessmentEvaluation.objects.filter(
                Q(is_approved='CONFIRMED', user__roles='SECRETARY') | Q(is_approved='CONFIRMED',
                                                                        user__roles='COMMITTEE_SECRETARY') | Q(
                    is_approved='CONFIRMED',
                    user__roles='DMD'))

        elif request.user.roles == 'MD':
            confirmed_list = LayerFormStatus.objects.filter(
                Q(layer=request.user.roles, form_status='PENDING'))

            user_lists = User.objects.filter(
                Q(roles='COMMITTEE_SECRETARY') | Q(roles='SECRETARY') | Q(roles='DMD') | Q(roles='MD'))
            assessment_eval_sign = AssessmentEvaluation.objects.filter(
                Q(is_approved='CONFIRMED', user__roles='SECRETARY') | Q(is_approved='CONFIRMED',
                                                                        user__roles='COMMITTEE_SECRETARY') | Q(
                    is_approved='CONFIRMED',
                    user__roles='DMD'), Q(is_approved='CONFIRMED', user__roles='MD'))

        print('***********', assessment_eval_sign)
        context = {
            'confirmed_list': confirmed_list,
            'assessment_eval_sign': assessment_eval_sign,
            'messages_to': messages_to,
            'user_lists': user_lists,
        }
        return render(request, 'contractor/pdf_view_list.html', context)

    def post(self, request, *args, **kwargs):
        comments = request.POST.get('comments')
        if 'forward_to_secretary' in request.POST:
            layer_from = request.user.roles
            layer_to = 'MD'
            comments_ = layer_from + ' > ' + layer_to + '  Comments: { ' + comments + ' } '
            confirmed_list = LayerFormStatus.objects.filter(layer=request.user.roles, form_status='CONFIRMED')

            for list in confirmed_list:
                list.layer = 'SECRETARY'
                list.form_status = 'PENDING'
                list.dict = comments_
                if request.user.roles == 'COMMITTEE_SECRETARY':
                    list.COMMITTEE_SECRETARY_comments = comments

                elif request.user.roles == 'SECRETARY':
                    print('secretary layer ....')
                    list.SECRETARY_comments = comments

                elif request.user.roles == 'DMD':
                    list.DMD_comments = comments

                elif request.user.roles == 'MD':
                    list.MD_comments = comments

                list.save()
            messages.success(request, 'List is forwarded successfully to Secretary Layer')
            return redirect('contractor_pdf_view')

        if 'forward_to_dmd' in request.POST:
            layer_from = request.user.roles
            layer_to = 'DMD'
            comments_ = layer_from + ' > ' + layer_to + '  Comments: { ' + comments + ' } '

            confirmed_list = LayerFormStatus.objects.filter(layer=request.user.roles, form_status='PENDING')
            for list in confirmed_list:
                list.layer = 'DMD'
                list.form_status = 'PENDING'
                list.dict = comments_
                if request.user.roles == 'COMMITTEE_SECRETARY':
                    list.COMMITTEE_SECRETARY_comments = comments

                elif request.user.roles == 'SECRETARY':
                    print('secretary layer ....')
                    list.SECRETARY_comments = comments

                elif request.user.roles == 'DMD':
                    list.DMD_comments = comments

                elif request.user.roles == 'MD':
                    list.MD_comments = comments

                list.save()
            messages.success(request, 'List is forwarded successfully to DMD Layer')
            return redirect('contractor_pdf_view')

        if 'forward_to_md' in request.POST:
            layer_from = request.user.roles
            layer_to = 'MD'
            comments_ = layer_from + ' > ' + layer_to + '  Comments: { ' + comments + ' } '

            confirmed_list = LayerFormStatus.objects.filter(layer=request.user.roles, form_status='PENDING')
            for list in confirmed_list:
                list.layer = 'MD'
                list.form_status = 'PENDING'
                list.dict = comments_
                if request.user.roles == 'COMMITTEE_SECRETARY':
                    list.COMMITTEE_SECRETARY_comments = comments

                elif request.user.roles == 'SECRETARY':
                    print('secretary layer ....')
                    list.SECRETARY_comments = comments

                elif request.user.roles == 'DMD':
                    list.DMD_comments = comments

                elif request.user.roles == 'MD':
                    list.MD_comments = comments

                list.save()
            messages.success(request, 'List is forwarded successfully to MD Layer')
            return redirect('contractor_pdf_view')

        if 'confirm_md' in request.POST:
            comments = request.POST.get('comments')
            layer_from = request.user.roles
            layer_to = 'MD'
            comments_ = layer_from + ' > ' + layer_to + '  Comments: { ' + comments + ' } '
            confirmed_list = LayerFormStatus.objects.filter(layer=request.user.roles, form_status='PENDING')
            for list in confirmed_list:
                list.layer = 'MD'
                list.form_status = 'CONFIRMED'
                list.dict = comments_
                if request.user.roles == 'COMMITTEE_SECRETARY':
                    list.COMMITTEE_SECRETARY_comments = comments

                elif request.user.roles == 'SECRETARY':
                    print('secretary layer ....')
                    list.SECRETARY_comments = comments

                elif request.user.roles == 'DMD':
                    list.DMD_comments = comments

                elif request.user.roles == 'MD':
                    list.MD_comments = comments
                list.save()
            messages.success(request, 'List is forwarded successfully to MD Layer')
            return redirect('contractor_pdf_view')

        if 'layer_forward' in request.POST:
            layer = request.POST.get('layer')
            if not layer:
                messages.error(request, 'Please select layer to send')
                return redirect('contractor_pdf_view')
            comments = request.POST.get('comments')
            layer_from = request.user.roles
            layer_to = layer
            comments_ = layer_from + ' > ' + layer_to + '  Comments: { ' + comments + ' } '
            confirmed_list = LayerFormStatus.objects.filter(layer=request.user.roles, form_status='PENDING')
            for list in confirmed_list:
                list.layer = layer
                list.form_status = 'PENDING'
                list.dict = comments_
                list.save()
            messages.success(request, 'List is forwarded successfully to MD Layer')
            return redirect('contractor_pdf_view')

        # return response


class HomeView(TemplateView):
    # template_name = 'frontend/home.html'
    template_name = 'contractor/home/home.html'

    # @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        if str(request.user) != 'AnonymousUser':
            if request.user.is_admin:
                return redirect('dashboard')
        return render(request, self.template_name)


class EnrollmentStep1View(View):
    template_name = 'step-1.html'
    queryset = ContractorPartners.objects.all()
    PartnerFormSet = modelformset_factory(ContractorPartners, form=ContractorPartnerForm, extra=1)

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=False)

        if contractor.exists():

            contractor_obj = contractor.first()

            form = ContractorFormStep1(instance=contractor_obj)
            formset = self.PartnerFormSet(queryset=ContractorPartners.objects.filter(contractor=contractor_obj,
                                                                                     contractor__layer__is_submitted=False))

        else:
            form = ContractorFormStep1()
            formset = self.PartnerFormSet(queryset=ContractorPartners.objects.none())

        context = {
            'form': form,
            'formset': formset,
            'contractor': contractor
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):

        formset = self.PartnerFormSet(request.POST, request.FILES)
        radios = request.POST.get('radios2')
        application_division = request.POST.get('application_division')
        trade_license_area = request.POST.get('trade_license_area')

        print('*********', radios, type(radios))

        contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=False)

        if contractor.exists():
            contractor_obj = contractor.last()
            form = ContractorFormStep1(request.POST, request.FILES, instance=contractor_obj)

        else:
            form = ContractorFormStep1(request.POST, request.FILES)
            nid = request.POST.get('NID_NUMBER')
            contractor_qs = Contractor.objects.filter(user=request.user,
                                                      application_division=application_division)

            if contractor_qs.exists():
                messages.error(request, 'You have already applied this category')
                return redirect('enrollment-step-1')

        if form.is_valid() and formset.is_valid():
            contractor = form.save(commit=False)
            contractor.applied_for = 'enrollment'
            contractor.user = request.user
            contractor.application_division = application_division
            contractor.trade_license_area = trade_license_area
            if radios == 'on':
                contractor.has_partners = True
            else:
                contractor.has_partners = False

            contractor.save()

            if contractor.has_partners:
                instances = formset.save(commit=False)
                for instance in instances:
                    instance.contractor = contractor
                    instance.save()

            return redirect('enrollment-step-2')

        context = {
            'form': form,
            'formset': formset
        }
        return render(request, self.template_name, context)


class EnrollmentStep2View(View):
    template_name = 'step-2.html'
    details_qr = DetailsRegistration.objects.filter(contractor__layer__is_submitted=False)
    print('here...', details_qr)
    if details_qr.exists():
        detailsRegFormset = modelformset_factory(DetailsRegistration, form=DetailsRegistrationForm, extra=0)
    else:
        detailsRegFormset = modelformset_factory(DetailsRegistration, form=DetailsRegistrationForm, extra=1)

    list_of_work_qr = ListOfWork.objects.filter(contractor__layer__is_submitted=False)

    if list_of_work_qr.exists():
        listofworkFormset = modelformset_factory(ListOfWork, form=ListOfWorkForm, extra=0)

    else:
        listofworkFormset = modelformset_factory(ListOfWork, form=ListOfWorkForm, extra=1)

    list_work_c2_qr = ListOfWorkC2.objects.filter(contractor__layer__is_submitted=False)

    if list_work_c2_qr.exists():
        listofworkC2Formset = modelformset_factory(ListOfWorkC2, form=ListOfWorkC2Form, extra=0)
    else:
        listofworkC2Formset = modelformset_factory(ListOfWorkC2, form=ListOfWorkC2Form, extra=1)

    list_work_c3_qr = ListOfWorkC3.objects.filter(contractor__layer__is_submitted=False)

    if list_work_c3_qr.exists():
        listofworkC3Formset = modelformset_factory(ListOfWorkC3, form=ListOfWorkC3Form, extra=0)
    else:
        listofworkC3Formset = modelformset_factory(ListOfWorkC3, form=ListOfWorkC3Form, extra=1)

    list_machinery_qr = ListOfMachinery.objects.filter(contractor__layer__is_submitted=False)

    if list_machinery_qr.exists():
        ListOfMachineryFormset = modelformset_factory(ListOfMachinery, form=ListOfMachineryForm, extra=0)
    else:
        ListOfMachineryFormset = modelformset_factory(ListOfMachinery, form=ListOfMachineryForm, extra=1)

    list_machinery_d2_qr = ListOfMachineryD2.objects.filter(contractor__layer__is_submitted=False)

    if list_machinery_d2_qr.exists():
        ListOfMachineryFormsetd2 = modelformset_factory(ListOfMachineryD2, form=ListOfMachineryFormD2, extra=0)
    else:
        ListOfMachineryFormsetd2 = modelformset_factory(ListOfMachineryD2, form=ListOfMachineryFormD2, extra=1)

    list_tech_qr = ListofTechnical.objects.filter(contractor__layer__is_submitted=False)

    if list_tech_qr.exists():
        ListOfTechnicalFormset = modelformset_factory(ListofTechnical, form=ListOfTechnicalForm, extra=0)
    else:
        ListOfTechnicalFormset = modelformset_factory(ListofTechnical, form=ListOfTechnicalForm, extra=1)

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        formset = self.detailsRegFormset(prefix='regform',
                                         queryset=DetailsRegistration.objects.filter(contractor__user=request.user,
                                                                                     contractor__layer__is_submitted=False))
        worklist_formset = self.listofworkFormset(prefix='worklist',
                                                  queryset=ListOfWork.objects.filter(contractor__user=request.user,
                                                                                     contractor__layer__is_submitted=False))

        worklist_formsetc2 = self.listofworkC2Formset(prefix='worklistc2', queryset=ListOfWorkC2.objects.filter(
            contractor__user=request.user, contractor__layer__is_submitted=False))
        worklist_formsetc3 = self.listofworkC3Formset(prefix='worklistc3', queryset=ListOfWorkC3.objects.filter(
            contractor__user=request.user, contractor__layer__is_submitted=False))
        ListOfMachineryForm = self.ListOfMachineryFormset(prefix='machinary', queryset=ListOfMachinery.objects.filter(
            contractor__user=request.user, contractor__layer__is_submitted=False))
        ListOfMachineryFormd2 = self.ListOfMachineryFormsetd2(prefix='machinaryd2',
                                                              queryset=ListOfMachineryD2.objects.filter(
                                                                  contractor__user=request.user,
                                                                  contractor__layer__is_submitted=False))
        ListOfTechnicalForm = self.ListOfTechnicalFormset(prefix='technical', queryset=ListofTechnical.objects.filter(
            contractor__user=request.user, contractor__layer__is_submitted=False))

        instant_val = ProfessionalTechnical.objects.filter(contractor__user=request.user,
                                                           contractor__layer__is_submitted=False)

        # print(instant_val)
        # return HttpResponse('ok')

        if instant_val.exists():
            protech_form = ProfessionalTechnicalForm(instance=instant_val[0])
        else:
            protech_form = ProfessionalTechnicalForm()

        # details_reg = DetailsRegistration.objects.filter(contractor__user=request.user)
        protech = ProfessionalTechnical.objects.filter(contractor__user=request.user,
                                                       contractor__layer__is_submitted=False)

        contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=False).last()

        if protech.exists():
            protech = protech[0]

        context = {
            'formset': formset,
            'protech': protech,
            'protech_form': protech_form,
            'worklist_formset': worklist_formset,
            'worklist_formset_c2': worklist_formsetc2,
            'worklist_formset_c3': worklist_formsetc3,
            'ListOfMachineryForm': ListOfMachineryForm,
            'ListOfMachineryFormd2': ListOfMachineryFormd2,
            'ListOfTechnicalForm': ListOfTechnicalForm,
            'type': 'enrollment',
            'contractor': contractor,

        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=False)

        if not contractor.exists():
            messages.info(request, 'No Contractor Found')
            return redirect('enrollment-step-1')
        detail_reg_formset = self.detailsRegFormset(request.POST, request.FILES, prefix='regform')
        worklist_formset = self.listofworkFormset(request.POST, request.FILES, prefix='worklist',
                                                  queryset=ListOfWork.objects.all())
        instant_val = ProfessionalTechnical.objects.filter(contractor__user=request.user,
                                                           contractor__layer__is_submitted=False)

        if instant_val.exists():
            protech_form = ProfessionalTechnicalForm(request.POST, request.FILES, instance=instant_val[0])
        else:
            protech_form = ProfessionalTechnicalForm(request.POST, request.FILES)

        if protech_form.is_valid():
            protech = protech_form.save(commit=False)
            protech.contractor = contractor.last()
            protech.save()

            if protech.has_registered:
                if detail_reg_formset.is_valid():
                    instances = detail_reg_formset.save(commit=False)
                    for instance in instances:
                        instance.contractor = contractor.last()
                        instance.save()

        listofworkFormset = modelformset_factory(ListOfWork, form=ListOfWorkForm)
        worklist_formset = listofworkFormset(request.POST, request.FILES, prefix='worklist',
                                             queryset=ListOfWork.objects.all())

        if worklist_formset.is_valid():
            worklist = worklist_formset.save(commit=False)
            for work_list in worklist:
                work_list.contractor = contractor.last()
                work_list.save()

        listofworkFormset_2 = modelformset_factory(ListOfWorkC2, form=ListOfWorkC2Form)
        worklist_formset_2 = listofworkFormset_2(request.POST, request.FILES, prefix='worklistc2',
                                                 queryset=ListOfWorkC2.objects.all())
        if worklist_formset_2.is_valid():
            worklist = worklist_formset_2.save(commit=False)
            for work_list in worklist:
                work_list.contractor = contractor.last()
                work_list.save()

        listofworkFormset_3 = modelformset_factory(ListOfWorkC3, form=ListOfWorkC3Form)
        worklist_formset_3 = listofworkFormset_3(request.POST, request.FILES, prefix='worklistc3',
                                                 queryset=ListOfWorkC3.objects.all())

        if worklist_formset_3.is_valid():
            worklist = worklist_formset_3.save(commit=False)
            for work_list in worklist:
                work_list.contractor = contractor.last()
                work_list.save()

        ListOfMachineryFormset = modelformset_factory(ListOfMachinery, form=ListOfMachineryForm)

        form_sets_machinery = ListOfMachineryFormset(request.POST, request.FILES, prefix='machinary',
                                                     queryset=ListOfMachinery.objects.all())

        if form_sets_machinery.is_valid():
            worklist = form_sets_machinery.save(commit=False)
            for work_list in worklist:
                work_list.contractor = contractor.last()
                work_list.save()

        ListOfMachineryFormsetd2 = modelformset_factory(ListOfMachineryD2, form=ListOfMachineryFormD2)

        form_sets_machinery_d2 = ListOfMachineryFormsetd2(request.POST, request.FILES, prefix='machinaryd2',
                                                          queryset=ListOfMachineryD2.objects.all())

        if form_sets_machinery_d2.is_valid():
            worklist = form_sets_machinery_d2.save(commit=False)
            for work_list in worklist:
                work_list.contractor = contractor.first()
                work_list.save()

        ListOfTechnicalFormset = modelformset_factory(ListofTechnical, form=ListOfTechnicalForm)

        form_sets_technical = ListOfTechnicalFormset(request.POST, request.FILES, prefix='technical',
                                                     queryset=ListofTechnical.objects.all())

        if form_sets_technical.is_valid():
            worklist = form_sets_technical.save(commit=False)
            for work_list in worklist:
                work_list.contractor = contractor.last()
                work_list.save()

        return redirect('enrollment-step-3')


# def worklistForm(request):
#     if request.method == 'POST':
#         print(request.POST)
#         listofworkFormset = modelformset_factory(ListOfWork, form=ListOfWorkForm)
#         worklist_formset = listofworkFormset(request.POST, request.FILES, prefix='worklist',
#                                              queryset=ListOfWork.objects.all())
#
#         if worklist_formset.is_valid():
#             print(worklist_formset.cleaned_data)
#             worklist = worklist_formset.save(commit=False)
#             for work_list in worklist:
#                 work_list.contractor = request.user.contractor
#                 work_list.save()
#             return redirect('enrollment-step-2')
#         else:
#             print(worklist_formset.errors)
#             return HttpResponse('worklist form error')
#
#
# def worklistFormc2(request):
#     if request.method == 'POST':
#         print(request.POST)
#         listofworkFormset = modelformset_factory(ListOfWorkC2, form=ListOfWorkC2Form)
#         worklist_formset = listofworkFormset(request.POST, request.FILES, prefix='worklistc2',
#                                              queryset=ListOfWorkC2.objects.all())
#
#         if worklist_formset.is_valid():
#             print(worklist_formset.cleaned_data)
#             worklist = worklist_formset.save(commit=False)
#             for work_list in worklist:
#                 work_list.contractor = request.user.contractor
#                 work_list.save()
#             return redirect('enrollment-step-2')
#         else:
#             print(worklist_formset.errors)
#             return HttpResponse('worklist form error')
#
#
# def worklistFormc3(request):
#     if request.method == 'POST':
#         print(request.POST)
#         listofworkFormset = modelformset_factory(ListOfWorkC3, form=ListOfWorkC3Form)
#         worklist_formset = listofworkFormset(request.POST, request.FILES, prefix='worklistc3',
#                                              queryset=ListOfWorkC3.objects.all())
#
#         if worklist_formset.is_valid():
#             print(worklist_formset.cleaned_data)
#             worklist = worklist_formset.save(commit=False)
#             for work_list in worklist:
#                 work_list.contractor = request.user.contractor
#                 work_list.save()
#             return redirect('enrollment-step-2')
#         else:
#             print(worklist_formset.errors)
#             return HttpResponse('worklist form error')


# def listofeqipment(request):
#     if request.method == 'POST':
#         print(request.POST)
#         ListOfMachineryFormset = modelformset_factory(ListOfMachinery, form=ListOfMachineryForm)
#
#         form_sets = ListOfMachineryFormset(request.POST, request.FILES, prefix='machinary',
#                                            queryset=ListOfMachinery.objects.all())
#
#         if form_sets.is_valid():
#             worklist = form_sets.save(commit=False)
#             for work_list in worklist:
#                 work_list.contractor = request.user.contractor
#                 work_list.save()
#             return redirect('enrollment-step-2')
#         else:
#             print(form_sets.errors)
#             return HttpResponse('worklist form error')


def listofeqipmentd2(request):
    if request.method == 'POST':
        print(request.POST)
        ListOfMachineryFormsetd2 = modelformset_factory(ListOfMachineryD2, form=ListOfMachineryFormD2)

        form_sets = ListOfMachineryFormsetd2(request.POST, request.FILES, prefix='machinaryd2',
                                             queryset=ListOfMachineryD2.objects.all())

        if form_sets.is_valid():
            worklist = form_sets.save(commit=False)
            for work_list in worklist:
                work_list.contractor = request.user.contractor
                work_list.save()
            protech = ProfessionalTechnical.objects.filter(contractor__user=request.user)

            if protech.exists():
                protech = protech[0]
                protech.desc_of_work_annex_d2 = True
                protech.save()
                print('saved..', protech.desc_of_work_annex_d2)

            print(protech.desc_of_work_annex_d2)
            return redirect('enrollment-step-2')


def listtechnical(request):
    if request.method == 'POST':
        print(request.POST)
        ListOfTechnicalFormset = modelformset_factory(ListofTechnical, form=ListOfTechnicalForm)

        form_sets = ListOfTechnicalFormset(request.POST, request.FILES, prefix='technical',
                                           queryset=ListofTechnical.objects.all())

        if form_sets.is_valid():
            worklist = form_sets.save(commit=False)
            for work_list in worklist:
                work_list.contractor = request.user.contractor
                work_list.save()
            protech = ProfessionalTechnical.objects.filter(contractor__user=request.user)

            if protech.exists():
                protech = protech[0]
                protech.list_of_work_e1 = True
                protech.save()
                print('saved..', protech.list_of_work_e1)

            print(protech.desc_of_work_annex_d2)
            return redirect('enrollment-step-2')


class EnrollmentStep3View(TemplateView):
    template_name = 'step-3.html'

    def get(self, request, *args, **kwargs):

        financial_capacity = FinancialCapacity.objects.filter(contractor__user=request.user,
                                                              contractor__layer__is_submitted=False)
        if financial_capacity.exists():
            form = FinancialCapacityForm(instance=financial_capacity[0])
        else:
            form = FinancialCapacityForm()

        balance_sheet = BalanceSheet.objects.filter(contractor__user=request.user,
                                                    contractor__layer__is_submitted=False)
        if balance_sheet.exists():
            balance_form = BalanceSheetForm(instance=balance_sheet[0])
        else:
            balance_form = BalanceSheetForm()

        context = {
            'form': form,
            'balance_form': balance_form,
            'type': 'enrollment',
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=False)

        financial_capacity = FinancialCapacity.objects.filter(contractor__user=request.user,
                                                              contractor__layer__is_submitted=False)

        if financial_capacity.exists():

            form = FinancialCapacityForm(request.POST, request.FILES, instance=financial_capacity[0])
        else:
            form = FinancialCapacityForm(request.POST, request.FILES)

        if form.is_valid():
            financial_capacity_obj = form.save(commit=False)
            financial_capacity_obj.contractor = contractor.last()
            financial_capacity_obj.save()

        balance_sheet = BalanceSheet.objects.filter(contractor__user=request.user,
                                                    contractor__layer__is_submitted=False)

        if balance_sheet.exists():
            balance_form = BalanceSheetForm(request.POST, request.FILES, instance=balance_sheet[0])
        else:
            balance_form = BalanceSheetForm(request.POST, request.FILES)

        if balance_form.is_valid():
            balance_form = balance_form.save(commit=False)
            balance_form.contractor = contractor.last()
            balance_form.save()
        else:
            print('*******', balance_form.errors)

        return redirect('enrollment-step-4')


class EnrollmentStep4View(TemplateView):
    template_name = 'step-4.html'

    def get(self, request, *args, **kwargs):
        annexf_formset = modelformset_factory(AnnexF, form=AnnexFForm, extra=1)
        annexf_form = annexf_formset(prefix='annexf',
                                     queryset=AnnexF.objects.filter(contractor__user=request.user,
                                                                    contractor__layer__is_submitted=False))

        ListOfMachineryFormset = modelformset_factory(ListOfMachinery, form=ListOfMachineryForm)

        form_sets = ListOfMachineryFormset(prefix='machinary', queryset=ListOfMachinery.objects.all())

        legal_capacity = LegalCapacity.objects.filter(contractor__user=request.user,
                                                      contractor__layer__is_submitted=False)
        legal_capacity_obj = ''
        if legal_capacity.exists():
            legal_capacity_obj = legal_capacity[0]
            legal_form = LegalCapacityForm(instance=legal_capacity[0])
        else:
            legal_form = LegalCapacityForm()

        context = {
            'legal_form': legal_form,
            'legal_capacity_obj': legal_capacity_obj,
            'annexf_form': annexf_form,
            'type': 'enrollment',
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=False)
        enclosed_Annex_F = request.POST.get('annex_f')
        legal_capacity = LegalCapacity.objects.filter(contractor__user=request.user,
                                                      contractor__layer__is_submitted=False)
        if legal_capacity.exists():
            legal_form = LegalCapacityForm(request.POST, request.FILES, instance=legal_capacity[0])
        else:
            legal_form = LegalCapacityForm(request.POST, request.FILES)

        if legal_form.is_valid():
            legalcapacity = legal_form.save(commit=False)
            legalcapacity.contractor = contractor.last()
            if enclosed_Annex_F == 'on':
                legalcapacity.enclosed_Annex_F = True
            else:
                legalcapacity.enclosed_Annex_F = False

            legalcapacity.save()

        else:
            print(legal_form.errors)
            return HttpResponse('error')

        annexf_formset = modelformset_factory(AnnexF, form=AnnexFForm)
        annexf_form = annexf_formset(request.POST, request.FILES, prefix='annexf',
                                     queryset=AnnexF.objects.filter(contractor=contractor.last()))

        if annexf_form.is_valid():
            annexf = annexf_form.save(commit=False)
            for anxf in annexf:
                anxf.contractor = contractor.last()
                anxf.save()
        else:
            print(annexf_form.errors)
            return HttpResponse('errror.')

        return redirect('enrollment-step-5')


class EnrollmentStep5View(TemplateView):
    template_name = 'step-5.html'

    def get(self, request, *args, **kwargs):
        contractor = Contractor.objects.filter(user=request.user)
        contractor_id = contractor[0].id

        texatation = Texation.objects.filter(contractor__user=request.user, contractor__layer__is_submitted=False)
        vats = Vat.objects.filter(contractor__user=request.user, contractor__layer__is_submitted=False)
        tins = Tin.objects.filter(contractor__user=request.user, contractor__layer__is_submitted=False)
        texation_obj = ''
        if texatation.exists():
            texation_form = TexationForm(instance=texatation[0])
            texation_obj = texatation[0]
        else:
            texation_form = TexationForm()

        if vats.exists():
            vat_formset = modelformset_factory(Vat, form=VatForm, extra=0)
        else:
            vat_formset = modelformset_factory(Vat, form=VatForm, extra=1)

        vat_form = vat_formset(prefix='vat', queryset=Vat.objects.filter(contractor__user=request.user,
                                                                         contractor__layer__is_submitted=False))

        print(Vat.objects.filter(contractor__user=request.user, contractor__layer__is_submitted=False))

        if tins.exists():
            tin_formset = modelformset_factory(Tin, form=TinForm, extra=0)
        else:
            tin_formset = modelformset_factory(Tin, form=TinForm, extra=1)
        tin_form = tin_formset(prefix='tin', queryset=Tin.objects.filter(contractor__user=request.user,
                                                                         contractor__layer__is_submitted=False))

        context = {
            'vat_form': vat_form,
            'tin_form': tin_form,
            'texation_form': texation_form,
            'texation_obj': texation_obj,
            'contractor': contractor[0],
            'type': 'enrollment',
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=False)

        contractor_id = contractor.first().id

        vat_formset = modelformset_factory(Vat, form=VatForm, extra=0)
        vat_form = vat_formset(request.POST, request.FILES, prefix='vat', queryset=Vat.objects.all())

        if vat_form.is_valid():
            vat_form_set = vat_form.save(commit=False)
            for vat_form in vat_form_set:
                vat_form.contractor = contractor.first()
                vat_form.save()

        tin_formset = modelformset_factory(Tin, form=TinForm, extra=0)
        tin_form = tin_formset(request.POST, request.FILES, prefix='tin', queryset=Tin.objects.all())

        if tin_form.is_valid():
            tin_form_set = tin_form.save(commit=False)
            for tin_form in tin_form_set:
                tin_form.contractor = contractor.first()
                tin_form.save()

        declaration_checkbox = request.POST.get('declaration_checkbox')
        texatation = Texation.objects.filter(contractor=contractor.first())
        if texatation.exists():
            texation_form = TexationForm(request.POST, request.FILES, instance=texatation[0])
        else:
            texation_form = TexationForm(request.POST, request.FILES)

        if texation_form.is_valid():
            texation_form_obj = texation_form.save(commit=False)
            texation_form_obj.contractor = contractor.first()
            if declaration_checkbox == 'on':
                texation_form_obj.declaration = True
            else:
                texation_form_obj.declaration = False

            texation_form_obj.save()

        else:
            print(texation_form.errors)

        return redirect('contractor_details_view', contractor_id)


class ContactView(TemplateView):
    template_name = 'frontend/contact.html'


class GuideLineView(TemplateView):
    template_name = 'frontend/guidelines.html'


@login_required()
def contractor_list_view(request, role, division):
    if request.user.roles == 'COMMITTEE_MEMBER':
        layer_form = LayerFormStatus.objects.filter(
            Q(layer='COMMITTEE_MEMBER', is_submitted=True, form_payment=True,
              contractor__application_division=role, contractor__applied_for=division) & Q(
                meetting__metting_members__member=request.user))

    elif request.user.roles == 'COMMITTEE_SECRETARY':
        layer_form = LayerFormStatus.objects.filter(Q(layer='COMMITTEE_MEMBER', is_submitted=True, form_payment=True,
                                                      contractor__application_division=role,
                                                      contractor__applied_for=division) |
                                                    Q(layer='COMMITTEE_SECRETARY', s_submitted=True, form_payment=True,
                                                      contractor__application_division=role,
                                                      contractor__applied_for=division) | Q(layer='SECRETARY') | Q(
            layer='DMD') | Q(layer='MD'))

    elif request.user.roles == 'DS_ADMIN':
        layer_form = LayerFormStatus.objects.filter(
            Q(layer='DS_ADMIN', is_submitted=True, form_payment=True, contractor__application_division=role,
              contractor__applied_for=division))

    elif request.user.roles == 'AS_ADMIN':
        layer_form = LayerFormStatus.objects.filter(
            Q(contractor__application_division=role, contractor__applied_for=division, layer='DS_ADMIN',
              form_payment=True, layer_view__user=request.user))

    elif request.user.roles == '0S_ADMIN':
        layer_form = LayerFormStatus.objects.filter(
            Q(contractor__application_division=role, contractor__applied_for=division, layer='DS_ADMIN',
              form_payment=True, layer_view__user=request.user))
    elif request.user.roles == 'CO_ADMIN':
        layer_form = LayerFormStatus.objects.filter(
            Q(contractor__application_division=role, contractor__applied_for=division, layer='DS_ADMIN',
              form_payment=True, layer_view__user=request.user))

    elif request.user.roles == 'SECRETARY':
        layer_form = LayerFormStatus.objects.filter(
            Q(layer='SECRETARY', contractor__application_division=role, is_submitted=True, form_payment=True) | Q(
                layer='DMD', contractor__application_division=role, is_submitted=True, form_payment=True) | Q(
                layer='MD', contractor__application_division=role, is_submitted=True, form_payment=True))

    elif request.user.roles == 'DMD':
        layer_form = LayerFormStatus.objects.filter(
            Q(layer='DMD', contractor__application_division=role, is_submitted=True, form_payment=True) | Q(layer='MD',
                                                                                                            contractor__application_division=role,
                                                                                                            is_submitted=True,
                                                                                                            form_payment=True))
    elif request.user.roles == 'MD':
        layer_form = LayerFormStatus.objects.filter(layer='MD', contractor__application_division=role,
                                                    is_submitted=True, form_payment=True)

    else:
        layer_form = LayerFormStatus.objects.filter(is_submitted=True, form_payment=True,
                                                    contractor__application_division=role,
                                                    contractor__applied_for=division)

    context = {
        'layer_form': layer_form,
        'division': division,
        'role': role,
    }
    return render(request, 'contractor/contractor_lists.html', context)


def approved_contractor_list_view(request):
    layer_form = LayerFormStatus.objects.filter(application_approved=True)
    context = {
        'layer_form': layer_form,
    }
    return render(request, 'contractor/approved_application_lists.html', context)


def ContractorDetailsView(request, pk):
    contractor = get_object_or_404(Contractor, id=pk)
    partners = ContractorPartners.objects.filter(contractor=contractor)
    protech = ProfessionalTechnical.objects.filter(contractor=contractor).first()
    details_reg = DetailsRegistration.objects.filter(contractor=contractor)
    list_work_c1 = ListOfWork.objects.filter(contractor=contractor)
    list_work_c2 = ListOfWorkC2.objects.filter(contractor=contractor)
    list_work_c3 = ListOfWorkC3.objects.filter(contractor=contractor)
    listofworkC3Formset = modelformset_factory(ListOfWorkC3, form=ListOfWorkC3FormView, extra=0)
    worklist_formsetc3 = listofworkC3Formset(prefix='worklistc3', queryset=ListOfWorkC3.objects.filter(
        contractor=contractor))

    list_of_machinery = ListOfMachinery.objects.filter(contractor=contractor)
    list_machinery_d2 = ListOfMachineryD2.objects.filter(contractor=contractor)
    list_technical = ListofTechnical.objects.filter(contractor=contractor)
    finance_capacity = FinancialCapacity.objects.filter(contractor=contractor).first()
    balance_sheet = BalanceSheet.objects.filter(contractor=contractor).first()
    legal_capacity = LegalCapacity.objects.filter(contractor=contractor).first()
    annex_f = AnnexF.objects.filter(contractor=contractor)
    vats = Vat.objects.filter(contractor=contractor)
    tins = Tin.objects.filter(contractor=contractor)
    texation = Texation.objects.filter(contractor=contractor).first()

    context = {
        'contractor': contractor,
        'partners': partners,
        'protech': protech,
        'details_reg': details_reg,
        'list_work_c1': list_work_c1,
        'list_work_c2': list_work_c2,
        'list_work_c3': list_work_c3,
        'worklist_formset_c3': worklist_formsetc3,
        'list_of_machinery': list_of_machinery,
        'list_machinery_d2': list_machinery_d2,
        'list_technical': list_technical,
        'finance_capacity': finance_capacity,
        'balance_sheet': balance_sheet,
        'legal_capacity': legal_capacity,
        'annex_f': annex_f,
        'vats': vats,
        'tins': tins,
        'texation': texation

    }
    return render(request, 'contractor/documents_details.html', context)


def ContractorDetailsViewBackend(request, pk):
    contractor = get_object_or_404(Contractor, id=pk)
    partners = ContractorPartners.objects.filter(contractor=contractor)
    protech = ProfessionalTechnical.objects.filter(contractor=contractor).first()
    details_reg = DetailsRegistration.objects.filter(contractor=contractor)
    list_work_c1 = ListOfWork.objects.filter(contractor=contractor)
    list_work_c2 = ListOfWorkC2.objects.filter(contractor=contractor)
    list_work_c3 = ListOfWorkC3.objects.filter(contractor=contractor)
    listofworkC3Formset = modelformset_factory(ListOfWorkC3, form=ListOfWorkC3FormView, extra=0)
    worklist_formsetc3 = listofworkC3Formset(prefix='worklistc3', queryset=ListOfWorkC3.objects.filter(
        contractor=contractor))

    list_of_machinery = ListOfMachinery.objects.filter(contractor=contractor)
    list_machinery_d2 = ListOfMachineryD2.objects.filter(contractor=contractor)
    list_technical = ListofTechnical.objects.filter(contractor=contractor)
    finance_capacity = FinancialCapacity.objects.filter(contractor=contractor).first()
    balance_sheet = BalanceSheet.objects.filter(contractor=contractor).first()
    legal_capacity = LegalCapacity.objects.filter(contractor=contractor).first()
    annex_f = AnnexF.objects.filter(contractor=contractor)
    vats = Vat.objects.filter(contractor=contractor)
    tins = Tin.objects.filter(contractor=contractor)
    texation = Texation.objects.filter(contractor=contractor).first()

    context = {
        'contractor': contractor,
        'partners': partners,
        'protech': protech,
        'details_reg': details_reg,
        'list_work_c1': list_work_c1,
        'list_work_c2': list_work_c2,
        'list_work_c3': list_work_c3,
        'worklist_formset_c3': worklist_formsetc3,
        'list_of_machinery': list_of_machinery,
        'list_machinery_d2': list_machinery_d2,
        'list_technical': list_technical,
        'finance_capacity': finance_capacity,
        'balance_sheet': balance_sheet,
        'legal_capacity': legal_capacity,
        'annex_f': annex_f,
        'vats': vats,
        'tins': tins,
        'texation': texation

    }
    return render(request, 'contractor/documents_details_backend.html', context)


def ContractorPreviewView(request, pk):
    contractor = get_object_or_404(Contractor, id=pk)
    partners = ContractorPartners.objects.filter(contractor=contractor)
    protech = ProfessionalTechnical.objects.filter(contractor=contractor).first()
    details_reg = DetailsRegistration.objects.filter(contractor=contractor)
    list_work_c1 = ListOfWork.objects.filter(contractor=contractor)
    list_work_c2 = ListOfWorkC2.objects.filter(contractor=contractor)
    list_work_c3 = ListOfWorkC3.objects.filter(contractor=contractor)
    listofworkC3Formset = modelformset_factory(ListOfWorkC3, form=ListOfWorkC3FormView, extra=0)
    worklist_formsetc3 = listofworkC3Formset(prefix='worklistc3', queryset=ListOfWorkC3.objects.filter(
        contractor=contractor))

    list_of_machinery = ListOfMachinery.objects.filter(contractor=contractor)
    list_machinery_d2 = ListOfMachineryD2.objects.filter(contractor=contractor)
    list_technical = ListofTechnical.objects.filter(contractor=contractor)
    finance_capacity = FinancialCapacity.objects.filter(contractor=contractor).first()
    balance_sheet = BalanceSheet.objects.filter(contractor=contractor).first()
    legal_capacity = LegalCapacity.objects.filter(contractor=contractor).first()
    annex_f = AnnexF.objects.filter(contractor=contractor)
    vats = Vat.objects.filter(contractor=contractor)
    tins = Tin.objects.filter(contractor=contractor)
    texation = Texation.objects.filter(contractor=contractor).first()

    context = {
        'contractor': contractor,
        'partners': partners,
        'protech': protech,
        'details_reg': details_reg,
        'list_work_c1': list_work_c1,
        'list_work_c2': list_work_c2,
        'list_work_c3': list_work_c3,
        'worklist_formset_c3': worklist_formsetc3,
        'list_of_machinery': list_of_machinery,
        'list_machinery_d2': list_machinery_d2,
        'list_technical': list_technical,
        'finance_capacity': finance_capacity,
        'balance_sheet': balance_sheet,
        'legal_capacity': legal_capacity,
        'annex_f': annex_f,
        'vats': vats,
        'tins': tins,
        'texation': texation

    }
    return render(request, 'contractor/documents_details.html', context)


def assesmentView(request, id):
    assessment_eval_sign = ''
    assess_eval = ''
    assess_eval_obj = ''
    confirmed_member_count = ''
    rejected_member_count = ''
    contractor = get_object_or_404(Contractor, id=id)
    layer_form = get_object_or_404(LayerFormStatus, contractor=contractor)
    texation = Texation.objects.filter(contractor=contractor).first()

    member_user_count = len(User.objects.filter(roles='COMMITTEE_MEMBER'))

    assessment = Assessment.objects.filter(contractor=contractor)
    print('assesment form', assessment)
    if assessment.exists():
        if request.user.roles == 'COMMITTEE_SECRETARY':
            form = AssessmentForm(request.POST or None, instance=assessment[0])
        else:
            form = AssessmentMemberForm(request.POST or None, instance=assessment[0])
    else:
        if request.user.roles == 'COMMITTEE_SECRETARY':
            form = AssessmentForm(request.POST or None)
        else:
            form = AssessmentMemberForm(request.POST or None)
    if assessment.exists():
        assess_eval = AssessmentEvaluation.objects.filter(user=request.user, assessment=assessment[0])
        confirmed_member_count = len(
            AssessmentEvaluation.objects.filter(assessment=assessment[0], is_approved='CONFIRMED',
                                                user__roles='COMMITTEE_MEMBER'))
        rejected_member_count = len(
            AssessmentEvaluation.objects.filter(assessment=assessment[0], is_approved='REJECTED',
                                                user__roles='COMMITTEE_MEMBER'))

        print('Here*****', confirmed_member_count, rejected_member_count)
        if layer_form.form_status != 'CONFIRMED':
            assessment_eval_sign = AssessmentEvaluation.objects.filter(is_approved='CONFIRMED',
                                                                       assessment=assessment[0],
                                                                       user__roles='COMMITTEE_MEMBER')
        else:
            assessment_eval_sign = layer_form.meetting.metting_members.all()

        if assess_eval.exists():
            assess_eval_obj = assess_eval[0]
        else:
            assess_eval_obj = False

    print('*********', assess_eval_obj)
    if 'submit' in request.POST:
        if form.is_valid():
            assessment = form.save(commit=False)
            assessment.contractor = contractor
            assessment.is_submitted = True
            assessment.save()
            layer_form.layer = 'COMMITTEE_MEMBER'
            layer_form.form_status = 'REVIEW'
            layer_form.save()
            messages.success(request, 'Form is Forwarded, Wait for Members Approval')
            return HttpResponseRedirect(request.path_info)

    if 'member_submit' in request.POST:
        if not request.user.signature:
            messages.error(request, 'You must have to add your signature first')
            return redirect('dashboard')
        member_accepted = request.POST.get('member_accepted')
        assessment, created = AssessmentEvaluation.objects.get_or_create(user=request.user, assessment=assessment[0])
        assessment.is_approved = member_accepted
        assessment.save()
        messages.success(request, 'Response is sent over to committee secretary')
        return HttpResponseRedirect(request.path_info)

    if 'reject' in request.POST:
        layer_form.layer = 'COMMITTEE_SECRETARY'
        layer_form.form_status = 'REJECT'
        layer_form.save()
        messages.success(request, 'Form is  Rejected by committee secretary Layer')
        return HttpResponseRedirect(request.path_info)

    if 'confirm' in request.POST:
        class_name = request.POST['member_class']
        layer_form.layer = 'COMMITTEE_SECRETARY'
        layer_form.form_status = 'CONFIRMED'
        layer_form.class_name = class_name
        layer_form.application_approved = True
        layer_form.save()
        assessment, created = AssessmentEvaluation.objects.get_or_create(user=request.user, assessment=assessment[0])
        assessment.is_approved = 'CONFIRMED'
        assessment.save()
        messages.success(request, 'Form is Sent to Secretary Layer')
        return HttpResponseRedirect(request.path_info)

    if 'secretary_submit' in request.POST:
        member_accepted = request.POST.get('member_accepted')
        print(member_accepted)
        assessment, created = AssessmentEvaluation.objects.get_or_create(user=request.user, assessment=assessment[0])
        assessment.is_approved = member_accepted
        assessment.save()
        if member_accepted == 'CONFIRMED':
            layer_form.layer = 'DMD'
            layer_form.form_status = 'PENDING'
            layer_form.save()
            messages.success(request, 'Form is sent over to DMD Layer')
        elif member_accepted == 'REJECTED':
            layer_form.layer = 'COMMITTEE_SECRETARY'
            layer_form.form_status = 'PENDING'
            layer_form.save()
            messages.success(request, 'Form is sent back to Committee Layer')

    if 'dmd_submit' in request.POST:
        member_accepted = request.POST.get('member_accepted')
        print(member_accepted)
        assessment, created = AssessmentEvaluation.objects.get_or_create(user=request.user,
                                                                         assessment=assessment[0])
        assessment.is_approved = member_accepted
        assessment.save()
        if member_accepted == 'CONFIRMED':
            layer_form.layer = 'MD'
            layer_form.form_status = 'PENDING'
            layer_form.save()
            messages.success(request, 'Form is sent over to MD Layer')
        elif member_accepted == 'REJECTED':
            layer_form.layer = 'SECRETARY'
            layer_form.form_status = 'PENDING'
            layer_form.save()
            messages.success(request, 'Form is sent back to Secretary Layer')

    if 'md_submit' in request.POST:
        member_accepted = request.POST.get('member_accepted')
        print(member_accepted)
        assessment, created = AssessmentEvaluation.objects.get_or_create(user=request.user,
                                                                         assessment=assessment[0])
        assessment.is_approved = member_accepted
        assessment.save()
        if member_accepted == 'CONFIRMED':
            layer_form.layer = 'MD'
            layer_form.form_status = 'CONFIRMED'
            layer_form.save()
            messages.success(request, 'Contractor is approved , Form Accepted by all layer members')
        elif member_accepted == 'REJECTED':
            layer_form.layer = 'DMD'
            layer_form.form_status = 'PENDING'
            layer_form.save()
            messages.success(request, 'Form is sent back to DMD Layer')

        return redirect('dashboard')

    context = {
        'form': form,
        'confirmed_member_count': confirmed_member_count,
        'rejected_member_count': rejected_member_count,
        'contractor': contractor,
        'assess_eval_obj': assess_eval_obj,
        'layer_form': layer_form,
        'assessment_eval_sign': assessment_eval_sign,
        'texation': texation,
    }
    return render(request, 'contractor/assesment_form.html', context)


def ProfileView(request):
    contractor = Contractor.objects.filter(user=request.user)
    if contractor.exists():
        contractor_obj = contractor.last()

    tracking_no = Contractor.objects.filter(user=request.user)[0].layer.tracking_id

    partners = ContractorPartners.objects.filter(contractor=contractor_obj)
    protech = ProfessionalTechnical.objects.filter(contractor=contractor_obj).first()
    details_reg = DetailsRegistration.objects.filter(contractor=contractor_obj)
    list_work_c1 = ListOfWork.objects.filter(contractor=contractor_obj)
    list_work_c2 = ListOfWorkC2.objects.filter(contractor=contractor_obj)
    list_work_c3 = ListOfWorkC3.objects.filter(contractor=contractor_obj)
    listofworkC3Formset = modelformset_factory(ListOfWorkC3, form=ListOfWorkC3FormView, extra=0)
    worklist_formsetc3 = listofworkC3Formset(prefix='worklistc3', queryset=ListOfWorkC3.objects.filter(
        contractor=contractor_obj))

    list_of_machinery = ListOfMachinery.objects.filter(contractor=contractor_obj)
    list_machinery_d2 = ListOfMachineryD2.objects.filter(contractor=contractor_obj)
    list_technical = ListofTechnical.objects.filter(contractor=contractor_obj)
    finance_capacity = FinancialCapacity.objects.filter(contractor=contractor_obj).first()
    balance_sheet = BalanceSheet.objects.filter(contractor=contractor_obj).first()
    legal_capacity = LegalCapacity.objects.filter(contractor=contractor_obj).first()
    annex_f = AnnexF.objects.filter(contractor=contractor_obj)
    vats = Vat.objects.filter(contractor=contractor_obj)
    tins = Tin.objects.filter(contractor=contractor_obj)
    list_work_c3 = ListOfWorkC3.objects.filter(contractor=contractor_obj)

    texation = Texation.objects.filter(contractor=contractor_obj).first()

    context = {
        'contractor': contractor_obj,
        'partners': partners,
        'protech': protech,
        'details_reg': details_reg,
        'list_work_c1': list_work_c1,
        'list_work_c2': list_work_c2,
        'list_work_c3': list_work_c3,
        'worklist_formset_c3': worklist_formsetc3,
        'list_of_machinery': list_of_machinery,
        'list_machinery_d2': list_machinery_d2,
        'list_technical': list_technical,
        'finance_capacity': finance_capacity,
        'balance_sheet': balance_sheet,
        'legal_capacity': legal_capacity,
        'annex_f': annex_f,
        'vats': vats,
        'tins': tins,
        'texation': texation,

    }
    return render(request, 'contractor/documents_preview.html', context)


@csrf_exempt
def deletePartnerView(request, id):
    partners = get_object_or_404(ContractorPartners, id=id)
    partners.delete()
    payload = {'delete': 'ok'}
    return JsonResponse(payload)


@csrf_exempt
def regdeleteView(request, id):
    reg = get_object_or_404(DetailsRegistration, id=id)
    reg.delete()
    payload = {'delete': 'ok'}
    return JsonResponse(payload)


@csrf_exempt
def worklistdeleteView(request, id):
    # return HttpResponse('delete')
    worklist = get_object_or_404(ListOfWork, id=id)
    worklist.delete()
    payload = {'delete': 'ok'}
    return JsonResponse(payload)


@csrf_exempt
def worklistc2deleteView(request, id):
    worklist = get_object_or_404(ListOfWorkC2, id=id)
    worklist.delete()
    payload = {'delete': 'ok'}
    return JsonResponse(payload)


@csrf_exempt
def worklistc3deleteView(request, id):
    worklist = get_object_or_404(ListOfWorkC3, id=id)
    worklist.delete()
    payload = {'delete': 'ok'}
    return JsonResponse(payload)


@csrf_exempt
def machinerydeleteView(request, id):
    worklist = get_object_or_404(ListOfMachinery, id=id)
    worklist.delete()
    payload = {'delete': 'ok'}
    return JsonResponse(payload)


@csrf_exempt
def machineryd2deleteView(request, id):
    worklist = get_object_or_404(ListOfMachineryD2, id=id)
    worklist.delete()
    payload = {'delete': 'ok'}
    return JsonResponse(payload)


@csrf_exempt
def technicaldeleteView(request, id):
    worklist = get_object_or_404(ListofTechnical, id=id)
    worklist.delete()
    payload = {'delete': 'ok'}
    return JsonResponse(payload)


@csrf_exempt
def annexFDelete(request, id):
    annexf = get_object_or_404(AnnexF, id=id)
    annexf.delete()
    payload = {'delete': 'ok'}
    return JsonResponse(payload)


@csrf_exempt
def vatDelete(request, id):
    vat = get_object_or_404(Vat, id=id)
    vat.delete()
    payload = {'delete': 'ok'}
    return JsonResponse(payload)


@csrf_exempt
def tinDelete(request, id):
    tin = get_object_or_404(Tin, id=id)
    tin.delete()
    payload = {'delete': 'ok'}
    return JsonResponse(payload)


@csrf_exempt
def successView(request):
    # response = request.POST
    if request.POST:
        status = request.POST.get('status')
        tran_id = request.POST.get('tran_id')
        if status == 'VALID':
            trans_obj = Transaction.objects.get(tran_id=tran_id)
            trans_obj.status = True
            trans_obj.save()
            layer = get_object_or_404(LayerFormStatus, contractor=trans_obj.contractor)
            if trans_obj.transaction_type == 1:
                layer.form_payment = True
                layer.save()
            elif trans_obj.transaction_type == 3:
                layer.renewal_form_payment = True
                layer.save()

            elif trans_obj.transaction_type == 2:
                layer.chalan_payment = True
                layer.save()
        else:
            trans_obj = Transaction.objects.get(tran_id=tran_id)
            trans_obj.delete()
            try:
                del request.session['tracking_number']
            except KeyError as e:
                messages.success(request, 'Not Found')
                print("The requested Session variable is already deleted")

        tran_date = request.POST.get('tran_date')
        payment_method = request.POST.get('payment_method')
        bank_tran_id = request.POST.get('bank_tran_id')
        amount = request.POST.get('amount')
        card_issuer = request.POST.get('card_issuer')
        context = {
            'trans_obj': trans_obj,
            'trans_id': tran_id,
            'trans_time': tran_date,
            'payment_method': payment_method,
            'bank_tran_id': bank_tran_id,
            'amount': amount,
            'card_issuer': card_issuer

        }
        return render(request, 'ssl/success_page.html', context)
    else:
        return HttpResponse('Method Not Allowed')

    #
    # email = request.POST.get('cus_email')
    #

    # card_brand = request.POST.get('card_brand')
    # bank_tran_id = request.POST.get('bank_tran_id')

    #
    # return HttpResponse(email)
    #
    # contractor = Contractor.objects.get(user__email=email)
    # print(contractor)
    #
    # if status == 'valid':


def failureView(request):
    return HttpResponse('error!!!')


import random
from datetime import date


def tracking_no_generate(dept):
    today = date.today()
    year = str(today.year)
    random_num = random.randint(1000, 9999)
    tracking_id = year + str(dept) + str(random_num)
    return str(tracking_id)


def contractor_form_submit(request):
    if request.user:
        layer_status = LayerFormStatus.objects.filter(contractor__user=request.user, is_submitted=False).first()
        layer_status.is_submitted = True
        tracking_id = tracking_no_generate(layer_status.contractor.application_division)
        layer_status.tracking_id = tracking_id
        layer_status.save()
        return redirect('ssl-payment', tran_type='1')

    else:
        return HttpResponse('error')


def renewal_form_submit(request):
    if request.user:
        layer_status = LayerFormStatus.objects.filter(contractor__user=request.user, renewal_form_payment=False).first()
        layer_status.renewal_submitted = True
        layer_status.save()
        return redirect('ssl-payment-renewal', tran_type='3')

    else:
        return HttpResponse('error')


def upgrade_form_submit(request):
    if request.user:
        layer_status = LayerFormStatus.objects.filter(contractor__user=request.user, upgrade_submitted=False).first()
        layer_status.upgrade_submitted = True
        layer_status.save()
        return redirect('ssl-payment-upgrade', tran_type='4')

    else:
        return HttpResponse('error')


def integrate_ssl_commrz(request, tran_type):
    contractor = Contractor.objects.filter(user=request.user, layer__is_submitted=True).last()
    layer = LayerFormStatus.objects.get(contractor=contractor)
    if tran_type == 1:
        if layer.is_submitted and layer.form_payment:
            return HttpResponse('You payment has been done already,')
        total_amount = 1000
    else:
        if layer.chalan_payment:
            return HttpResponse('You chalan payment has been done already,')
        total_amount = 5000

    base_url = "{0}://{1}".format(request.scheme, request.get_host())
    success_url = base_url + '/success-url/'

    ssl_settings = SSLSettings.objects.all()
    if ssl_settings.exists():
        store_id = ssl_settings[0].store_id
        store_pass = ssl_settings[0].store_pass
    else:
        return HttpResponse('No SSL Store added Yet')

    tran_id = uuid.uuid1()

    contractor_name = contractor.contractor_name
    email = contractor.user.email
    add_1 = contractor.mailing_address
    add_2 = contractor.address
    phone = contractor.user.phone
    fax_no = contractor.fax_no

    if layer.is_submitted:
        transaction, _created = Transaction.objects.get_or_create(contractor=contractor, paid_amount=total_amount,
                                                                  transaction_type=tran_type)
        if _created:
            transaction.tran_id = tran_id
            transaction.save()
    else:
        return HttpResponse('Form is not submitted Yet')

    data = {
        'store_id': store_id,
        'store_passwd': store_pass,
        'total_amount': total_amount,
        'currency': 'BDT',
        'tran_id': tran_id,
        'product_category': 'toys',
        'success_url': success_url,
        'fail_url': '',
        'cancel_url': '',
        'emi_option': 0,
        'cus_name': contractor_name,
        'cus_email': email,
        'cus_add1': add_1,
        'cus_add2': add_2,
        'cus_city': 'Dhaka',
        'cus_state': 'Dhaka',
        'cus_postcode': '',
        'cus_country': "Bangladesh",
        'cus_phone': phone,
        'cus_fax': fax_no,
        'shipping_method': 'NO',
        'num_of_item': 1,
        'value_a': 'ref001_A',
        'value_b': 'ref002_B',
        'value_c': 'ref003_C',
        'value_d': 'ref004_D',
        'product_name': 'Dhaka Wasa',
        'product_profile': 'Application Form',
    }

    x = requests.post("https://sandbox.sslcommerz.com/gwprocess/v4/api.php", data=data)
    print("**********here is the response****")
    response = x.json()
    gateway = response['GatewayPageURL']
    # return JsonResponse(response)

    return redirect(gateway)


def integrate_ssl_commrz_renewal(request, tran_type):
    contractor = Contractor.objects.filter(user=request.user, layer__renewal_submitted=True).last()
    layer = LayerFormStatus.objects.get(contractor=contractor)
    if tran_type == 3:
        if layer.renewal_submitted and layer.renewal_form_payment:
            return HttpResponse('You payment has been done already,')
        total_amount = 1000

    base_url = "{0}://{1}".format(request.scheme, request.get_host())
    success_url = base_url + '/success-url/'

    ssl_settings = SSLSettings.objects.all()
    if ssl_settings.exists():
        store_id = ssl_settings[0].store_id
        store_pass = ssl_settings[0].store_pass
    else:
        return HttpResponse('No SSL Store added Yet')

    tran_id = uuid.uuid1()

    contractor_name = contractor.contractor_name
    email = contractor.user.email
    add_1 = contractor.mailing_address
    add_2 = contractor.address
    phone = contractor.user.phone
    fax_no = contractor.fax_no

    if layer.renewal_submitted:
        transaction, _created = Transaction.objects.get_or_create(contractor=contractor, paid_amount=total_amount,
                                                                  transaction_type=tran_type)
        if _created:
            transaction.tran_id = tran_id
            transaction.save()
    else:
        return HttpResponse('Form is not submitted Yet')

    data = {
        'store_id': store_id,
        'store_passwd': store_pass,
        'total_amount': total_amount,
        'currency': 'BDT',
        'tran_id': tran_id,
        'product_category': 'toys',
        'success_url': success_url,
        'fail_url': '',
        'cancel_url': '',
        'emi_option': 0,
        'cus_name': contractor_name,
        'cus_email': email,
        'cus_add1': add_1,
        'cus_add2': add_2,
        'cus_city': 'Dhaka',
        'cus_state': 'Dhaka',
        'cus_postcode': '',
        'cus_country': "Bangladesh",
        'cus_phone': phone,
        'cus_fax': fax_no,
        'shipping_method': 'NO',
        'num_of_item': 1,
        'value_a': 'ref001_A',
        'value_b': 'ref002_B',
        'value_c': 'ref003_C',
        'value_d': 'ref004_D',
        'product_name': 'Dhaka Wasa',
        'product_profile': 'Application Form',
    }

    x = requests.post("https://sandbox.sslcommerz.com/gwprocess/v4/api.php", data=data)
    print("**********here is the response****")
    response = x.json()
    gateway = response['GatewayPageURL']
    # return JsonResponse(response)

    return redirect(gateway)


def integrate_ssl_commrz_upgrade(request, tran_type):
    contractor = Contractor.objects.filter(user=request.user, layer__upgrade_submitted=True).last()
    layer = LayerFormStatus.objects.get(contractor=contractor)
    if tran_type == 4:
        if layer.upgrade_submitted and layer.upgrade_form_payment:
            return HttpResponse('You payment has been done already,')
        total_amount = 1000

    base_url = "{0}://{1}".format(request.scheme, request.get_host())
    success_url = base_url + '/success-url/'

    ssl_settings = SSLSettings.objects.all()
    if ssl_settings.exists():
        store_id = ssl_settings[0].store_id
        store_pass = ssl_settings[0].store_pass
    else:
        return HttpResponse('No SSL Store added Yet')

    tran_id = uuid.uuid1()

    contractor_name = contractor.contractor_name
    email = contractor.user.email
    add_1 = contractor.mailing_address
    add_2 = contractor.address
    phone = contractor.user.phone
    fax_no = contractor.fax_no

    if layer.renewal_submitted:
        transaction, _created = Transaction.objects.get_or_create(contractor=contractor, paid_amount=total_amount,
                                                                  transaction_type=tran_type)
        if _created:
            transaction.tran_id = tran_id
            transaction.save()
    else:
        return HttpResponse('Form is not submitted Yet')

    data = {
        'store_id': store_id,
        'store_passwd': store_pass,
        'total_amount': total_amount,
        'currency': 'BDT',
        'tran_id': tran_id,
        'product_category': 'toys',
        'success_url': success_url,
        'fail_url': '',
        'cancel_url': '',
        'emi_option': 0,
        'cus_name': contractor_name,
        'cus_email': email,
        'cus_add1': add_1,
        'cus_add2': add_2,
        'cus_city': 'Dhaka',
        'cus_state': 'Dhaka',
        'cus_postcode': '',
        'cus_country': "Bangladesh",
        'cus_phone': phone,
        'cus_fax': fax_no,
        'shipping_method': 'NO',
        'num_of_item': 1,
        'value_a': 'ref001_A',
        'value_b': 'ref002_B',
        'value_c': 'ref003_C',
        'value_d': 'ref004_D',
        'product_name': 'Dhaka Wasa',
        'product_profile': 'Application Form',
    }

    x = requests.post("https://sandbox.sslcommerz.com/gwprocess/v4/api.php", data=data)
    print("**********here is the response****")
    response = x.json()
    gateway = response['GatewayPageURL']
    # return JsonResponse(response)

    return redirect(gateway)


def integrate_chalan_ssl_commrz(request):
    base_url = "{0}://{1}".format(request.scheme, request.get_host())
    success_url = base_url + '/success-url/'

    ssl_settings = SSLSettings.objects.all()
    if ssl_settings.exists():
        store_id = ssl_settings[0].store_id
        store_pass = ssl_settings[0].store_pass
    else:
        return HttpResponse('No SSL Store added Yet')

    tran_id = uuid.uuid1()
    total_amount = 5000
    contractor = Contractor.objects.get(user=request.user)
    contractor_name = contractor.contractor_name
    email = contractor.user.email
    add_1 = contractor.mailing_address
    add_2 = contractor.address
    phone = contractor.user.phone
    fax_no = contractor.fax_no

    layer = get_object_or_404(LayerFormStatus, contractor=contractor)
    # if layer.is_submitted and layer.form_payment:
    #     return HttpResponse('You payment has been done already,')
    # if layer.is_submitted:
    #     transaction, _created = Transaction.objects.get_or_create(contractor=contractor, paid_amount=total_amount)
    #     if _created:
    #         transaction.tran_id = tran_id
    #         transaction.save()
    # else:
    #     return HttpResponse('Form is not submitted Yet')

    data = {
        'store_id': store_id,
        'store_passwd': store_pass,
        'total_amount': total_amount,
        'currency': 'BDT',
        'tran_id': tran_id,
        'product_category': 'toys',
        'success_url': success_url,
        'fail_url': '',
        'cancel_url': '',
        'emi_option': 0,
        'cus_name': contractor_name,
        'cus_email': email,
        'cus_add1': add_1,
        'cus_add2': add_2,
        'cus_city': 'Dhaka',
        'cus_state': 'Dhaka',
        'cus_postcode': '',
        'cus_country': "Bangladesh",
        'cus_phone': phone,
        'cus_fax': fax_no,
        'shipping_method': 'NO',
        'num_of_item': 1,
        'value_a': 'ref001_A',
        'value_b': 'ref002_B',
        'value_c': 'ref003_C',
        'value_d': 'ref004_D',
        'product_name': 'Dhaka Wasa',
        'product_profile': 'Application Form',
    }

    x = requests.post("https://sandbox.sslcommerz.com/gwprocess/v4/api.php", data=data)
    print("**********here is the response****")
    response = x.json()
    gateway = response['GatewayPageURL']
    # return JsonResponse(response)

    return redirect(gateway)


def mettingCall(request):
    form = MettingForm(request.POST or None)
    users = User.objects.filter(roles='COMMITTEE_MEMBER')

    if request.method == 'POST':
        members = request.POST.getlist('member_user')
        layers = LayerFormStatus.objects.filter(layer='DS_ADMIN', is_submitted=True, form_payment=True)
        if form.is_valid():
            if layers.exists():
                meetting = form.save()
                for member in members:
                    user = User.objects.get(email=member)
                    MettingMembers.objects.create(metting=meetting, member=user)
                for layer in layers:
                    layer.layer = 'COMMITTEE_SECRETARY'
                    layer.form_status = 'PENDING'
                    layer.meetting = meetting
                    layer.save()
            messages.error(request, 'No Contractor found in pending List')
        return redirect('meetting_list')

    context = {
        'form': form,
        'users': users,
    }
    return render(request, 'contractor/metting.html', context)


def mettingEditCall(request, id):
    metting_members_list = []
    meeting = get_object_or_404(Metting, id=id)
    form = MettingForm(request.POST or None, instance=meeting)
    users = User.objects.filter(roles='COMMITTEE_MEMBER')
    metting_members = MettingMembers.objects.filter(metting=meeting)
    for member in metting_members:
        metting_members_list.append(member.member)

    if request.method == 'POST':
        members = request.POST.getlist('member_user')
        layers = LayerFormStatus.objects.filter(layer='DS_ADMIN', is_submitted=True, form_payment=True)
        if form.is_valid():
            meetting = form.save()
            for metting_members_obj in metting_members:
                metting_members_obj.delete()
            for member in members:
                user = User.objects.get(email=member)
                MettingMembers.objects.create(metting=meetting, member=user)
            for layer in layers:
                layer.layer = 'COMMITTEE_SECRETARY'
                layer.form_status = 'PENDING'
                layer.meetting = meetting
                layer.save()
            messages.error(request, 'No Contractor found in pending List')
        return redirect('meetting_list')

    context = {
        'form': form,
        'users': users,
        'metting_members_list': metting_members_list
    }
    return render(request, 'contractor/metting.html', context)


def meettingList(request):
    meettings = Metting.objects.all()
    # contractor_list = LayerFormStatus.objects.filter(layer='DS_ADMIN')
    context = {
        'meettings': meettings,
    }
    return render(request, 'contractor/meeting_list.html', context)


# class ChalanView(CreateView):
#     form_class = ChalanForm
#     template_name = 'contractor/chalan-form.html'
#
#     # success_url = '/'
#
#     def get_context_data(self, **kwargs):
#         contractor = Contractor.objects.filter(layer__is_submitted=True)
#         print(contractor)
#         # self.chalan_form.fields["contractor"].queryset = Contractor.objects.filter(layer__is_submitted=True)
#         context = super().get_context_data(**kwargs)
#         authorized = AuthorizedPerson.objects.all()
#         if authorized.exists():
#             context["authorized_person"] = authorized.first()
#         return context


def ChalanView(request):
    form = ChalanForm(request.POST or None)
    form.fields["contractor"].queryset = Contractor.objects.filter(
        Q(layer__application_approved=True) & Q(layer__chalan_payment=False))

    if form.is_valid():
        chalan = form.save(commit=False)
        chalan.chalan_type = chalan.contractor.applied_for
        chalan.contractor_group_type = chalan.contractor.application_division
        chalan.address = chalan.contractor.office_address
        chalan.save()
        messages.success(request, 'Chalan created for contrator, {}'.format(chalan.contractor.contractor_name))
        return redirect('chalan-list')

    context = {
        'form': form
    }
    return render(request, 'contractor/chalan-form.html', context)


def getContractor(request, pk):
    # return HttpResponse('ok')
    # contractor_id = request.GET.get('contractor_id', None)
    contractor_all_info = Contractor.objects.filter(id=pk)
    contractor_info = Contractor.objects.filter(id=pk).values('address', 'applied_for', 'contractor_name',
                                                              'fathers_name', 'mothers_name', 'proprietor_name',
                                                              'photograph')
    authorized_person = AuthorizedPerson.objects.filter(id=1).values_list('name', 'signature')
    authorized_person = list(authorized_person)
    # chalan_info = get_object_or_404(Chalan, id=pk)
    #
    # class_name = chalan_info.class_type

    contractor_info = list(contractor_info)
    contractor_info.append(authorized_person)

    # contractor_info.append({'class_name': class_name})
    return JsonResponse({"contractor_info": contractor_info})


def PrintPreview(request, pk):
    contractor_chalan = get_object_or_404(Chalan, id=pk)
    fiscal_year = contractor_chalan.fiscal_year

    fiscal_year = fiscal_year.split('-')

    renewal_year = [str(int(fiscal_year[0]) - 1), '-', str(int(fiscal_year[1]) - 1)]
    renewal_year = ''.join(renewal_year)

    if contractor_chalan:
        choice_template = contractor_chalan.contractor.applied_for
        authorized_person = AuthorizedPerson.objects.filter(id=1).first()
        if choice_template == 'enrollment':
            template_name = 'contractor/chalan/license_issue_print_preview.html'

        elif choice_template == 'renewal':
            template_name = 'contractor/chalan/print_preview.html'
        else:
            template_name = 'contractor/chalan/class_upgradation_print_preview.html'

        data = {
            'contractor_chalan': contractor_chalan,
            'authorized_person': authorized_person,
            'renewal_year': renewal_year

        }
    return render(request, template_name, data)


def print_license_preview(request, pk):
    license = get_object_or_404(License, id=pk)
    contractor = license.contractor
    # chalan = get_object_or_404(Chalan,contractor = contractor).first()
    chalan = Chalan.objects.filter(contractor=contractor).first()
    template_name = 'contractor/license/print_license.html'
    data = {
        'license': license,
        'chalan': chalan

    }

    return render(request, template_name, data)
from django.contrib.auth.mixins import LoginRequiredMixin


class LicenseList(LoginRequiredMixin,ListView):
    model = License
    context_object_name = 'licenseList'
    template_name = 'contractor/license/licenselist.html'
    admin_template = 'contractor/license/backend/license_list.html'

    @method_decorator(login_required)
    def get_queryset(self):
        queryset = super(LicenseList, self).get_queryset()
        user = self.request.user
        if not user.is_superuser:
            queryset = queryset.filter(contractor__user=self.request.user).order_by('-id')
        queryset = queryset.order_by('-id')
        return queryset

    def get_template_names(self):
        user = self.request.user
        if user.is_superuser:
            return [self.admin_template]
        return super().get_template_names()


# class PrintPreview(DetailView):
#
#     model = Chalan
#     # template_name = 'contractor/chalan/print_preview.html'
#     #template_name = 'contractor/chalan/license_issue_print_preview.html'
#     template_name = 'contractor/chalan/class_upgradation_print_preview.html'
#     context_object_name = 'contractor_chalan'
#
#     def get_queryset(self):
#         """ Exclude any unpublished questions. """
#         return Question.objects.filter(pub_date__lte=timezone.now())
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#
#         context["authorized_person"] = AuthorizedPerson.objects.filter(id=1).first()
#         print(context["authorized_person"])
#         return context

class addLicenseView(CreateView):
    model = License
    form_class = LicenseForm
    template_name = 'contractor/license/license.html'

    def get_queryset(self):
        queryset = super(addLicenseView, self).get_queryset()
        self.form_class.fields["contractor"].queryset = Contractor.objects.filter(
            Q(layer__application_approved=True) & Q(layer__chalan_payment=True))
        queryset = queryset.filter(contractor__user=self.request.user).order_by('-created_at')
        return queryset


def addLicenseView(request):
    form = LicenseForm(request.POST or None)
    form.fields["contractor"].queryset = Contractor.objects.filter(
        Q(layer__application_approved=True) & Q(layer__chalan_payment=True) & Q(layer__license_created=False))

    if form.is_valid():
        license = form.save(commit=False)
        license.save()
        layer = LayerFormStatus.objects.get(contractor=license.contractor)
        layer.license_created = True
        layer.save()

        messages.success(request, 'License created for contrator, {}'.format(license.contractor.user.email))
        return redirect('license_list')

    context = {
        'form': form
    }
    return render(request, 'contractor/license/license.html', context)


def contractorBackendLicense(request, pk):
    pass


def contractorLicensePreview(request, pk):
    user = request.user
    print("Userinfo", user)
    # if user.is_superuser:
    print("Chalan_id", pk)
    # contractor = get_object_or_404(Contractor, user=user)
    # contractor_chalan = get_object_or_404(Chalan, contractor=contractor)
    contractor_chalan = get_object_or_404(Chalan, id=pk)

    fiscal_year = contractor_chalan.fiscal_year

    fiscal_year = fiscal_year.split('-')

    renewal_year = [str(int(fiscal_year[0]) - 1), '-', str(int(fiscal_year[1]) - 1)]
    renewal_year = ''.join(renewal_year)

    if contractor_chalan:
        choice_template = contractor_chalan.contractor.applied_for
        print("Output", choice_template);
        authorized_person = AuthorizedPerson.objects.filter(id=1).first()
        if choice_template == 'enrollment':
            if user.is_superuser:
                # template_name = 'contractor/chalan/license_issue_print_preview.html'
                template_name = 'contractor/chalan/backend/backend_contractor_license_enlishment_fee.html'
            else:
                template_name = 'contractor/chalan/frontend_contractor_license_enlishment_fee.html'
        elif choice_template == 'renewal':
            if user.is_superuser:
                template_name = 'contractor/chalan/backend/backend_contractor_renewal_chalan.html'
            else:
                template_name = 'contractor/chalan/frontend_contractor_renewal_chalan.html'

        else:
            if user.is_superuser:
                template_name = 'contractor/chalan/backend/backend_contractor_class_upgradation_print_preview.html'
            else:

                template_name = 'contractor/chalan/frontend_contractor_class_upgradation_print_preview.html'

        data = {
            'contractor_chalan': contractor_chalan,
            'authorized_person': authorized_person,
            'renewal_year': renewal_year

        }

    return render(request, template_name, data)


class ChalanList(ListView):
    model = Chalan
    template_name = 'contractor/chalan/chalanlist.html'
    context_object_name = 'ChalanList'

    def get_queryset(self):
        queryset = super(ChalanList, self).get_queryset()
        queryset = queryset.filter(contractor__user=self.request.user).order_by('-created_at')
        return queryset


class ChalanListBack(ListView):
    model = Chalan
    template_name = 'contractor/chalan/backend/chalan_list.html'
    context_object_name = 'ChalanList'

    def get_queryset(self):
        queryset = super(ChalanListBack, self).get_queryset()
        # queryset = queryset.filter(contractor__user=self.request.user).order_by('-created_at')
        queryset = queryset.order_by('-created_at')
        return queryset


def contractor_print_license_preview(request, pk):
    license = get_object_or_404(License, id=pk)
    contractor = license.contractor
    # chalan = get_object_or_404(Chalan,contractor = contractor).first()
    chalan = Chalan.objects.filter(contractor=contractor).first()

    if request.user.is_superuser:
        template_name = 'contractor/license/backend/backend_print_license.html'
    else:
        template_name = 'contractor/license/frontend_print_license.html'
    data = {
        'license': license,
        'chalan': chalan

    }

    return render(request, template_name, data)


def chalanPayment(request):
    return render(request, 'contractor/chalan_payment.html')

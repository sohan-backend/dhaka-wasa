from django.apps import AppConfig


class SetModuleConfig(AppConfig):
    name = 'set_module'

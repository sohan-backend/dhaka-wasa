from .models import Notice
from django.contrib.auth.models import User
from django import forms


# external admin form start
class AddNoticeForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AddNoticeForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            self.fields['published_status'].widget.attrs['class'] = 'select2 form-control'

    class Meta:
        model = Notice
        fields = ('title', 'post_date', 'end_date', 'published_status', 'documents',)


class EditNoticeForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EditNoticeForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            self.fields['published_status'].widget.attrs['class'] = 'select2 form-control'

    class Meta:
        model = Notice
        fields = ('title', 'post_date', 'end_date', 'published_status', 'documents',)

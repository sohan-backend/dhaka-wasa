from django.db import models
import pytz
from datetime import datetime
from django.conf import settings
from django.utils.timezone import now
from datetime import date
from django.core.exceptions import ValidationError


class Notice(models.Model):
    notice_status = (
        ('published', 'published'),
        ('unpublished', 'unpublished')
    )

    title = models.CharField(max_length=255)
    post_date = models.DateField(blank=True)
    end_date = models.DateField(blank=True)
    published_status = models.CharField(max_length=55, choices=notice_status,
                                        default='published')
    documents = models.FileField(upload_to='uploads/%Y/%m/%d/')

    @property
    def is_available(self):
        if self.published_status == 'published':
            now = date.today()
            if now >= self.post_date and now <= self.end_date:
                return {'status': True}
            else:
                return {'status': False}
        else:
            return {'status': False}

    def __str__(self):
        return self.title

    def clean(self):
        if self.post_date > self.end_date:
            raise ValidationError('Post Date should be before than end date ')

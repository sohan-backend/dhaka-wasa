from django.urls import path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('add-notice/', notice_view, name='add-notice'),
    path('notice/', notice_view_user, name='notice'),
    path('list-notice/', notice_list, name='list-notice'),
    path('edit-notice/<int:id>/', notice_edit_view, name='edit-notice'),
    path('delete-notice/<int:id>/', notice_delete_view, name='delete-notice'),
]


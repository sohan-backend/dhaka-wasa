from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import TemplateView
from .models import Notice
# Create your views here.
from .forms import AddNoticeForm, EditNoticeForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import HttpResponse


# @login_required()
# @user_passes_test(lambda user: user.is_admin)
def notice_view(request):
    if request.method == 'POST':
        form = AddNoticeForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, 'Notice successfully added')
            return redirect('list-notice')
        else:
            print(form.errors)
            return HttpResponse('error')
    else:
        form = AddNoticeForm()
        context = {
            'form': form
        }
        return render(request, 'notice/add-notice.html', context)


def notice_view_user(request):
    notice = Notice.objects.filter(published_status='published')
    print(notice)
    context = {
        'notices': notice
    }
    return render(request, 'notice/notice.html', context)


@login_required()
@user_passes_test(lambda user: user.is_admin)
def notice_list(request):
    notice = Notice.objects.all()
    context = {
        'notice': notice
    }
    return render(request, 'notice/list-notice.html', context)


@login_required()
@user_passes_test(lambda user: user.is_admin)
def notice_edit_view(request, id):
    notice = get_object_or_404(Notice, id=id)
    if request.method == 'POST':
        form = EditNoticeForm(request.POST, request.FILES, instance=notice)
        if form.is_valid():
            form.save()
            messages.success(request, 'Successfully Updated')
            return redirect('list-notice')
    form = EditNoticeForm(instance=notice)
    context = {
        'form': form
    }
    return render(request, 'notice/edit-notice.html', context)


@login_required()
@user_passes_test(lambda user: user.is_admin)
def notice_delete_view(request, id):
    notice = get_object_or_404(Notice, id=id)
    notice.delete()
    messages.success(request, 'Successfully deleted')
    return redirect('list-notice')

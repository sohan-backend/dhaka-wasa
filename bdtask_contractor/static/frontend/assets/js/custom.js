console.log('custom js panel here ...');

step = 1
$('#button-next-applicant').click(function(){
    console.log('next');
    step = step +1;
    $("#button-edit-applicant-enrollment").attr("href", `/enrollment/step-${step}/`);
    $("#button-edit-applicant-renewal").attr("href", `/renewal/step-${step}/`);
    $("#button-edit-applicant-upgrade").attr("href", `/license-upgrade/step-${step}/`);
})


$('#button-previous-applicant').click(function(){
    step = step - 1;
    $("#button-edit-applicant").attr("href", `/enrollment/step-${step}/`);
    $("#button-edit-applicant-renewal").attr("href", `/renewal/step-${step}/`);
    $("#button-edit-applicant-upgrade").attr("href", `/license-upgrade/step-${step}/`);
})

$('#contractor_role').change(function(){
    role = this.value;
    $.ajax({
            method: 'GET',
            'contentType': 'application/json',
            url: '/user-lists/'+role + '/',
            success: function (data) {
                  $('#option_list_users').html('');
                  $('#option_list_users').html(data);
            }
        });

})

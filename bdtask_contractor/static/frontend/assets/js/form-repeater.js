// Class definition
var KTFormRepeater = function() {
    var demo3 = function() {
        $('#kt_repeater_1, #kt_repeater_2, #kt_repeater_3, #kt_repeater_4, #kt_repeater_5, #kt_repeater_6, #kt_repeater_7, #kt_repeater_8, #kt_repeater_9, #kt_repeater_10,#kt_repeater_2_form').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            }
        });
    }


    return {
        // public functions
        init: function() {
            demo3();
        }
    };
}();

jQuery(document).ready(function() {
    KTFormRepeater.init();
});


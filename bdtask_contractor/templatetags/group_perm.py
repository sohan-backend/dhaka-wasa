from django import template

register = template.Library()


@register.filter(name='has_group1')
def has_group1(user, group_name):
    return user.groups.filter(name=group_name).exists()

@register.filter(name='joinby')
def joinby(value, arg):
    return arg.join(value)


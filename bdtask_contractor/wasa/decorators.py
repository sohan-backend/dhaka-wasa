from django.contrib import messages
from django.shortcuts import redirect

def allowed_users(allowed_permission=[]):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):
            all_perms = request.user.get_all_permissions()
            print('all permissions: ',all_perms)
            print('allowed perms: ',allowed_permission)
            for perm in allowed_permission:
                if perm in all_perms or request.user.is_admin:
                    return view_func(request, *args, **kwargs)
            else:
                messages.error(request, 'You do not have enough permissions to perform this operations')
                return redirect('/')

        return wrapper_func

    return decorator


def allowed_roles(allowed_permission=[]):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):
            all_perms = request.user.get_all_permissions()
            for perm in allowed_permission:
                if perm in all_perms or request.user.is_admin:
                    return view_func(request, *args, **kwargs)
            else:
                messages.error(request, 'You do not have enough permissions to perform this operations')
                return redirect('/')

        return wrapper_func

    return decorator



def allowed_restaurants_users():
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):
            user_id = request.user.id
            print('here...',user_id)
            restaurant = Restaurants.objects.filter(user=request.user)
            if restaurant.exists():
                user_lists = []
                current_user = RestaurantsUser.objects.get(user=request.user)
                current_user = str(current_user)
                restaurant_user = RestaurantsUser.objects.filter(restaurant=restaurant.first())
                for user in restaurant_user:
                    user_lists.append(user.user.email)
                print(current_user,user_lists)
                if current_user in user_lists:
                    print('ok')
                    return view_func(request, *args, **kwargs)
                else:
                    print('not ok')
                    return redirect('/')

            else:
                messages.error(request, 'You have no restaurants')
                return redirect('/')

        return wrapper_func

    return decorator


# from django.http import HttpResponse
# from django.shortcuts import redirect
# from django.contrib import messages
#
#
# def allowed_users(allowed_groups):
#     def decorator(view_func):
#         def wrapper_func(request, *args, **kwargs):
#             list_groups = []
#             if request.user.groups.exists():
#                 groups = request.user.groups.all()
#                 for group in groups:
#                     list_groups.append(group.name)
#
#             if allowed_groups in list_groups or request.user.is_admin:
#                 return view_func(request, *args, **kwargs)
#             else:
#                 messages.error(request, 'You have to not enough permissions to perform this operations')
#                 return redirect('/')
#
#         return wrapper_func
#
#     return decorator
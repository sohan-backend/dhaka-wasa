import os

DEV = os.environ.get("DEV", True)

if DEV:
    from .local import *
else:
    from .prod import *

from .base import *

DEBUG = True

ALLOWED_HOSTS = ['*']

# print('*******', BASE_DIR)


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}



# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': "wasa",
#         'USER': "postgres",
#         'PASSWORD': "postgres",
#         'HOST': '127.0.0.1',
#         'PORT': 5432
#     }
# }

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)


